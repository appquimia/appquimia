package com.appquimia.appquimia.services;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.Publicidad;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by usuario on 07/10/2017.
 */

public class PublicidadService
{
    public static final String API_SECTION = "publicidad";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_NEWS = Constantes.API_BASE + API_SECTION + "/n/?";

    private VolleyCallbackAbstract mCallback;
    private String mJsonOutput;

    public static List<Publicidad> process(String jsonOutput, SQLiteDatabase database) throws Exception {
        Gson gson = new Gson();

        Type listType = new TypeToken<List<Publicidad>>(){}.getType();
        List<Publicidad> items = gson.fromJson(jsonOutput, listType);

        if(!items.isEmpty())
            Publicidad.clearTable(database);
        try {
            for (Publicidad item : items) {
//                item.setImagenb(downloadImage(item.getUrl()));
                item.save(database);
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static List<Publicidad> extractImages(String jsonOutput) {
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<List<Publicidad>>(){}.getType();
            List<Publicidad> items = gson.fromJson(jsonOutput, listType);

            return items;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public JsonArrayRequest getRequestListdado(Context context, VolleyCallbackAbstract callback, SQLiteDatabase database) {
        mCallback = callback;

        String fechaUltModif = Publicidad.getLastFechaModif(database);
        String url = API_CALL_NEWS.replace("?", fechaUltModif);

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            mJsonOutput = response.toString();
                            mCallback.onConnectionFinished();
                        } catch (Exception e) {
                            mCallback.onConnectionFailed(API_SECTION + ": " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        mCallback.onConnectionFailed(API_SECTION + ": " + volleyError.toString());
                    }
                }
        );

        return request;
    }

    private byte[] downloadImage(String url) throws Exception {
        byte[] returnArray = null;
        int responseCode;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            url = url.replace("https", "http");
            URL imageUrl = new URL(url);
            HttpURLConnection ucon = (HttpURLConnection) imageUrl.openConnection();
            ucon.setDoInput(true);
            ucon.setConnectTimeout(1000);
            ucon.connect();
            responseCode = ucon.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                InputStream is = ucon.getInputStream();

                BufferedInputStream bis = new BufferedInputStream(is);
                ByteArrayOutputStream buffer = new ByteArrayOutputStream(2048);
                byte[] data = new byte[512];
                int current = 0;

                while ((current = bis.read(data, 0, data.length)) != -1) {
                    buffer.write(data, 0, current);
                }

//                Bitmap bitmap = Utils.decodeSampledBitmapFromResource(buffer, 256, 128);
                ucon.disconnect();
                returnArray = buffer.toByteArray();
            } else {
                mCallback.onConnectionFailed(API_SECTION + ": " + "Error de conexión!");
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            throw e;
        }
        return returnArray;
    }

//    private byte[] downloadImage(String url) {
//        byte[] returnArray = null;
//        try {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//
//            url = url.replace("https", "http");
//            URL imageUrl = new URL(url);
//            URLConnection ucon = imageUrl.openConnection();
//            InputStream is = ucon.getInputStream();
//
//            BufferedInputStream bis = new BufferedInputStream(is);
//            ByteArrayOutputStream buffer = new ByteArrayOutputStream(2048);
//            byte[] data = new byte[512];
//            int current = 0;
//
//            while ((current = bis.read(data, 0, data.length)) != -1) {
//                buffer.write(data, 0, current);
//            }

//            Bitmap bitmap = Utils.decodeSampledBitmapFromResource(buffer, 256, 128);
            /*
            Bitmap bitmap = BitmapFactory.decodeByteArray(buffer.toByteArray(), 0, buffer.toByteArray().length);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, buffer);
            */
//            if(Utils.DEBUG) {
//                File f = new File(mContext.getFilesDir(), imageUrl.getFile().substring(imageUrl.getFile().lastIndexOf('/') + 1));
//                f.createNewFile();
//
//                FileOutputStream fos = new FileOutputStream(f);
//                fos.write(buffer.toByteArray());
//                fos.flush();
//                fos.close();
//            }
//
//            ucon.disconnect();
//            returnArray =  buffer.toByteArray();
//        } catch (Exception e) {
//            Log.d("ImageManager", "Error: " + e.toString());
//        }
//        return returnArray;
//    }
}
