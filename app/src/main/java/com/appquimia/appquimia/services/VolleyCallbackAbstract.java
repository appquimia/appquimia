package com.appquimia.appquimia.services;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by usuario on 10/10/2017.
 */

public abstract class VolleyCallbackAbstract
{
    protected Context mContext;
    protected String mService;

    public VolleyCallbackAbstract(Context cntx, String service)
    {
        mContext = cntx;
        mService = service;
    }

    public void  onConnectionFinished()
    {
        Toast.makeText(mContext, mService + " sincronización finalizada.", Toast.LENGTH_SHORT).show();
    }

    public void onConnectionFailed(String error)
    {
        Toast.makeText(mContext, "Error en " + mService + ": " + error, Toast.LENGTH_SHORT).show();
    }
}
