package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.appquimia.appquimia.entities.Clasificacion;
import com.appquimia.appquimia.entities.Cuenta;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by usuario on 19/12/2017.
 */

public class CuentaService
{
    public static final String API_SECTION = "cuenta";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION;

    private Gson mGson;
    private SQLiteDatabase mDatabase;
    private VolleyCallbackAbstract mCallback;

    public JsonArrayRequest getRequestListdado(VolleyCallbackAbstract callback, SQLiteDatabase database)
    {
        mDatabase = database;
        mGson = new Gson();
        mCallback = callback;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, API_CALL_LISTADO, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        try
                        {
                            String jsonOutput = response.toString();
                            Type listType = new TypeToken<List<Clasificacion>>(){}.getType();
                            List<Clasificacion> items = (List<Clasificacion>) mGson.fromJson(jsonOutput, listType);

                            for (Clasificacion item : items)
                            {
                                item.save(mDatabase);
                            }

                            mCallback.onConnectionFinished();
                        }
                        catch (Exception e)
                        {
                            mCallback.onConnectionFailed(API_SECTION + ": " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError)
                    {
                        mCallback.onConnectionFailed(API_SECTION + ": " + volleyError.toString());
                    }
                }
        );

        return request;
    }

    public JsonRequest sendNewCuenta(VolleyCallbackAbstract callback, SQLiteDatabase database, Cuenta entity)
    {
        mGson = new Gson();
        mCallback = callback;

        mGson.toJson(entity);
        Type type = new TypeToken<Cuenta>() {}.getType();
        String json = mGson.toJson(entity, type);
        JSONObject jobject = null;
        try {
            jobject = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        JsonRequest request = new JsonObjectRequest(
                Request.Method.POST,
                API_CALL_LISTADO,
                jobject,
                new Response.Listener<JSONObject>()
                {
                    @Override
                    public void onResponse(JSONObject response) {
                        try
                        {
                            String jsonOutput = response.toString();
                            mCallback.onConnectionFinished();
                        }
                        catch (Exception e)
                        {
                            mCallback.onConnectionFailed(API_SECTION + ": " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError)
                    {
                        mCallback.onConnectionFailed(API_SECTION + ": " + volleyError.toString());
                    }
                }
        )
        {
            @Override
            public Map<String, String> getHeaders()
            {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                headers.put("Accept", "application/json");
                return headers;
            }

            @Override
            public String getBodyContentType()
            {
                return "application/json; charset=utf-8" + getParamsEncoding();
            }
        };

        return request;
    }
}
