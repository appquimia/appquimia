package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Premio;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


/**
 * Created by usuario on 07/10/2017.
 */

public class PremioService {
    public static final String API_SECTION = "premio";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_NEWS = Constantes.API_BASE + API_SECTION + "/n/?";

    private VolleyCallbackAbstract mCallback;
    private String mJsonOutput;

    public static List<Premio> process(String jsonOutput, SQLiteDatabase database) throws Exception {
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<List<Premio>>(){}.getType();
            List<Premio> items = gson.fromJson(jsonOutput, listType);

            if(!items.isEmpty())
                Premio.clearTable(database);

            for (Premio item : items){
                item.save(database);
            }
            return items;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    public static List<Premio> extractImages(String jsonOutput) {
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<List<Premio>>(){}.getType();
            List<Premio> items = gson.fromJson(jsonOutput, listType);

            return items;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

//    public void processImages(SQLiteDatabase database) throws Exception {
//        Gson gson = new Gson();
//        Type listType = new TypeToken<List<Premio>>(){}.getType();
//        List<Premio> items = gson.fromJson(mJsonOutput, listType);
//
//        try {
//            for (Premio item : items){
//                item.setImagenb(downloadImage(item.getImagen()));
//                item.save(database);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            throw e;
//        }
//    }
//
//    public JsonArrayRequest getRequestListdado(VolleyCallbackAbstract callback, SQLiteDatabase database) {
//        mCallback = callback;
//
//        String fechaUltModif = Utils.dateTimerEncode(Premio.getLastFechaModif(database), "yyyy-MM-dd HH:mm:ss");
//        String url = API_CALL_NEWS.replace("?", fechaUltModif);
//
//        return new JsonArrayRequest(Request.Method.GET, url, null,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        try {
//                            mJsonOutput = response.toString();
//                            mCallback.onConnectionFinished();
//                        } catch (Exception e) {
//                            mCallback.onConnectionFailed(API_SECTION + ": " + e.getMessage());
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        mCallback.onConnectionFailed(API_SECTION + ": " + volleyError.toString());
//                    }
//                }
//        );
//    }
//
//    private byte[] downloadImage(String url) throws Exception {
//        byte[] returnArray = null;
//        int responseCode;
//        try {
//            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//            StrictMode.setThreadPolicy(policy);
//            long mtInicio = System.currentTimeMillis();
//            url = url.replace("https", "http");
//            URL imageUrl = new URL(url);
//            HttpURLConnection ucon = (HttpURLConnection) imageUrl.openConnection();
//            ucon.setDoInput(true);
//            ucon.setConnectTimeout(1000);
//            ucon.connect();
//            responseCode = ucon.getResponseCode();
//
//            if (responseCode == HttpURLConnection.HTTP_OK) {
//                InputStream is = ucon.getInputStream();
//
//                BufferedInputStream bis = new BufferedInputStream(is);
//                ByteArrayOutputStream buffer = new ByteArrayOutputStream(102400);
//                byte[] data = new byte[10240];
//
////                ByteArrayOutputStream buffer = new ByteArrayOutputStream(1024);
////                byte[] data = new byte[512];
//                int current = 0;
//
//                int count = 0;
//                while ((current = bis.read(data, 0, data.length)) != -1){
//                    buffer.write(data, 0, current);
//                    count++;
//                }
//
////                Bitmap bitmap = Utils.decodeSampledBitmapFromResource(buffer, 256, 128);
//                ucon.disconnect();
//                returnArray =  buffer.toByteArray();
//            } else {
//                mCallback.onConnectionFailed(API_SECTION + ": " +  "Error de conexión!");
//            }
//
//        } catch (MalformedURLException e) {
//            e.printStackTrace();
//            throw e;
//        } catch (IOException e) {
//            e.printStackTrace();
//            throw e;
//        }catch (Exception e) {
//            throw e;
//        }
//        return returnArray;
//    }
}
