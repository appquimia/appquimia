package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.StrictMode;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.entities.Conductor;
import com.appquimia.appquimia.entities.Movil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by usuario on 07/10/2017.
 */

public class MovilService
{
    public static final String API_SECTION = "movil";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION;

    private VolleyCallbackAbstract mCallback;
    private String mJsonOutput;

    public static void process(String jsonOutput, SQLiteDatabase database) {
        Gson gson = new Gson();
        try {

            Type listType = new TypeToken<List<Movil>>(){}.getType();
            List<Movil> items = gson.fromJson(jsonOutput, listType);
            Movil ite = new Movil();
            ite.desactivateAll(database);

            for (Movil item : items){
                item.save(database);
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public JsonArrayRequest getRequestListdado(VolleyCallbackAbstract callback)
    {
        mCallback = callback;

        return new JsonArrayRequest(Request.Method.GET, API_CALL_LISTADO, null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            mJsonOutput = response.toString();
                            mCallback.onConnectionFinished();
                        } catch (Exception e) {
                            mCallback.onConnectionFailed(API_SECTION + ": " + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        mCallback.onConnectionFailed(API_SECTION + ": " + volleyError.toString());
                    }
                }
        );
    }

    private byte[] downloadImage(String url) {
        byte[] returnArray = null;
        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            URL imageUrl = new URL(url);
            URLConnection ucon = imageUrl.openConnection();
            InputStream is = ucon.getInputStream();

            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayOutputStream buffer = new ByteArrayOutputStream(500);
            byte[] data = new byte[250];
            int current = 0;

            while ((current = bis.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, current);
            }

            Bitmap bitmap = BitmapFactory.decodeByteArray(buffer.toByteArray(), 0, buffer.toByteArray().length);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, buffer);
            returnArray =  buffer.toByteArray();
        } catch (Exception e) {
            Log.d("ImageManager", "Error: " + e.toString());
        }
        return returnArray;
    }
}
