package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.entities.Conductor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by usuario on 07/10/2017.
 */

public class ConductorService {
    public static final String API_SECTION = "conductor";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION;

//    private VolleyCallbackAbstract mCallback;
//    private String mJsonOutput;

    public static void process(String ssonOutput, SQLiteDatabase database) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Conductor>>(){}.getType();
        List<Conductor> items = gson.fromJson(ssonOutput, listType);
        Conductor ite = new Conductor();
        try {
            ite.desactivateAll(database);
        } catch (Exception e) {
            e.printStackTrace();
        }

        for (Conductor item : items) {
            item.save(database);
        }
    }

//    public JsonArrayRequest getRequestListdado(VolleyCallbackAbstract callback) {
//        mCallback = callback;
//
//        return new JsonArrayRequest(Request.Method.GET, API_CALL_LISTADO, null,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        try {
//                            mJsonOutput = response.toString();
//                            mCallback.onConnectionFinished();
//                        } catch (Exception e) {
//                            mCallback.onConnectionFailed(API_SECTION + ": " + e.getMessage());
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        mCallback.onConnectionFailed(API_SECTION + ": " + volleyError.toString());
//                    }
//                }
//        );
//    }
}
