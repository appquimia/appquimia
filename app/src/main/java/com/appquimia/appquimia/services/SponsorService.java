package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;
import android.os.StrictMode;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.Sponsor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by usuario on 07/10/2017.
 */

public class SponsorService {
    public static final String API_SECTION = "sponsor";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    private static final String API_CALL_NEWS = Constantes.API_BASE + API_SECTION + "/n/?";
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION + "/s";

    public static void process(String jsonOutput, SQLiteDatabase database) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Sponsor>>(){}.getType();
        List<Sponsor> items = gson.fromJson(jsonOutput, listType);

        if(!items.isEmpty())
            Sponsor.clearTable(database);

        for (Sponsor item : items) {
            item.save(database);
        }
    }

    public static List<Sponsor> extractImages(String jsonOutput) {
        Gson gson = new Gson();
        try {
            Type listType = new TypeToken<List<Sponsor>>(){}.getType();
            List<Sponsor> items = gson.fromJson(jsonOutput, listType);

            return items;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }
}
