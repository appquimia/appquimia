package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;

import com.appquimia.appquimia.entities.ConfiguracionTiempo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class ConfiguracionTiempoService {
    public static final String API_SECTION = "configuraciontiempo";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION;

//    private VolleyCallbackAbstract mCallback;
//    private String mJsonOutput;

    public static void process(String ssonOutput, SQLiteDatabase database) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<ConfiguracionTiempo>>(){}.getType();
        List<ConfiguracionTiempo> items = gson.fromJson(ssonOutput, listType);
        ConfiguracionTiempo ite = new ConfiguracionTiempo();

        for (ConfiguracionTiempo item : items) {
            item.save(database);
        }
    }
//    public void process(SQLiteDatabase database) {
//        Gson gson = new Gson();
//
//        Type listType = new TypeToken<List<ConfiguracionTiempo>>(){}.getType();
//        List<ConfiguracionTiempo> items = gson.fromJson(mJsonOutput, listType);
//
//        for (ConfiguracionTiempo item : items) {
//            item.save(database);
//        }
//    }

//    public JsonArrayRequest getRequestListdado(VolleyCallbackAbstract callback) {
//        mCallback = callback;
//
//        return new JsonArrayRequest(Request.Method.GET, API_CALL_LISTADO, null,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        try {
//                            mJsonOutput = response.toString();
//                            mCallback.onConnectionFinished();
//                        } catch (Exception e) {
//                            mCallback.onConnectionFailed(e.getMessage());
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError volleyError) {
//                        mCallback.onConnectionFailed(volleyError.toString());
//                    }
//                }
//        );
//    }
}
