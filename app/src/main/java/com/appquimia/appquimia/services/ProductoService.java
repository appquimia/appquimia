package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;

import com.appquimia.appquimia.entities.Producto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by usuario on 07/10/2017.
 */

public class ProductoService {
    public static final String API_SECTION = "producto";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION;
    public static final String API_CALL_BYIDSPONSOR = Constantes.API_BASE + API_SECTION + "/byidsponsor/?";


    public static void process(String jsonOutput, SQLiteDatabase database) {
        Gson gson = new Gson();
        Type listType = new TypeToken<List<Producto>>(){}.getType();
        List<Producto> items = gson.fromJson(jsonOutput, listType);

        if(!items.isEmpty())
            Producto.clearTable(database);

        for (Producto item : items) {
            item.save(database);
        }
    }
}
