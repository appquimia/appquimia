package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.entities.Paquete;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by usuario on 07/10/2017.
 */

public class VersionService {
    public static final String API_SECTION = "version";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    //GETs
    public static final String API_GET_VERSION = Constantes.API_BASE + API_SECTION;
    public static final String API_GET_MANTENIMIENTO = Constantes.API_BASE + API_SECTION + "mantenimiento";

//    private VolleyCallbackAbstract mCallback;

//    public static void process(String jsonOutput, SQLiteDatabase database) {
//        Gson gson = new Gson();
//
//        Type listType = new TypeToken<List<Paquete>>(){}.getType();
//        List<Paquete> items = gson.fromJson(jsonOutput, listType);
//
//        for (Paquete item : items){
//            item.save(database);
//        }
//    }
}
