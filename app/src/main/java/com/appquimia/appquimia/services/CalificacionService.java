package com.appquimia.appquimia.services;

import android.database.sqlite.SQLiteDatabase;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.entities.Clasificacion;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by usuario on 20/12/2017.
 */

public class CalificacionService
{
    public static final String API_SECTION = "calificacion";
    public static final String API_CALL = Constantes.API_BASE + API_SECTION;

    // GETs
    public static final String API_CALL_LISTADO = Constantes.API_BASE + API_SECTION;

    private Gson mGson;
    private SQLiteDatabase mDatabase;
    private VolleyCallbackAbstract mCallback;

    public JsonArrayRequest getRequestListdado(VolleyCallbackAbstract callback, SQLiteDatabase database)
    {
        mDatabase = database;
        mGson = new Gson();
        mCallback = callback;

        JsonArrayRequest request = new JsonArrayRequest(Request.Method.GET, API_CALL_LISTADO, null,
                new Response.Listener<JSONArray>()
                {
                    @Override
                    public void onResponse(JSONArray response)
                    {
                        try
                        {
                            String jsonOutput = response.toString();
                            Type listType = new TypeToken<List<Clasificacion>>(){}.getType();
                            List<Clasificacion> items = (List<Clasificacion>) mGson.fromJson(jsonOutput, listType);

                            for (Clasificacion item : items)
                            {
                                item.save(mDatabase);
                            }

                            mCallback.onConnectionFinished();
                        }
                        catch (Exception e)
                        {
                            mCallback.onConnectionFailed(e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError volleyError)
                    {
                        mCallback.onConnectionFailed(volleyError.toString());
                    }
                }
        );

        return request;
    }

}
