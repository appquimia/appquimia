package com.appquimia.appquimia;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.adeel.library.easyFTP;
import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.entities.Sponsor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Random;

public class PremiosActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    public static final String TIPO_MODULO = "tipo_modulo";

    public static String FROM_LOGIN = "fromlogin";
    private ArrayList<PremioXCuenta> marrPremios = null;

    private DrawerLayout mDrawer;
    NavigationView navigationView;

    static boolean mActive = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        int dl = R.id.dlPremios;
        int nv = R.id.nvPremios;
        int tab = R.layout.title_action_bar_premios;

        switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
            case Constantes.MODULE_MOVILES:
                setContentView(R.layout.activity_premios);
                dl = R.id.dlPremios;
                nv = R.id.nvPremios;
                tab = R.layout.title_action_bar_premios;
                break;
            case Constantes.MODULE_COMMERCE:
                setContentView(R.layout.activity_cmr_premios);
                dl = R.id.dlCmrPremios;
                nv = R.id.nvCmrPremios;
                tab = R.layout.title_action_bar_premios;
                break;
            case Constantes.MODULE_STREET:
                setContentView(R.layout.activity_str_premios);
                dl = R.id.dlStrPremios;
                nv = R.id.nvStrPremios;
                tab = R.layout.title_action_bar_premios;
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        // Sección acttion bar, toolbar, navbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(dl);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        if(getIntent().getBooleanExtra(PremiosActivity.FROM_LOGIN, false)) {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.syncState();
        }

        navigationView = findViewById(nv);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                PremiosActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    PremiosActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(tab);
        }

        // Sección comportamiento particular
        PremioXCuenta.vencerPremios(DatabaseHelper.getDBInstance(PremiosActivity.this));
        if(DatabaseHelper.getDBInstance(this).isOpen())
            DatabaseHelper.getDBInstance(this).close();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(PremiosActivity.this));

        RecyclerViewOnItemClickListener mRVOnItemClickListener = new RecyclerViewOnItemClickListener() {
            @Override
            public void onClick(View v, int position) {
                PremioXCuenta pxc = marrPremios.get(position);
                SessionProcessor.setPremioID(getApplicationContext(), pxc.getId());
                Intent intent = new Intent();
                intent.setClass(PremiosActivity.this, PremioActivity.class);
                if (getIntent().getBooleanExtra(PremiosActivity.FROM_LOGIN, false)) {
                    intent.putExtra(PremiosActivity.FROM_LOGIN, true);
                } else {
                    switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
                        case Constantes.MODULE_MOVILES:
                            intent.putExtra(PremioActivity.TIPO_MODULO, Constantes.MODULE_MOVILES);
                            break;
                        case Constantes.MODULE_COMMERCE:
                            intent.putExtra(PremioActivity.TIPO_MODULO, Constantes.MODULE_COMMERCE);
                            break;
                        case Constantes.MODULE_STREET:
                            intent.putExtra(PremioActivity.TIPO_MODULO, Constantes.MODULE_STREET);
                            break;
                    }
                }
                startActivity(intent);
            }
        };
        RecyclerView.LayoutManager mlmPremios = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false) {
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
        RecyclerView mLista = findViewById(R.id.lstPremios);
        mLista.setHasFixedSize(true);
        marrPremios = PremioXCuenta.getListByIdCuenta(cuenta.getId(), DatabaseHelper.getDBReadableInstance(PremiosActivity.this));
        PremiosAdapter mAdapter = new PremiosAdapter(marrPremios, mRVOnItemClickListener);
        mLista.setLayoutManager(mlmPremios);
        mLista.setAdapter(mAdapter);

        DividerItemDecoration verticalDecoration = new DividerItemDecoration(mLista.getContext(), DividerItemDecoration.HORIZONTAL);
        Drawable verticalDivider = ContextCompat.getDrawable(this, R.drawable.horizontal_divider);
        assert verticalDivider != null;
        verticalDecoration.setDrawable(verticalDivider);
        mLista.addItemDecoration(verticalDecoration);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_MOVILES) {
            if (id == R.id.nav_movil) {
                NavigationOptions.opMovil(this, true);
            } else if (id == R.id.nav_tarifador) {
                NavigationOptions.opTarifador(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificar(this, true);
            } else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugar(this, true);
            } else if (id == R.id.nav_premios) {
                //NavigationOptions.opPremios(this, true);
            } else if (id == R.id.nav_bitacora) {
                NavigationOptions.opBitacora(this, true);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else  if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_COMMERCE) {
//            if (id == R.id.nav_marca) {
//                NavigationOptions.opMarca(this, true);
//            }  else if (id == R.id.nav_jugar) {
//                NavigationOptions.opJugarStreet(this, true);
//            } else if (id == R.id.nav_premios) {
////                NavigationOptions.opPremiosCommerce(this, true);
//            } else if (id == R.id.nav_calificar) {
//                NavigationOptions.opCalificarCommerce(this, true);
//            } else if (id == R.id.nav_recomendar) {
//                NavigationOptions.opRecomentdarCommerce(this, false);
//            } else if (id == R.id.nav_fin) {
//                NavigationOptions.finish(this);
//            }
        } else {
            if (id == R.id.nav_auspiciantes) {
                NavigationOptions.opAuspiciantes(this, true);
            }  else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                //NavigationOptions.opPremiosStreet(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActive = true;
        final Handler hdPublicidad = new Handler();
        hdPublicidad.postDelayed(new Runnable() {
            public void run() {
                PremiosActivity.this.showPublicidad(DatabaseHelper.getDBReadableInstance(PremiosActivity.this));
                if (mActive)
                    hdPublicidad.postDelayed(this, 10000);
            }
        }, 500);
    }

    @Override
    protected void  onPause() {
        System.gc();
        mActive = false;
        super.onPause();
    }

    @Override
    protected void onStop(){
        System.gc();
        mActive = false;
        super.onStop();
    }

    private void showPublicidad(SQLiteDatabase db) {
        Publicidad pub1 = new Publicidad();
        Publicidad pub2 = new Publicidad();
        Random randomGenerator = new Random();
        float randomInt;

        while (pub1.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub1.fill(-1, randomInt, db);
        }

        while (pub2.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub2.fill(pub1.getId(), randomInt, db);
        }

        ByteArrayInputStream imageStream;

        if(pub1.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub1.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice1);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }

        if(pub2.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub2.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice2);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }
    }

    private class PremiosAdapter extends android.support.v7.widget.RecyclerView.Adapter<PremiosAdapter.ItemView> {
        ArrayList<PremioXCuenta> mItems;
        private RecyclerViewOnItemClickListener mOnItemClickListener;

        PremiosAdapter(ArrayList<PremioXCuenta> items,
                       @NonNull RecyclerViewOnItemClickListener onItemClickListener) {
            this.mItems = items;
            this.mOnItemClickListener = onItemClickListener;
        }

        @NonNull
        @Override
        public ItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_premios, parent, false);
            return new ItemView(v);
        }

        @Override
        public void onBindViewHolder(@NonNull ItemView holder, int position) {
            Spanned text;
            PremioXCuenta s = mItems.get(position);
            if (s != null) {
                Premio premio = new Premio();
                premio.fillByID(s.getIdpremio(), DatabaseHelper.getDBReadableInstance(PremiosActivity.this));
                Sponsor sponsor = new Sponsor();
                sponsor.fillByID(premio.getIdsponsor(), DatabaseHelper.getDBReadableInstance(PremiosActivity.this));
                holder.mSponsor.setText(sponsor.getRazonsocial());
                holder.mDescripcion.setText(premio.getDescripcion());
                text = Html.fromHtml("<b>Código: </b>" + premio.getCodigo());
                holder.mCodigo.setText(text);
                text = Html.fromHtml("<b>Vencimiento: </b>" + s.getFecven());
                holder.mVencimiento.setText(text);

                if (premio.getImagenb() != null) {
                    ByteArrayInputStream imageStream = new ByteArrayInputStream(premio.getImagenb());
                    Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                    holder.mPremio.setImageBitmap(theImage);
                } else {
                    holder.mPremio.setBackgroundResource(R.drawable.premio);
                }
            }
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }

        public class ItemView extends RecyclerView.ViewHolder implements View.OnClickListener {
            private TextView mSponsor;
            private TextView mDescripcion;
            private TextView mCodigo;
            private TextView mVencimiento;
            private ImageView mPremio;

            ItemView(View itemView) {
                super(itemView);

                mSponsor = itemView.findViewById(R.id.tvSponsor);
                mDescripcion = itemView.findViewById(R.id.tvDescripcionPremio);
                mCodigo = itemView.findViewById(R.id.tvCodigoPremio);
                mVencimiento = itemView.findViewById(R.id.tvVencimiento);
                mPremio = itemView.findViewById(R.id.imgPremio);
                itemView.setOnClickListener(this);
            }

            @Override
            public void onClick(View view) {
                mOnItemClickListener.onClick(view, getAdapterPosition());
            }
        }
    }

    public interface  RecyclerViewOnItemClickListener {
        void onClick(View v, int position);
    }
}
