package com.appquimia.appquimia.adapters;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.appquimia.appquimia.R;
import com.appquimia.appquimia.classes.MainApp;
import com.appquimia.appquimia.entities.Sponsor;

import java.util.ArrayList;

public class CarouselAdapter extends android.support.v7.widget.RecyclerView.Adapter<CarouselAdapter.ItemView> {
    private ArrayList<Sponsor> mDataSet;
    private ImageLoader imageLoader = MainApp.getPermission().getImageLoader();
    private int width;
    private int height;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public CarouselAdapter(ArrayList<Sponsor> dataSet) {
        mDataSet = dataSet;
    }

    @NonNull
    public CarouselAdapter.ItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_carousel, parent, false);
        return new ItemView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CarouselAdapter.ItemView holder, int position) {
        Sponsor sponsor = mDataSet.get(position);

        holder.mSponsor.setText(Html.fromHtml(sponsor.getRazonsocial()));
        holder.mDescripcion.setText(sponsor.getDirecciones());
        holder.mTelefono.setText(sponsor.getTelefono());
        holder.mImagen.setImageUrl(sponsor.getImagen(), imageLoader);

        int width = (int) (CarouselAdapter.this.width * 0.64);
        int height = (int) (CarouselAdapter.this.height * 0.36);
        ConstraintLayout.LayoutParams layoutParams=new ConstraintLayout.LayoutParams(width, height);
        layoutParams.setMargins(0, 0, 0, 0);
        holder.mImagen.setLayoutParams(layoutParams);

        ConstraintSet set = new ConstraintSet();
        set.clone(holder.mConstraintLayout);
        set.connect(holder.mSponsor.getId(), ConstraintSet.START, holder.mImagen.getId(), ConstraintSet.START, 10);
        set.connect(holder.mSponsor.getId(), ConstraintSet.END, holder.mImagen.getId(), ConstraintSet.END, 10);
        set.connect(holder.mDescripcion.getId(), ConstraintSet.START, holder.mImagen.getId(), ConstraintSet.START, 10);
        set.connect(holder.mDescripcion.getId(), ConstraintSet.END, holder.mImagen.getId(), ConstraintSet.END, 10);
        set.connect(holder.mTelefono.getId(), ConstraintSet.START, holder.mImagen.getId(), ConstraintSet.START, 10);
        set.connect(holder.mTelefono.getId(), ConstraintSet.END, holder.mImagen.getId(), ConstraintSet.END, 10);
        set.connect(R.id.line1, ConstraintSet.START, holder.mImagen.getId(), ConstraintSet.START, 10);
        set.connect(R.id.line1, ConstraintSet.END, holder.mImagen.getId(), ConstraintSet.END, 10);
        set.applyTo(holder.mConstraintLayout);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    class ItemView extends RecyclerView.ViewHolder {
        private TextView mSponsor;
        private TextView mDescripcion;
        private TextView mTelefono;
        private NetworkImageView mImagen;
        private ConstraintLayout mConstraintLayout;

        ItemView(View itemView) {
            super(itemView);

            if (imageLoader == null)
                imageLoader = MainApp.getPermission().getImageLoader();

            mImagen = itemView.findViewById(R.id.ivItem);
            mSponsor = itemView.findViewById(R.id.tvSponsor);
            mDescripcion = itemView.findViewById(R.id.tvDescripcion);
            mTelefono = itemView.findViewById(R.id.tvTelefono);

            mConstraintLayout  = itemView.findViewById(R.id.layout1);
        }
    }
}
