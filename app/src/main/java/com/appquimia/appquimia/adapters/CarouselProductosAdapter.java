package com.appquimia.appquimia.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.appquimia.appquimia.R;
import com.appquimia.appquimia.classes.MainApp;
import com.appquimia.appquimia.entities.Producto;

import java.util.ArrayList;

public class CarouselProductosAdapter extends RecyclerView.Adapter<CarouselProductosAdapter.ItemView> {
    private ArrayList<Producto> mDataSet;
    private ImageLoader imageLoader = MainApp.getPermission().getImageLoader();
    private int width;
    private int height;

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public CarouselProductosAdapter(ArrayList<Producto> dataSet) {
        mDataSet = dataSet;
    }

    @NonNull
    @Override
    public CarouselProductosAdapter.ItemView onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_list_productos, parent, false);
        return new ItemView(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CarouselProductosAdapter.ItemView holder, int position) {
        Producto producto = mDataSet.get(position);

        holder.mProducto.setText(Html.fromHtml(producto.getProducto()));
        holder.mDescripcion.setText(Html.fromHtml(producto.getDescripcion()));
        holder.mImagen.setImageUrl(producto.getImagen(), imageLoader);

        int width = (int) (CarouselProductosAdapter.this.height * 0.28);
        int height = (int) (CarouselProductosAdapter.this.height * 0.28);
        LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(width, height);
        layoutParams.setMargins(0, 0, 0, 0);
        holder.mImagen.setLayoutParams(layoutParams);
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }

    class ItemView extends RecyclerView.ViewHolder {
        private TextView mProducto;
        private TextView mDescripcion;
        private NetworkImageView mImagen;

        ItemView(View itemView) {
            super(itemView);

            if (imageLoader == null)
                imageLoader = MainApp.getPermission().getImageLoader();

            mImagen = itemView.findViewById(R.id.ivItem);
            mProducto = itemView.findViewById(R.id.tvProducto);
            mDescripcion = itemView.findViewById(R.id.tvDescripcion);
        }
    }
}
