package com.appquimia.appquimia.classes;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;
import android.widget.Toast;

import com.appquimia.appquimia.AutenticacionActivity;
import com.appquimia.appquimia.BitacoraActivity;
import com.appquimia.appquimia.CalificarActivity;
import com.appquimia.appquimia.CmrCalificarActivity;
import com.appquimia.appquimia.CmrMarcaActivity;
import com.appquimia.appquimia.JuegoActivity;
import com.appquimia.appquimia.MovilActivity;
import com.appquimia.appquimia.PremiosActivity;
import com.appquimia.appquimia.StrAuspiciantesActivity;
import com.appquimia.appquimia.TarifasActivity;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.ConfiguracionTiempo;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.entities.Sucursal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 * Created by ajade on 16/03/2018.
 */

public class NavigationOptions {

    public static void opMovil(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, MovilActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opTarifador(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, TarifasActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opCalificar(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, CalificarActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opJugar(Context cntx, boolean finish) {
        if(SessionProcessor.getExistPremios(cntx)) {
            Intent intent = new Intent();
            if (SessionProcessor.getLastGameId(cntx.getApplicationContext()) == -1) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat fActual = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
                long lActual = Long.parseLong(fActual.format(calendar.getTime()));

                Bitacora bitacora = new Bitacora();
                bitacora.fillLast(DatabaseHelper.getDBReadableInstance(cntx));

                long lastGame = Long.parseLong(SessionProcessor.getLastGame(cntx));
                String lastMovil = SessionProcessor.getLastMovil(cntx);

                ConfiguracionTiempo conf = new ConfiguracionTiempo();
                conf.fill(DatabaseHelper.getDBReadableInstance(cntx));
                long timeToWait = conf.getMoviles() * 100;

                if(!Utils.DEBUG) {
                    if (bitacora.getIdmovil().equals(lastMovil) && (lActual - lastGame) < timeToWait) {
                        Utils.showMessage(cntx, "Todavía no puede volver a jugar en este móvil.", false);
                        return;
                    }
                }
                intent.setClass(cntx, JuegoActivity.class);
                cntx.startActivity(intent);
                if(finish)
                    ((Activity)cntx).finish();
            } else
                Toast.makeText(cntx.getApplicationContext(), "Ya ha jugado!!", Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(cntx.getApplicationContext(), "No hay premios disponibles", Toast.LENGTH_LONG).show();
    }

    public static void opPremios(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, PremiosActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opBitacora(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, BitacoraActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opAuspiciantes(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, StrAuspiciantesActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opJugarStreet(Context cntx, boolean finish) {
        if(SessionProcessor.getExistPremios(cntx)) {
            Intent intent = new Intent();
            if (SessionProcessor.getLastGameId(cntx.getApplicationContext()) == -1) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat fActual = new SimpleDateFormat("yyyyMMddHHmmss", java.util.Locale.getDefault());
                long lActual = Long.parseLong(fActual.format(calendar.getTime()));

                Bitacora bitacora = new Bitacora();
                bitacora.fillLast(DatabaseHelper.getDBReadableInstance(cntx));

                long lastGame = Long.parseLong(SessionProcessor.getLastGame(cntx));

                ConfiguracionTiempo conf = new ConfiguracionTiempo();
                conf.fill(DatabaseHelper.getDBReadableInstance(cntx));
                long timeToWait = conf.getMoviles() * 100;

                if ((lActual - lastGame) < timeToWait) {
                    Utils.showMessage(cntx, "Todavía no puede volver a jugar.", false);
                    return;
                }

                intent.putExtra(JuegoActivity.TIPO_MODULO, Constantes.MODULE_STREET);
                intent.setClass(cntx, JuegoActivity.class);
                cntx.startActivity(intent);
                if(finish)
                    ((Activity)cntx).finish();
            } else
                Toast.makeText(cntx.getApplicationContext(), "Ya ha jugado!!", Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(cntx.getApplicationContext(), "No hay premios disponibles", Toast.LENGTH_LONG).show();
    }

    public static void opPremiosStreet(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.putExtra(JuegoActivity.TIPO_MODULO, Constantes.MODULE_STREET);
        intent.setClass(cntx, PremiosActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opCalificarStreet(Context cntx, boolean finish) {
        try {
            cntx.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + cntx.getPackageName())));
        } catch (android.content.ActivityNotFoundException e) {
            cntx.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + cntx.getPackageName())));
        }
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opRecomentdarStreet(Context cntx, boolean finish) {
        final String appPackageName = cntx.getPackageName();
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Descargá Appquimia desde: https://play.google.com/store/apps/details?id=" + appPackageName);
        sendIntent.setType("text/plain");
        cntx.startActivity(sendIntent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opMarca(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, CmrMarcaActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opJugarCommerce(Context cntx, boolean finish) {
        if(SessionProcessor.getExistPremios(cntx)) {
            Intent intent = new Intent();
            if (SessionProcessor.getLastGameId(cntx.getApplicationContext()) == -1) {
                Calendar calendar = Calendar.getInstance();
                SimpleDateFormat fActual = new SimpleDateFormat("yyyyMMddHHmmss", java.util.Locale.getDefault());
                long lActual = Long.parseLong(fActual.format(calendar.getTime()));

                Bitacora bitacora = new Bitacora();
                bitacora.fillLast(DatabaseHelper.getDBReadableInstance(cntx));

                long lastGame = Long.parseLong(SessionProcessor.getLastGame(cntx));

                ConfiguracionTiempo conf = new ConfiguracionTiempo();
                conf.fill(DatabaseHelper.getDBReadableInstance(cntx));
                long timeToWait = conf.getMoviles() * 100;

                if(!Utils.DEBUG) {
                    if ((lActual - lastGame) < timeToWait) {
                        Utils.showMessage(cntx, "Todavía no puede volver a jugar en este local.", false);
                        return;
                    }
                }
                intent.putExtra(JuegoActivity.TIPO_MODULO, Constantes.MODULE_COMMERCE);
                intent.setClass(cntx, JuegoActivity.class);
                cntx.startActivity(intent);
                if(finish)
                    ((Activity)cntx).finish();
            } else
                Toast.makeText(cntx.getApplicationContext(), "Ya ha jugado!!", Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(cntx.getApplicationContext(), "No hay premios disponibles", Toast.LENGTH_LONG).show();
    }

    public static void opPremiosCommerce(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.putExtra(JuegoActivity.TIPO_MODULO, Constantes.MODULE_COMMERCE);
        intent.setClass(cntx, PremiosActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opCalificarCommerce(Context cntx, boolean finish) {
        Intent intent = new Intent();
        intent.setClass(cntx, CmrCalificarActivity.class);
        cntx.startActivity(intent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void opRecomentdarCommerce(Context cntx, boolean finish) {
        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(cntx));
        Sponsor sponsor = new Sponsor();
        sponsor.fillByID(bitacora.getIdsponsor(), DatabaseHelper.getDBReadableInstance(cntx));
        Sucursal sucursal = new Sucursal();
        sucursal.fillByID(bitacora.getIdsucursal(), DatabaseHelper.getDBReadableInstance(cntx));

        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, "Recomendación Appqumia:" + sponsor.getRazonsocial() + "\r\n" + "sucursal: " + sucursal.getDescripcion());
        sendIntent.setType("text/plain");
        cntx.startActivity(sendIntent);
        if(finish)
            ((Activity)cntx).finish();
    }

    public static void finish(final Context cntx) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(cntx);
        dlgAlert.setMessage("Gracias por elegirnos!!!");
        dlgAlert.setTitle("Appquimia");
        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(DatabaseHelper.getDBInstance(cntx).isOpen())
                    DatabaseHelper.getDBInstance(cntx).close();
                Intent intent = new Intent(cntx.getApplicationContext(), AutenticacionActivity.class);
                ComponentName cn = intent.getComponent();
                Intent mainIntent = Intent.makeRestartActivityTask(cn);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                mainIntent.putExtra("close", true);
                cntx.startActivity(mainIntent);
                ((Activity)cntx).finish();
            }
        });
        dlgAlert.setCancelable(true);
        Dialog dlg = dlgAlert.create();
        dlg.show();
        Activity act = (Activity) cntx;
        try {
            Typeface tf = Typeface.createFromAsset(act.getAssets(), "fonts/acuminpro_regular.otf");
            @SuppressWarnings("ConstantConditions")
            TextView alertMessage = dlg.getWindow().findViewById(android.R.id.message);
            alertMessage.setTypeface(tf);

            int textViewId = dlg.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
            if (textViewId != 0) {
                tf = Typeface.createFromAsset(act.getAssets(), "fonts/acuminpro_medium.otf");
                TextView alertTitle = dlg.getWindow().findViewById(textViewId);
                alertTitle.setTypeface(tf);
            }
        } catch (NullPointerException ignored) {}
    }
}
