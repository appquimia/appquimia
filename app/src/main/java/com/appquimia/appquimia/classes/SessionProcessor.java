package com.appquimia.appquimia.classes;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by usuario on 27/09/2017.
 */

public class SessionProcessor {
    private static final String PREFS_NAME = "com.appquimia.appquimia";

    private static final String LAST_GAME_ID = "LAST_GAME_ID";
    private static final String PREMIO_ID = "PREMIO_ID";
    private static final String LAST_GAME = "LAST_GAME";
    private static final String LAST_MOVIL = "LAST_MOVIL";
    private static final String EXIST_PREMIOS = "EXIST_PREMIOS";

    public static void setLastGameID(Context cntxt, int idgame) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(LAST_GAME_ID, String.valueOf(idgame));

        editor.commit();
    }

    public static int getLastGameId(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            return Integer.parseInt(prefs.getString(LAST_GAME_ID, "-1"));
        } catch (Exception e) {
            return 0;
        }
    }

    public static void setPremioID(Context cntxt, String idpremio) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(PREMIO_ID, idpremio);

        editor.commit();
    }

    public static String getPremioId(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            return prefs.getString(PREMIO_ID, "-1");
        } catch (Exception e) {
            return "-1";
        }
    }

    public static void setLastGame(Context cntxt, String game) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(LAST_GAME, String.valueOf(game));

        editor.commit();
    }

    public static String getLastGame(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            return prefs.getString(LAST_GAME, "");
        } catch (Exception e) {
            return "";
        }
    }

    public static void setLastMovil(Context cntxt, String movil) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putString(LAST_MOVIL, String.valueOf(movil));

        editor.commit();
    }

    public static String getLastMovil(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            return prefs.getString(LAST_MOVIL, "");
        } catch (Exception e) {
            return "";
        }
    }

    public static void setExistPremios(Context cntxt, boolean exist) {
        SharedPreferences.Editor editor = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE).edit();
        editor.putBoolean(EXIST_PREMIOS, exist);

        editor.commit();
    }

    public static boolean getExistPremios(Context cntxt) {
        SharedPreferences prefs = cntxt.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);

        try {
            return prefs.getBoolean(EXIST_PREMIOS, false);
        } catch (Exception e) {
            return false;
        }
    }
}
