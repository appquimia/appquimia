package com.appquimia.appquimia.classes;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;

import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Calificacion;
import com.appquimia.appquimia.entities.Clasificacion;
import com.appquimia.appquimia.entities.Conductor;
import com.appquimia.appquimia.entities.ConductorXMovil;
import com.appquimia.appquimia.entities.ConfiguracionTiempo;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Movil;
import com.appquimia.appquimia.entities.Paquete;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Producto;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.entities.Reclamo;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.entities.Sucursal;
import com.appquimia.appquimia.entities.Tarifa;
import com.appquimia.appquimia.entities.TarifaFija;
import com.appquimia.appquimia.entities.TiempoEspera;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;


/**
 * Created by usuario on 19/07/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper {
    // Database Info
    public static final String DATABASE_NAME = "appquimia.db";
    private static final int DATABASE_VERSION = 3;
    private static DatabaseHelper mInstance = null;
//    private static SQLiteDatabase mDBInstance = null;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public static synchronized DatabaseHelper getInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance;
    }

    public static synchronized SQLiteDatabase getDBReadableInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance.getReadableDatabase();
    }


    public static synchronized SQLiteDatabase getDBInstance(Context ctx) {
        if (mInstance == null) {
            mInstance = new DatabaseHelper(ctx.getApplicationContext());
        }
        return mInstance.getWritableDatabase();
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();
        try {
            db.execSQL(Conductor.CREATE_TABLE);
            db.execSQL(Movil.CREATE_TABLE);
            db.execSQL(Premio.CREATE_TABLE);
            db.execSQL(Sponsor.CREATE_TABLE);
            db.execSQL(Paquete.CREATE_TABLE);
            db.execSQL(Clasificacion.CREATE_TABLE);
            db.execSQL(Publicidad.CREATE_TABLE);
            db.execSQL(Tarifa.CREATE_TABLE);
            db.execSQL(TarifaFija.CREATE_TABLE);
            db.execSQL(Cuenta.CREATE_TABLE);
            db.execSQL(Bitacora.CREATE_TABLE);
            db.execSQL(ConductorXMovil.CREATE_TABLE);
            db.execSQL(Calificacion.CREATE_TABLE);
            db.execSQL(PremioXCuenta.CREATE_TABLE);
            db.execSQL(Reclamo.CREATE_TABLE);
            //db.execSQL(PremioEstado.CREATE_TABLE);
            db.execSQL(TiempoEspera.CREATE_TABLE);
            db.execSQL(ConfiguracionTiempo.CREATE_TABLE);
            db.execSQL(Producto.CREATE_TABLE);
            db.execSQL(Sucursal.CREATE_TABLE);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        db.beginTransaction();
        try {
            switch (oldVersion) {
                //case 1:
                    //db.execSQL(DATABASE_CREATE_color);
                    // we want both updates, so no break statement here...
                case 1:
                    db.execSQL(Premio.ALTER_TABLE_V2_1);
                    db.execSQL("delete from "+ Premio.TABLE_NAME);
                case 2:
                    db.execSQL(Bitacora.ALTER_TABLE_V3_1);
                    db.execSQL(Bitacora.ALTER_TABLE_V3_2);
                    db.execSQL(Bitacora.ALTER_TABLE_V3_3);
                    db.execSQL(Calificacion.ALTER_TABLE_V3_1);
                    db.execSQL(Calificacion.ALTER_TABLE_V3_2);
                    db.execSQL(Calificacion.ALTER_TABLE_V3_3);
                    db.execSQL(Calificacion.ALTER_TABLE_V3_4);
                    db.execSQL(Calificacion.ALTER_TABLE_V3_5);
                    db.execSQL(ConfiguracionTiempo.ALTER_TABLE_V3_1);
                    db.execSQL(Movil.ALTER_TABLE_V3_1);
                    db.execSQL(Movil.ALTER_TABLE_V3_2);
                    db.execSQL(Producto.ALTER_TABLE_V3_1);
                    db.execSQL(Sponsor.ALTER_TABLE_V3_1);
                    db.execSQL(Sponsor.ALTER_TABLE_V3_2);
                    db.execSQL(Sponsor.ALTER_TABLE_V3_3);
                    db.execSQL(Sponsor.ALTER_TABLE_V3_4);
                    db.execSQL(Sucursal.ALTER_TABLE_V3_1);
            }
            db.setTransactionSuccessful();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

//        if (oldVersion != newVersion)
//        {
//            // Simplest implementation is to drop all old tables and recreate them
//            db.beginTransaction();
//            try {
//                if (oldVersion < 2) {
//                    db.execSQL(Premio.ALTER_TABLE_V2_1);
//                }
//            } catch (Exception e) {
//                Log.e("Actualización", e.getMessage());
//            }
//            db.setTransactionSuccessful();
//            db.endTransaction();
//        }
    }

    public static void backup(Context ctx) {
        try {
            File dbFile = ctx.getDatabasePath(DATABASE_NAME);
            if(!dbFile.exists()) {
                return;
            }
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = Environment.getExternalStorageDirectory()+ "/" + DATABASE_NAME + ".bck";

            // Open the empty db as the output stream
            OutputStream output;
            output = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0){
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void restore(Context ctx) {
        try {
            File dbFile = new File(Environment.getExternalStorageDirectory()+ "/" + DATABASE_NAME + ".bck");//ctx.getDatabasePath(DATABASE_NAME+ ".bck");
            if(!dbFile.exists()) {
                return;
            }
            FileInputStream fis = new FileInputStream(dbFile);

            String outFileName = ctx.getDatabasePath(DATABASE_NAME+ ".bck").getPath();

            // Open the empty db as the output stream
            OutputStream output = new FileOutputStream(outFileName);
            byte[] buffer = new byte[1024];
            int length;
            while ((length = fis.read(buffer)) > 0){
                output.write(buffer, 0, length);
            }
            output.flush();
            output.close();
            fis.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
