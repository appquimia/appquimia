package com.appquimia.appquimia.classes;

/**
 * Created by ajade on 30/07/2018.
 */

public class Constantes {
    public static final int MODULE_MOVILES = 0;
    public static final int MODULE_COMMERCE = 1;
    public static final int MODULE_STREET = 2;

    public static String MODULE_PREFIX_MOVILES = "Appquimia";
    public static String MODULE_PREFIX_COMMERCE = "Commerce";
    public static String MODULE_PREFIX_STREET = "Street";
}
