package com.appquimia.appquimia.classes;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;
import android.widget.TextView;

import com.appquimia.appquimia.DashboardActivity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by usuario on 07/10/2017.
 */

public class Utils {
    public static final boolean DEBUG = false;
    public static final boolean TEST = false;

    public static String longToDate(String lngDate) {
        Date date;
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        SimpleDateFormat dfout = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss", new Locale("es", "AR"));
        String out;
        try {
            date = df.parse(String.valueOf(lngDate));
            out = dfout.format(date);
        } catch (ParseException e) {
            throw new RuntimeException("Failed to parse date: ", e);
        }

        return out;
    }

    public static String dateToLong(String lngDate, String format) {
        Date date;
        SimpleDateFormat dfOut = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        SimpleDateFormat dfIn = new SimpleDateFormat(format, new Locale("es", "AR"));
        String out;
        try {
            date = dfIn.parse(String.valueOf(lngDate));
            out = dfOut.format(date);
        } catch (ParseException e) {
            throw new RuntimeException("Failed to parse date: ", e);
        }

        return out;
    }

    public static String dateTimerEncode(String lngDate, String format) {
        Date date;
        if (String.valueOf(lngDate).length() < 19)
            lngDate += " 00:00:00";

        SimpleDateFormat dfOut = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        SimpleDateFormat dfIn = new SimpleDateFormat(format, new Locale("es", "AR"));
        String out;
        try {
            date = dfIn.parse(String.valueOf(lngDate));
            out = dfOut.format(date);
        } catch (ParseException e) {
            throw new RuntimeException("Failed to parse date: ", e);
        }

        return out;
    }

    public static int mod(int x, int y) {
        int result = x % y;
        return result < 0? result + y : result;
    }

    public static void showMessage(final Context cntx, String mensaje, Boolean salir) {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(cntx);
        dlgAlert.setMessage(mensaje);
        dlgAlert.setTitle("Appquimia");
        if(!salir) {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
        } else {
            dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
//                    Intent intent = new Intent(cntx, DashboardActivity.class);
//                    cntx.startActivity(intent);
                    ((Activity) cntx).finish();
                }
            });
        }
        dlgAlert.setCancelable(false);
        Activity act = (Activity) cntx;
        if(!act.isFinishing()) {
            Dialog dlg = dlgAlert.create();
            dlg.show();

            try {
                Typeface tf = Typeface.createFromAsset(act.getAssets(), "fonts/acuminpro_regular.otf");
                @SuppressWarnings("ConstantConditions")
                TextView alertMessage = dlg.getWindow().findViewById(android.R.id.message);
                alertMessage.setTypeface(tf);

                int textViewId = dlg.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
                if (textViewId != 0) {
                    tf = Typeface.createFromAsset(act.getAssets(), "fonts/acuminpro_medium.otf");
                    TextView alertTitle = dlg.getWindow().findViewById(textViewId);
                    alertTitle.setTypeface(tf);
                }
            } catch (NullPointerException ignored) {}
        }
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(ByteArrayOutputStream buffer, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(buffer.toByteArray(), 0, buffer.toByteArray().length, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(buffer.toByteArray(), 0, buffer.toByteArray().length, options);
    }

    public static Point getScreenSize(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point screenSize = new Point();
        display.getSize(screenSize);

        return screenSize;
    }

    public static void adjustFontScale(Context context, Configuration configuration) {
        if (configuration.fontScale != 1) {
            configuration.fontScale = 1;
            DisplayMetrics metrics = context.getResources().getDisplayMetrics();
            WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
            wm.getDefaultDisplay().getMetrics(metrics);
            metrics.scaledDensity = configuration.fontScale * metrics.density;
            context.getResources().updateConfiguration(configuration, metrics);
        }
    }

//    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
//        // Raw height and width of image
//        final int height = options.outHeight;
//        final int width = options.outWidth;
//        int inSampleSize = 1;
//
//        if (height > reqHeight || width > reqWidth) {
//
//            final int halfHeight = height / 2;
//            final int halfWidth = width / 2;
//
//            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
//            // height and width larger than the requested height and width.
//            while ((halfHeight / inSampleSize) >= reqHeight
//                    && (halfWidth / inSampleSize) >= reqWidth) {
//                inSampleSize *= 2;
//            }
//        }
//
//        return inSampleSize;
//    }

//    public static Bitmap decodeSampledBitmapFromResource(ByteArrayOutputStream buffer, int reqWidth, int reqHeight) {
//        final BitmapFactory.Options options = new BitmapFactory.Options();
//        options.inJustDecodeBounds = true;
//        BitmapFactory.decodeByteArray(buffer.toByteArray(), 0, buffer.toByteArray().length, options);
//        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
//
//        options.inJustDecodeBounds = false;
//        return BitmapFactory.decodeByteArray(buffer.toByteArray(), 0, buffer.toByteArray().length, options);
//    }
}
