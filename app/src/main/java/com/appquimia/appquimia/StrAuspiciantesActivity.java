package com.appquimia.appquimia;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.appquimia.appquimia.adapters.CarouselAdapter;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Sponsor;

import java.util.ArrayList;

public class StrAuspiciantesActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private RecyclerView.LayoutManager mlmSponsors;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_str_auspiciantes);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(R.id.dlStrAuspiciantes);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nvStrAuspiciantes);
        mNavigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = mNavigationView.getMenu();
        mNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                StrAuspiciantesActivity.this.mNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    StrAuspiciantesActivity.this.mNavigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_str_auspiciantes);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBInstance(this));

        int displayWidth = Utils.getScreenSize(this).x;
        int displayHeight = Utils.getScreenSize(this).y;

        createLayoutManager();
        RecyclerView rvDestacados = findViewById(R.id.rvDestacados);
        rvDestacados.setHasFixedSize(true);
        rvDestacados.setLayoutManager(mlmSponsors);
        ArrayList<Sponsor> arrSponsors = Sponsor.getList(DatabaseHelper.getDBInstance(this));
        RecyclerView.Adapter adpSponsors = new CarouselAdapter(arrSponsors);
        ((CarouselAdapter) adpSponsors).setWidth(displayWidth);
        ((CarouselAdapter) adpSponsors).setHeight(displayHeight);
        rvDestacados.addItemDecoration(new DividerItemDecoration(this, 0));
        rvDestacados.setAdapter(adpSponsors);

    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_auspiciantes) {
            //NavigationOptions.opAuspiciantes(this, true);
        }  else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugarStreet(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremiosStreet(this, true);
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificarStreet(this, false);
        } else if (id == R.id.nav_recomendar) {
            NavigationOptions.opRecomentdarStreet(this, false);
        } else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    private void createLayoutManager() {
        mlmSponsors = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
    }
}
