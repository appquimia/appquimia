package com.appquimia.appquimia.enums;

/**
 * Created by ajade on 20/12/2017.
 */

public enum PremioEstados {
    Unknow,
    Pendiente,
    Canjeado,
    Reclamado,
    Vencido,
}
