package com.appquimia.appquimia;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Calificacion;
import com.appquimia.appquimia.enums.TipoAppquimia;

import java.util.ArrayList;
import java.util.UUID;

public class CmrCalificarActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {
    private DrawerLayout mDrawer;
    private RatingBar mRatAtencion;
    private RatingBar mRatLocal;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_cmr_calificar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(R.id.dlCmrCalificar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nvCmrCalificar);
        mNavigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = mNavigationView.getMenu();
        mNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                CmrCalificarActivity.this.mNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    CmrCalificarActivity.this.mNavigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_cmr_calificar);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ImageButton btnGuardar = findViewById(R.id.btnGuardar);
        mRatAtencion = findViewById(R.id.rbAtencion);
        mRatLocal = findViewById(R.id.rbLocal);

        btnGuardar.setOnClickListener(this);

        mRatAtencion.setNumStars(5);
        mRatAtencion.setMax(5);
        mRatAtencion.setStepSize(1.0F);

        mRatLocal.setNumStars(5);
        mRatLocal.setMax(5);
        mRatLocal.setStepSize(1.0F);

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBInstance(CmrCalificarActivity.this));

        Calificacion calificacion = new Calificacion();
        calificacion.fillByID(bitacora.getId(), DatabaseHelper.getDBInstance(CmrCalificarActivity.this));

        if (calificacion.getId().length() > 0) {
            mRatAtencion.setEnabled(false);
            mRatLocal.setEnabled(false);
            mRatAtencion.setRating(calificacion.getEstrellasconductor());
            mRatLocal.setRating(calificacion.getEstrellasmovil());
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_marca) {
            NavigationOptions.opMarca(this, true);
        } else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugarCommerce(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremiosCommerce(this, true);
        } else if (id == R.id.nav_calificar) {
//            NavigationOptions.opCalificarCommerce(this, false);
        } else if (id == R.id.nav_recomendar) {
            NavigationOptions.opRecomentdarCommerce(this, false);
        } else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnGuardar) {
            Bitacora bitacora = new Bitacora();
            bitacora.fillLast(DatabaseHelper.getDBInstance(CmrCalificarActivity.this));

            Calificacion calificacion = new Calificacion();
            calificacion.fillByID(bitacora.getId(), DatabaseHelper.getDBInstance(CmrCalificarActivity.this));

            DatabaseHelper.getDBInstance(CmrCalificarActivity.this).enableWriteAheadLogging();

            if (calificacion.getId().length() == 0) {
                String uuid = UUID.randomUUID().toString();

                calificacion.setIdcuenta(bitacora.getIdcuenta());
                calificacion.setId(uuid);
                calificacion.setIdsponsor(bitacora.getIdsponsor());
                calificacion.setIdlocal(bitacora.getIdsucursal());
                calificacion.setTipoappquimia(TipoAppquimia.Commerce.ordinal());
                calificacion.setEstrellasatencion((int) mRatAtencion.getRating());
                calificacion.setEstrellaslocal((int) mRatLocal.getRating());
                calificacion.setFecha(bitacora.getFecha());
                calificacion.setIdbitacora(bitacora.getIdcuenta());
                calificacion.setSync(0);
                calificacion.save(DatabaseHelper.getDBInstance(CmrCalificarActivity.this));
            }
            showMessage();
        }
        else
            finish();
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    private void showMessage() {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("Su calificación se envió correctamente. Muchas gracias!!");
        dlgAlert.setTitle("Calificar");
        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                finish();
            }
        });
        dlgAlert.setCancelable(true);
        dlgAlert.create().show();
    }
}
