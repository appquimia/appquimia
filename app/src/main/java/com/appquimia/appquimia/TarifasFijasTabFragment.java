package com.appquimia.appquimia;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.entities.TarifaFija;
import com.appquimia.appquimia.entities.TiempoEspera;

import java.util.List;
import java.util.Locale;


public class TarifasFijasTabFragment extends Fragment {
    private static final String ARG_PARAM1 = "titulo";
    private static final String ARG_PARAM2 = "posicion";

    private String mTitulo;

    DatabaseHelper mDBHelper;
    SQLiteDatabase mDatabase;

    TarifasFijasAdapter mAdapter;

    public TarifasFijasTabFragment() {
    }

    public static TarifasFijasTabFragment newInstance(String param1, String param2) {
        TarifasFijasTabFragment fragment = new TarifasFijasTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitulo = getArguments().getString(ARG_PARAM1);
//            String mPosicion = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tarifas_fijas_tab, container, false);

//        mDBHelper = DatabaseHelper.getInstance(getActivity());
//        mDatabase = mDBHelper.getWritableDatabase();
//        mDatabase.enableWriteAheadLogging();

        TiempoEspera tiempo = new TiempoEspera();
        tiempo.fill(DatabaseHelper.getDBReadableInstance(this.getActivity()));
        TextView mCosto = view.findViewById(R.id.tvTiempoEspera);
        String costo = "Costo tiempo de espera: " + String.format(new Locale("es", "AR"), "$ %.2f", tiempo.getCosto())  + " el minuto.";
        mCosto.setText(costo);

        ListView mTarifasFijas = view.findViewById(R.id.lstDestinosFijos);
        List<TarifaFija> mArr = TarifaFija.getList(DatabaseHelper.getDBReadableInstance(this.getActivity()));
        mAdapter = new TarifasFijasAdapter(getActivity(), R.id.lstDestinosFijos, mArr);
        mTarifasFijas.setAdapter(mAdapter);
//        mDatabase.close();

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public String getTitulo() {
        return mTitulo;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    private class TarifasFijasAdapter extends ArrayAdapter<TarifaFija> {
        protected List<TarifaFija> items;

        TarifasFijasAdapter(Context context, int textViewResourceId, List<TarifaFija> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public TarifaFija getItem(int pos) {
            return items.get(pos);
        }

        @SuppressLint("InflateParams")
        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                @SuppressWarnings("ConstantConditions")
                LayoutInflater vi = (LayoutInflater) TarifasFijasTabFragment.this.getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert vi != null;
                v = vi.inflate(R.layout.item_list_destino_fijo, null);
            }

            TarifaFija t = items.get(position);

            if (t != null) {
                TextView f1 = v.findViewById(R.id.tvDestino);
                f1.setText(t.getDestino());
                TextView f2 = v.findViewById(R.id.tvCosto);

                String formatted = String.format(new Locale("es", "AR"), "$ %.2f", t.getPrecio());
                f2.setText(formatted);
            }
            return v;
        }
    }
}
