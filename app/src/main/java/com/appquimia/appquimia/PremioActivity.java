package com.appquimia.appquimia;

import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Reclamo;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.enums.PremioEstados;
import com.appquimia.appquimia.enums.ReclamoEstado;

import java.io.ByteArrayInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;

public class PremioActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    public static final String TIPO_MODULO = "tipo_modulo";
    static String IDPREMIO_TO_CASH = "idpremio";

    private ImageView mImagen;
    private TextView mSponsor;
    private TextView mDescripcion;
    private TextView mDireccion;
    private TextView mCodigo;
    private TextView mFecha;
    private TextView mEstado;
    private TextView mTelefono;

    private DrawerLayout mDrawer;

    PremioXCuenta mPCuentaEnt;
    Premio mPremioEnt;
    Sponsor mSponsorEnt;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());

        int dl = R.id.dlPremio;
        int nv = R.id.nvPremio;

        switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
            case Constantes.MODULE_MOVILES:
                setContentView(R.layout.activity_premio);
                dl = R.id.dlPremio;
                nv = R.id.nvPremio;
                break;
            case Constantes.MODULE_COMMERCE:
                setContentView(R.layout.activity_cmr_premio);
                dl = R.id.dlCmrPremio;
                nv = R.id.nvCmrPremio;
                break;
            case Constantes.MODULE_STREET:
                setContentView(R.layout.activity_str_premio);
                dl = R.id.dlStrPremio;
                nv = R.id.nvStrPremio;
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        // Sección acttion bar, toolbar, navbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(dl);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        if(getIntent().getBooleanExtra(PremiosActivity.FROM_LOGIN, false)) {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.syncState();
        }

        navigationView = findViewById(nv);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                PremioActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    PremioActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_premio);
        }

        // Sección comportamiento particular
        mImagen = findViewById(R.id.imgPremio);
        mSponsor = findViewById(R.id.tvSponsor);
        mDescripcion = findViewById(R.id.tvDescripcion);
        mDireccion = findViewById(R.id.tvDireccion);
        mCodigo = findViewById(R.id.tvCodigo);
        mFecha = findViewById(R.id.tvFecha);
        mEstado = findViewById(R.id.tvEstado);
        mTelefono = findViewById(R.id.tvTelefono);
        Button canjear = findViewById(R.id.btnCanjear);
        Button reclamar = findViewById(R.id.btnReclamar);

        canjear.setOnClickListener(this);
        reclamar.setOnClickListener(this);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_MOVILES) {
            if (id == R.id.nav_movil) {
                NavigationOptions.opMovil(this, true);
            } else if (id == R.id.nav_tarifador) {
                NavigationOptions.opTarifador(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificar(this, true);
            } else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugar(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremios(this, true);
            } else if (id == R.id.nav_bitacora) {
                NavigationOptions.opBitacora(this, true);
            }else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else  if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_COMMERCE) {
//            if (id == R.id.nav_marca) {
//                NavigationOptions.opMarca(this, true);
//            }  else if (id == R.id.nav_jugar) {
//                NavigationOptions.opJugarStreet(this, true);
//            } else if (id == R.id.nav_premios) {
//                NavigationOptions.opPremiosCommerce(this, true);
//            } else if (id == R.id.nav_calificar) {
//                NavigationOptions.opCalificarCommerce(this, true);
//            } else if (id == R.id.nav_recomendar) {
//                NavigationOptions.opRecomentdarCommerce(this, false);
//            } else if (id == R.id.nav_fin) {
//                NavigationOptions.finish(this);
//            }
        } else {
            if (id == R.id.nav_auspiciantes) {
                NavigationOptions.opAuspiciantes(this, true);
            }  else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosStreet(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        String mIdPremio = SessionProcessor.getPremioId(getApplicationContext());
//        mDBHelper = DatabaseHelper.getInstance(this);
//        mDatabase = mDBHelper.getWritableDatabase();
//        mDatabase.enableWriteAheadLogging();

        mPCuentaEnt = new PremioXCuenta();
        mPCuentaEnt.fillByID(mIdPremio, DatabaseHelper.getDBReadableInstance(this));

        mPremioEnt = new Premio();
        mPremioEnt.fillByID(mPCuentaEnt.getIdpremio(), DatabaseHelper.getDBReadableInstance(this));

        mSponsorEnt = new Sponsor();
        mSponsorEnt.fillByID(mPremioEnt.getIdsponsor(), DatabaseHelper.getDBReadableInstance(this));
//        mDatabase.close();
        fillData();
    }

    private void fillData() {
        Bitmap theImage;
        if(mPremioEnt.getImagenb() != null) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(mPremioEnt.getImagenb());
            theImage = BitmapFactory.decodeStream(imageStream);
        } else {
            theImage = BitmapFactory.decodeResource(getResources(), R.drawable.premio);
        }

        mImagen.setImageBitmap(theImage);

        mSponsor.setText(mSponsorEnt.getRazonsocial());
        mDescripcion.setText(mPremioEnt.getDescripcion());
        Spanned text = Html.fromHtml("<b>Dirección de canjeo: </b>" + mPremioEnt.getDireccion());
        mDireccion.setText(text);
        text = Html.fromHtml("<b>Código del premio: </b>" + mPremioEnt.getCodigo());
        mCodigo.setText(text);
        String estado = PremioEstados.values()[mPCuentaEnt.getIdestado()].name();
        text = Html.fromHtml("<b>Estado: </b>" + estado);
        mEstado.setText(text);

        text = Html.fromHtml("<b>Teléfono: </b>" + mSponsorEnt.getTelefono());
        mTelefono.setText(text);

        SimpleDateFormat fInfo = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        SimpleDateFormat fFecha = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss", new Locale("es", "AR"));


        try {
            Date date = fInfo.parse(mPCuentaEnt.getFecpremio());
            text = Html.fromHtml("<b>Fecha: </b>" + fFecha.format(date));
            mFecha.setText(text);
        } catch (ParseException e) {
            text = Html.fromHtml("<b>Fecha: </b>");
            mFecha.setText(text);
            e.printStackTrace();
        }

    }

    @Override
    public void onClick(View view)
    {
        if(view.getId() == R.id.btnReclamar)
            reclamarPremio();
        else if(view.getId() == R.id.btnCanjear)
            canjearPremio();
    }

    private void canjearPremio()
    {
        if(mPCuentaEnt.getIdestado() != PremioEstados.Canjeado.ordinal()
                && mPCuentaEnt.getIdestado() != PremioEstados.Vencido.ordinal())
        {
            Intent intent = new Intent();
            intent.putExtra(PremioActivity.IDPREMIO_TO_CASH, mPremioEnt.getId());
            intent.setClass(PremioActivity.this, CanjeoActivity.class);

            if(getIntent().getBooleanExtra(PremiosActivity.FROM_LOGIN, false)) {
                intent.putExtra(PremiosActivity.FROM_LOGIN, true);
            } else {
                switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
                    case Constantes.MODULE_MOVILES:
                        intent.putExtra(CanjeoActivity.TIPO_MODULO, Constantes.MODULE_MOVILES);
                        break;
                    case Constantes.MODULE_COMMERCE:
                        intent.putExtra(CanjeoActivity.TIPO_MODULO, Constantes.MODULE_COMMERCE);
                        break;
                    case Constantes.MODULE_STREET:
                        intent.putExtra(CanjeoActivity.TIPO_MODULO, Constantes.MODULE_STREET);
                        break;
                }
            }
            startActivity(intent);
        }
    }

    private void reclamarPremio() {
        if(PremioEstados.Pendiente.ordinal() == mPCuentaEnt.getIdestado()) {
            Calendar calendar = Calendar.getInstance();
            SimpleDateFormat fFecha = new SimpleDateFormat("yyyy-MM-dd", new Locale("es", "AR"));
            SimpleDateFormat fHora = new SimpleDateFormat("HH:mm:ss", new Locale("es", "AR"));
            String sFecha = fFecha.format(calendar.getTime());
            String sHora = fHora.format(calendar.getTime());

            Reclamo reclamo = new Reclamo();
            reclamo.setId(UUID.randomUUID().toString());
            reclamo.setIdpremio(mPCuentaEnt.getIdpremio());
            int recestado = ReclamoEstado.pendiente.ordinal();
            reclamo.setIdestado(recestado);
            reclamo.setIdcuenta(mPCuentaEnt.getIdcuenta());
            reclamo.setFecha(sFecha);
            reclamo.setHora(sHora);
            reclamo.setSync(0);

            mPCuentaEnt.setIdestado(PremioEstados.Reclamado.ordinal());

            reclamo.save(DatabaseHelper.getInstance(this).getWritableDatabase());
            mPCuentaEnt.save(DatabaseHelper.getInstance(this).getWritableDatabase());

            Utils.showMessage(this, "Se ha enviado su reclamo a Appquimia. Muchas gracias.", true);

            String estado = PremioEstados.values()[mPCuentaEnt.getIdestado()].name();
            mEstado.setText(estado);
        } else if(PremioEstados.Pendiente.ordinal() != mPCuentaEnt.getIdestado()) {
            Utils.showMessage(this,"Sólo se pueden reclamar premios en estado pendiente.", false);
        }
    }
}
