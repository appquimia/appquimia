package com.appquimia.appquimia;

import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Calificacion;
import com.appquimia.appquimia.entities.ConductorXMovil;

import java.util.ArrayList;

public class CalificarActivity extends AppCompatActivity implements OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private RatingBar mRatConductor;
    private RatingBar mRatMovil;

//    DatabaseHelper mDBHelper;
//    SQLiteDatabase mDatabase;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_calificar);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.dlCalificar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nvCalificar);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                CalificarActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    CalificarActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_calificar);
        }

        // Sección comportamiento particular
        ImageButton mGuardar = findViewById(R.id.btnGuardar);
        mRatConductor = findViewById(R.id.rbConductor);
        mRatMovil = findViewById(R.id.rbMovil);

        mGuardar.setOnClickListener(this);

        mRatConductor.setNumStars(5);
        mRatConductor.setMax(5);
        mRatConductor.setStepSize(1.0F);

        mRatMovil.setNumStars(5);
        mRatMovil.setMax(5);
        mRatMovil.setStepSize(1.0F);

//        mDBHelper = DatabaseHelper.getInstance(this);
//        mDatabase = mDBHelper.getWritableDatabase();
//        mDatabase.enableWriteAheadLogging();

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(this));

        Calificacion calificacion = new Calificacion();
        calificacion.fillByID(bitacora.getId(), DatabaseHelper.getDBReadableInstance(this));

        if(calificacion.getId().length() > 0) {
            mRatConductor.setEnabled(false);
            mRatMovil.setEnabled(false);
            mRatConductor.setRating(calificacion.getEstrellasconductor());
            mRatMovil.setRating(calificacion.getEstrellasmovil());
        }

//        mDatabase.close();
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_movil) {
            NavigationOptions.opMovil(this, true);
        } else if (id == R.id.nav_tarifador) {
            NavigationOptions.opTarifador(this, true);
        } else if (id == R.id.nav_calificar) {
            //NavigationOptions.opCalificar(this, true);
        } else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugar(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremios(this, true);
        } else if (id == R.id.nav_bitacora) {
            NavigationOptions.opBitacora(this, true);
        }else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        DrawerLayout drawer = findViewById(R.id.dlCalificar);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnGuardar) {
//            mDBHelper = DatabaseHelper.getInstance(this);
//            mDatabase = mDBHelper.getWritableDatabase();
//            mDatabase.enableWriteAheadLogging();

            Bitacora bitacora = new Bitacora();
            bitacora.fillLast(DatabaseHelper.getDBReadableInstance(this));

            Calificacion calificacion = new Calificacion();
            calificacion.fillByID(bitacora.getId(), DatabaseHelper.getDBReadableInstance(this));

            ArrayList array = null;
            try {
                array = ConductorXMovil.getConductoresByIDMovil(bitacora.getIdmovil(), DatabaseHelper.getDBReadableInstance(this));
            } catch (Exception e) {
                e.printStackTrace();
            }
            assert array != null;
            ConductorXMovil cxm = (ConductorXMovil) array.get(0);

            if(calificacion.getId().length() == 0) {
                calificacion.setIdcuenta(bitacora.getIdcuenta());
                calificacion.setId(bitacora.getId());
                calificacion.setEstrellasconductor((int) mRatConductor.getRating());
                calificacion.setEstrellasmovil((int) mRatMovil.getRating());
                calificacion.setFecha(bitacora.getFecha());
                calificacion.setIdbitacora(bitacora.getIdcuenta());
                calificacion.setIdmovil(bitacora.getIdmovil());
                calificacion.setIdconductor(cxm.getIdconductor());
                calificacion.setSync(0);
                calificacion.save(DatabaseHelper.getDBInstance(this));
                if(DatabaseHelper.getDBInstance(this).isOpen())
                    DatabaseHelper.getDBInstance(this).close();
            }
            Utils.showMessage(this, "Su calificación se envió correctamente. Muchas gracias!!", true);
        }
        else
            finish();
    }
}
