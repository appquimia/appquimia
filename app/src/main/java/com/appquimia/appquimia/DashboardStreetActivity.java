package com.appquimia.appquimia;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.CustomTFSpan;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Calificacion;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Movil;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.entities.Reclamo;
import com.appquimia.appquimia.services.CalificacionService;
import com.appquimia.appquimia.services.CuentaService;
import com.appquimia.appquimia.services.PremioXCuentaService;
import com.appquimia.appquimia.services.ReclamoService;
import com.appquimia.appquimia.services.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DashboardStreetActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    static boolean active = false;
    long mLastGame;
    private boolean mHayPremios;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_street);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = mNavigationView.getMenu();
        mNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                DashboardStreetActivity.this.mNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    DashboardStreetActivity.this.mNavigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent target = getIntent();
        mLastGame = Long.parseLong(target.getStringExtra("LastGame"));

        SessionProcessor.setLastGame(this, mLastGame + "");

        ImageView mAvatarUsuario = findViewById(R.id.imgAvatar) ;

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));

        Picasso.get()
                .load(cuenta.getImagen())
                .into(mAvatarUsuario);

        TextView mUsuario = findViewById(R.id.tvUsuario);
        mUsuario.setText(cuenta.getNombre());

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));

        mHayPremios = Premio.getPremiosByIDPaquete(0, DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this)).size() > 0;
        SessionProcessor.setExistPremios(this, mHayPremios);

        ListView mLista = findViewById(R.id.recOpciones);
        List<String> mArrLogs = Arrays.asList("Auspiciantes", "Jugar", "Premios", "Calificar Appquimia", "Recomendar Appquimia", "Finalizar");
        OpcionesAdapter mAdapter = new OpcionesAdapter(this, R.id.recOpciones, mArrLogs);
        mLista.setAdapter(mAdapter);
        mLista.setOnItemClickListener(new ItemClickListener());

        active = true;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                syncCuenta();
                syncReclamos();
                syncCalificaciones();
                syncPremios();
                syncCanjeos();
                if(active)
                    handler.postDelayed(this, 36000);
            }
        }, 500);

        final Handler hdPublicidad = new Handler();
        hdPublicidad.postDelayed(new Runnable() {
            public void run() {
                DashboardStreetActivity.this.
                        showPublicidad(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));
                if(active)
                    hdPublicidad.postDelayed(this, 10000);
            }
        }, 500);
    }

    @Override
    protected void  onPause() {
        active = false;
        System.gc();
        deleteRecursive(this.getCacheDir());
        super.onPause();
    }

    @Override
    protected void onStop(){
        active = false;
        System.gc();
        deleteRecursive(this.getCacheDir());
        super.onStop();
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return super.onCreateOptionsMenu(menu);

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_auspiciantes) {
            NavigationOptions.opAuspiciantes(this, false);
        } else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugarStreet(this, false);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremiosStreet(this, false);
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificarStreet(this, false);
        } else if (id == R.id.nav_recomendar) {
            NavigationOptions.opRecomentdarStreet(this, false);
        }else if (id == R.id.nav_fin) {
            showByeMessage();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showPublicidad(SQLiteDatabase db) {
        Publicidad pub1 = new Publicidad();
        Publicidad pub2 = new Publicidad();
        Random randomGenerator = new Random();
        float randomInt = randomGenerator.nextInt(100) / 100;
        pub1.fill(-1, randomInt, db);
        randomInt = randomGenerator.nextInt(100) / 100;
        pub2.fill(pub1.getId(), randomInt, db);

        ByteArrayInputStream imageStream;

        if(pub1.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub1.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice1);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }

        if(pub2.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub2.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice2);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        try {
            if (fileOrDirectory.isDirectory() && fileOrDirectory.getCanonicalPath().equals(this.getCacheDir().getCanonicalPath()))
                return;
        } catch (IOException e) {
            e.printStackTrace();
        }

        fileOrDirectory.delete();
    }

    private class ItemClickListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    // opAuspiciantes();
                    NavigationOptions.opAuspiciantes(DashboardStreetActivity.this, false);
                    break;
                case 1:
//                    opJugar();
                    NavigationOptions.opJugarStreet(DashboardStreetActivity.this, false);
                    break;
                case 2:
//                    opPremios();
                    NavigationOptions.opPremiosStreet(DashboardStreetActivity.this, false);
                    break;
                case 3:
//                    opCalificar();
                    NavigationOptions.opCalificarStreet(DashboardStreetActivity.this, false);
                    break;
                case 4:
//                    opRecomendar();
                    NavigationOptions.opRecomentdarStreet(DashboardStreetActivity.this, false);
                    break;
                case 5:
                    showByeMessage();
                    break;
            }
        }
    }

    private void showByeMessage() {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("Gracias por elegirnos!!!");
        dlgAlert.setTitle("Appquimia");
        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(DatabaseHelper.getDBInstance(DashboardStreetActivity.this).isOpen())
                    DatabaseHelper.getDBInstance(DashboardStreetActivity.this).close();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAndRemoveTask();
                } else {
                    finish();
                    System.exit(0);
                }
            }
        });
        dlgAlert.setCancelable(true);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
        CustomTFSpan tfSpan = new CustomTFSpan(tf);
        SpannableString spannableString = new SpannableString("Appquimia");
        spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        dlgAlert.setTitle(spannableString);

        Dialog dlg = dlgAlert.create();
        dlg.show();

        try {
            tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_regular.otf");
            @SuppressWarnings("ConstantConditions")
            TextView alertMessage = dlg.getWindow().findViewById(android.R.id.message);
            alertMessage.setTypeface(tf);
        } catch (NullPointerException ignored) {}
    }

    private class OpcionesAdapter extends ArrayAdapter<String> {
        protected List<String> items;

        OpcionesAdapter(Context context, int textViewResourceId, List<String> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public String getItem(int pos)
        {
            return items.get(pos);
        }

        @SuppressLint("InflateParams")
        @SuppressWarnings("ConstantConditions")
        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View v = convertView;
            if (v == null)
            {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if(vi != null) {
                    v = vi.inflate(R.layout.item_list_opciones, null);
                }
            }

            String s = items.get(position);
            if (s != null && v != null) {
                View line = v.findViewById(R.id.line1);
                line.setVisibility((position != 0) ? (View.INVISIBLE) : (View.VISIBLE));

                TextView f1 = v.findViewById(R.id.tvOpcionTag);
                f1.setText(s);
            }
            return v;
        }
    }

    //*********************************
    //***   Sinconización de datos  ***
    //*********************************
    public void syncCuenta() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));

        HashMap<String, String> map = cuenta.getHashMap();

        // Crear nuevo objeto Json basado en el mapa
        JSONObject jobject = new JSONObject(map);
        // Depurando objeto Json...
        Log.d(this.getLocalClassName(), jobject.toString());

        // Actualizar datos en el servidor
        mVolley.addToRequestQueue(
                new JsonObjectRequest(
                        Request.Method.POST,
                        CuentaService.API_CALL,
                        jobject,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                // Procesar la respuesta del servidor
                                Toast.makeText(DashboardStreetActivity.this, "Cuenta sincronizada",Toast.LENGTH_SHORT).show();
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                if(Utils.DEBUG)
                                    Toast.makeText(DashboardStreetActivity.this, "Sin conexión",Toast.LENGTH_SHORT).show();
                            }
                        }
                )
                {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
        );
    }

    public void syncReclamos() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());

        Reclamo reclamo = new Reclamo();
        ArrayList<Reclamo> arrReclamos = reclamo.getNotSync(DatabaseHelper.getDBReadableInstance(this));

        for (Reclamo r : arrReclamos) {

            HashMap<String, String> map = r.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                    new JsonObjectRequest(
                            Request.Method.POST,
                            ReclamoService.API_CALL,
                            jobject,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    // Procesar la respuesta del servidor
                                    Toast.makeText(DashboardStreetActivity.this, "Cuenta sincronizada", Toast.LENGTH_SHORT).show();

                                    try {
                                        Reclamo reclamo = new Reclamo();
                                        reclamo.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));
                                        reclamo.setSync(1);
                                        reclamo.save(DatabaseHelper.getDBInstance(DashboardStreetActivity.this));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(Utils.DEBUG)
                                        Toast.makeText(DashboardStreetActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            headers.put("Accept", "application/json");
                            return headers;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8" + getParamsEncoding();
                        }
                    }
            );
        }
    }

    public void syncCalificaciones() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());
        Calificacion calificacion = new Calificacion();
        ArrayList<Calificacion> arrCalif = null;
        try {
            arrCalif = calificacion.getNotSync(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        assert arrCalif != null;
        for (Calificacion c : arrCalif) {
            HashMap<String, String> map = c.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                    new JsonObjectRequest(
                            Request.Method.POST,
                            CalificacionService.API_CALL,
                            jobject,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        Calificacion calif = new Calificacion();
                                        calif.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));
                                        calif.setSync(1);
                                        calif.save(DatabaseHelper.getDBInstance(DashboardStreetActivity.this));

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(Utils.DEBUG)
                                        Toast.makeText(DashboardStreetActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            headers.put("Accept", "application/json");
                            return headers;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8" + getParamsEncoding();
                        }
                    }
            );
        }
    }

    public void syncPremios() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());
        PremioXCuenta premio = new PremioXCuenta();
        ArrayList<PremioXCuenta> arrReclamos = premio.getNotSync(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));

        for (PremioXCuenta r : arrReclamos) {

            HashMap<String, String> map = r.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                    new JsonObjectRequest(
                            Request.Method.POST, PremioXCuentaService.API_CALL,
                            jobject,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        PremioXCuenta pxc = new PremioXCuenta();
                                        pxc.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));
                                        pxc.setSync(1);
                                        pxc.save(DatabaseHelper.getDBInstance(DashboardStreetActivity.this));//mDatabase);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(Utils.DEBUG)
                                        Toast.makeText(DashboardStreetActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            headers.put("Accept", "application/json");
                            return headers;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8" + getParamsEncoding();
                        }
                    }
            );
        }
    }

    public void syncCanjeos() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());
        PremioXCuenta premio = new PremioXCuenta();
        ArrayList<PremioXCuenta> arrPremios = premio.getCanjeadosNotSync(DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));

        for (PremioXCuenta r : arrPremios) {

            HashMap<String, String> map = r.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());
            String customUrl =  PremioXCuentaService.API_CALL + "/" + r.getId();

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                    new JsonObjectRequest(
                            Request.Method.PUT,
                            customUrl,
                            jobject,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        PremioXCuenta pxc = new PremioXCuenta();
                                        pxc.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardStreetActivity.this));
                                        pxc.setSync(1);
                                        pxc.save(DatabaseHelper.getDBInstance(DashboardStreetActivity.this));//mDatabase);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } finally {
                                        if(DatabaseHelper.getDBInstance(DashboardStreetActivity.this).isOpen())
                                            DatabaseHelper.getDBInstance(DashboardStreetActivity.this).close();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(Utils.DEBUG)
                                        Toast.makeText(DashboardStreetActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            headers.put("Accept", "application/json");
                            return headers;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8" + getParamsEncoding();
                        }
                    }
            );
        }
    }

}
