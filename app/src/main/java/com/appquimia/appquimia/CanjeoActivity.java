package com.appquimia.appquimia;

import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.enums.PremioEstados;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class CanjeoActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener  {
    public static final String TIPO_MODULO = "tipo_modulo";
    private EditText mCodigo;

    private DrawerLayout mDrawer;

    PremioXCuenta mPCuentaEnt;
    Premio mPremioEnt;
    Sponsor mSponsorEnt;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        int dl = R.id.dlCanjeo;
        int nv = R.id.nvCanjeo;

        switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
            case Constantes.MODULE_MOVILES:
                setContentView(R.layout.activity_canjeo);
                dl = R.id.dlCanjeo;
                nv = R.id.nvCanjeo;
                break;
            case Constantes.MODULE_COMMERCE:
                setContentView(R.layout.activity_cmr_canjeo);
                dl = R.id.dlCmrCanjeo;
                nv = R.id.nvCmrCanjeo;
                break;
            case Constantes.MODULE_STREET:
                setContentView(R.layout.activity_str_canjeo);
                dl = R.id.dlStrCanjeo;
                nv = R.id.nvStrCanjeo;
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        // Sección acttion bar, toolbar, navbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(dl);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        if(getIntent().getBooleanExtra(PremiosActivity.FROM_LOGIN, false)) {
            mDrawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.onDrawerStateChanged(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            toggle.setDrawerIndicatorEnabled(false);
            toggle.syncState();
        }

        navigationView = findViewById(nv);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                CanjeoActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    CanjeoActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_canjeo);
        }

        // Sección comportamiento particular
        Button canjear = findViewById(R.id.btnCanjear);
        mCodigo = findViewById(R.id.etCodigo);

        canjear.setOnClickListener(this);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_MOVILES) {
            if (id == R.id.nav_movil) {
                NavigationOptions.opMovil(this, true);
            } else if (id == R.id.nav_tarifador) {
                NavigationOptions.opTarifador(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificar(this, true);
            } else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugar(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremios(this, true);
            } else if (id == R.id.nav_bitacora) {
                NavigationOptions.opBitacora(this, true);
            }else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else  if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_COMMERCE) {
            if (id == R.id.nav_marca) {
                NavigationOptions.opMarca(this, true);
            }  else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosCommerce(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarCommerce(this, true);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarCommerce(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else {
            if (id == R.id.nav_auspiciantes) {
                NavigationOptions.opAuspiciantes(this, true);
            }  else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosStreet(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnCanjear) {
            String mIdPremio = SessionProcessor.getPremioId(getApplicationContext());
//            mDBHelper = DatabaseHelper.getInstance(this);
//            mDatabase = mDBHelper.getWritableDatabase();
//            mDatabase.enableWriteAheadLogging();

            mPCuentaEnt = new PremioXCuenta();
            mPCuentaEnt.fillByID(mIdPremio, DatabaseHelper.getDBReadableInstance(this));

            mPremioEnt = new Premio();
            mPremioEnt.fillByID(mPCuentaEnt.getIdpremio(), DatabaseHelper.getDBReadableInstance(this));

            mSponsorEnt = new Sponsor();
            mSponsorEnt.fillByID(mPremioEnt.getIdsponsor(), DatabaseHelper.getDBReadableInstance(this));

            String codigo = mCodigo.getText().toString();
            if(codigo.equals(mSponsorEnt.getCodigocanjeo())) {
                Date todayDate = Calendar.getInstance().getTime();
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                String todayString = formatter.format(todayDate);
                mPCuentaEnt.setFeccanjeo(todayString);
                mPCuentaEnt.setSync(0);
                mPCuentaEnt.setIdestado(PremioEstados.Canjeado.ordinal());
                mPCuentaEnt.save(DatabaseHelper.getDBInstance(this));
                Utils.showMessage(this, "El premio se registró como canjeado.", true);
            }
            else {
                Utils.showMessage(this, "El código no es correcto. Por favor intente de nuevo.", false);
            }
        }
    }
}
