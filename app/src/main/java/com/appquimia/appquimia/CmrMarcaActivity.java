package com.appquimia.appquimia;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.entities.Sucursal;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class CmrMarcaActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mDrawer;
    Sponsor mEntSponsor;
    private ImageView mImagen;
    private TextView mSponsor;
    private TextView mDescripcion;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_cmr_marca);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(R.id.dlCmrMarca);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nvCmrMrca);
        mNavigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = mNavigationView.getMenu();
        mNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                CmrMarcaActivity.this.mNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    CmrMarcaActivity.this.mNavigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_cmr_marca);
        }

        mImagen = findViewById(R.id.imgLogo);
        mSponsor = findViewById(R.id.tvSponsor);
        mDescripcion = findViewById(R.id.tvDescripcion);

        Button productos = findViewById(R.id.btnProductos);
        productos.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBInstance(this));
        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(CmrMarcaActivity.this));

        mEntSponsor = new Sponsor();
        mEntSponsor.fillByID(bitacora.getIdsponsor(), DatabaseHelper.getDBReadableInstance(CmrMarcaActivity.this));

        fillData();
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnProductos) {
            Intent intent = new Intent();
            intent.setClass(CmrMarcaActivity.this, CmrProductosActivity.class);
            startActivity(intent);
        }
    }

    private void fillData() {
        Bitmap theImage;
        if(mEntSponsor.getImagenb() != null) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(mEntSponsor.getImagenb());
            theImage = BitmapFactory.decodeStream(imageStream);
        } else {
            theImage = BitmapFactory.decodeResource(getResources(), R.drawable.sponsor_default);
        }

        mImagen.setImageBitmap(theImage);

        mSponsor.setText(mEntSponsor.getRazonsocial());
        mDescripcion.setText(mEntSponsor.getDescripcion());

        ListView mSucursales = findViewById(R.id.lstSucursales);
        List<Sucursal> mArr = Sucursal.getListByIdSponsor(mEntSponsor.getId(), DatabaseHelper.getDBReadableInstance(CmrMarcaActivity.this));
        SucursaleAdapter mAdapter = new SucursaleAdapter(this, R.id.lstSucursales, mArr);
        mSucursales.setAdapter(mAdapter);
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_marca) {
//            NavigationOptions.opMarca(this, true);
        }  else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugarCommerce(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremiosCommerce(this, true);
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificarCommerce(this, false);
        } else if (id == R.id.nav_recomendar) {
            NavigationOptions.opRecomentdarCommerce(this, false);
        } else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    private class SucursaleAdapter extends ArrayAdapter<Sucursal> {
        protected List<Sucursal> items;

        public SucursaleAdapter(Context context, int textViewResourceId, List<Sucursal> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public Sucursal getItem(int pos) {
            return items.get(pos);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                v = vi.inflate(R.layout.item_list_sucursal, null);
            }

            Sucursal s = items.get(position);
            if (s != null) {
                TextView f1 = v.findViewById(R.id.tvSucursal);
                f1.setText(s.getSucursal());
                TextView f2 = v.findViewById(R.id.tvDireccion);
                f2.setText(s.getDireccion());
            }

            return v;
        }
    }
}
