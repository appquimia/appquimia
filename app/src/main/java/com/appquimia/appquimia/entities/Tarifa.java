package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 09/10/2017.
 */

public class Tarifa extends EntityAbstract
{
    public static final String TABLE_NAME = "tarifas";
    public static final String CREATE_TABLE = "CREATE TABLE [tarifas]([id] BIGINT UNIQUE, [fecha] VARCHAR2, [valor] DECIMAL, [kms] DECIMAL);";

    private long id;
    private String fecha;
    private double valor;
    private double kms;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public double getValor()
    {
        return valor;
    }

    public void setValor(double valor)
    {
        this.valor = valor;
    }

    public double getKms()
    {
        return kms;
    }

    public void setKms(double kms)
    {
        this.kms = kms;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("fecha", fecha);
        contentValues.put("valor", valor);
        contentValues.put("kms", kms);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public static ArrayList<Tarifa> getList(SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        ArrayList<Tarifa> mArr = new ArrayList<Tarifa>();
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE kms > -1 ORDER BY kms;", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                Tarifa item = new Tarifa();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.kms = cur.getDouble(cur.getColumnIndex("kms"));
                item.valor = cur.getDouble(cur.getColumnIndex("valor"));
                item.fecha = cur.getString(cur.getColumnIndex("fecha"));
                mArr.add(item);
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mArr;
    }

    public static Tarifa getBase(SQLiteDatabase db) {
        Tarifa item = null;
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE kms = -1;", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                item = new Tarifa();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.kms = cur.getDouble(cur.getColumnIndex("kms"));
                item.valor = cur.getDouble(cur.getColumnIndex("valor"));
                item.fecha = cur.getString(cur.getColumnIndex("fecha"));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            item = null;
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return item;
    }
}
