package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by usuario on 09/10/2017.
 */

public class Calificacion extends EntityAbstract {
    public static final String TABLE_NAME = "calificaciones";
    public static final String CREATE_TABLE = "CREATE TABLE [calificaciones](" +
            "[id] VARCHAR2 UNIQUE, [idcuenta] VARCHAR2, [estrellasconductor] INT, [fecha] VARCHAR2, " +
            "[estrellasmovil] INT, [idmovil] VARCHAR, [idbitacora] VARCHAR2, [idconductor] VARCHAR, " +
            "[tipoappquimia] INT, [idsponsor] BIGINT, [idlocal] VARCHAR2, [estrellasatencion] INT, " +
            "[estrellaslocal] INT, [sync] INT);";

    public static final String ALTER_TABLE_V3_1 = "ALTER TABLE calificaciones ADD COLUMN [idsponsor] BIGINT DEFAULT '';";
    public static final String ALTER_TABLE_V3_2 = "ALTER TABLE calificaciones ADD COLUMN [tipoappquimia] INT DEFAULT '';";
    public static final String ALTER_TABLE_V3_3 = "ALTER TABLE calificaciones ADD COLUMN [idlocal] VARCHAR2 DEFAULT '';";
    public static final String ALTER_TABLE_V3_4 = "ALTER TABLE calificaciones ADD COLUMN [estrellasatencion] INT DEFAULT '';";
    public static final String ALTER_TABLE_V3_5 = "ALTER TABLE calificaciones ADD COLUMN [estrellaslocal] INT DEFAULT '';";

    private String id;
    private String idcuenta;
    private int estrellasconductor;
    private String fecha;
    private int estrellasmovil;
    private String idmovil;
    private String idbitacora;
    private String idconductor;
    private int tipoappquimia;
    private long idsponsor;
    private String idlocal;
    private int estrellasatencion;
    private int estrellaslocal;
    private int sync;

    public Calificacion() {
        id = "";
        idlocal = "";
        idmovil = "";
        idconductor = "";
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

//    public String getIdcuenta()
//    {
//        return idcuenta;
//    }

    public void setIdcuenta(String idcuenta)
    {
        this.idcuenta = idcuenta;
    }

    public int getEstrellasconductor()
    {
        return estrellasconductor;
    }

    public void setEstrellasconductor(int estrellasconductor) {
        this.estrellasconductor = estrellasconductor;
    }

//    public String getFecha()
//    {
//        return fecha;
//    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public int getEstrellasmovil()
    {
        return estrellasmovil;
    }

    public void setEstrellasmovil(int estrellasmovil)
    {
        this.estrellasmovil = estrellasmovil;
    }

    public String getIdmovil()
    {
        return idmovil;
    }

    public void setIdmovil(String idmovil)
    {
        this.idmovil = idmovil;
    }

//    public String getIdbitacora()
//    {
//        return idbitacora;
//    }

    public void setIdbitacora(String idbitacora)
    {
        this.idbitacora = idbitacora;
    }

//    public String getIdconductor() {
//        return idconductor;
//    }

    public void setIdconductor(String idconductor) {
        this.idconductor = idconductor;
    }

//    public int getTipoappquimia() {
//        return tipoappquimia;
//    }

    public void setTipoappquimia(int tipoappquimia) {
        this.tipoappquimia = tipoappquimia;
    }

    public long getIdsponsor() {
        return idsponsor;
    }

    public void setIdsponsor(long idsponsor) {
        this.idsponsor = idsponsor;
    }

//    public String getIdlocal() {
//        return idlocal;
//    }

    public void setIdlocal(String idlocal) {
        this.idlocal = idlocal;
    }

//    public int getEstrellasatencion() {
//        return estrellasatencion;
//    }

    public void setEstrellasatencion(int estrellasatencion) {
        this.estrellasatencion = estrellasatencion;
    }

//    public int getEstrellaslocal() {
//        return estrellaslocal;
//    }

    public void setEstrellaslocal(int estrellaslocal) {
        this.estrellaslocal = estrellaslocal;
    }

//    public int getSync() {
//        return sync;
//    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("idcuenta", idcuenta);
        contentValues.put("estrellasconductor", estrellasconductor);
        contentValues.put("fecha", fecha);
        contentValues.put("estrellasmovil", estrellasmovil);
        contentValues.put("idmovil", idmovil);
        contentValues.put("idbitacora", idbitacora);
        contentValues.put("idconductor", idconductor);
        contentValues.put("tipoappquimia", tipoappquimia);
        contentValues.put("idsponsor", idsponsor);
        contentValues.put("idlocal", idlocal);
        contentValues.put("estrellasatencion", estrellasatencion);
        contentValues.put("estrellaslocal", estrellaslocal);
        contentValues.put("sync", sync);

        return super.save(db, TABLE_NAME, contentValues, this.id);
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fillLast(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY fecha DESC LIMIT 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                this.setEstrellasconductor(cur.getInt(cur.getColumnIndex("estrellasconductor")));
                this.setFecha(cur.getString(cur.getColumnIndex("fecha")));
                this.setEstrellasmovil(cur.getInt(cur.getColumnIndex("estrellasmovil")));
                this.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                this.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                this.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                this.setTipoappquimia(cur.getInt(cur.getColumnIndex("tipoappquimia")));
                this.setIdsponsor(cur.getInt(cur.getColumnIndex("idsponsor")));
                this.setIdlocal(cur.getString(cur.getColumnIndex("idlocal")));
                this.setEstrellasatencion(cur.getInt(cur.getColumnIndex("estrellasatencion")));
                this.setEstrellaslocal(cur.getInt(cur.getColumnIndex("estrellaslocal")));
                this.setSync(cur.getInt(cur.getColumnIndex("sync")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fillByID(String id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                this.setEstrellasconductor(cur.getInt(cur.getColumnIndex("estrellasconductor")));
                this.setFecha(cur.getString(cur.getColumnIndex("fecha")));
                this.setEstrellasmovil(cur.getInt(cur.getColumnIndex("estrellasmovil")));
                this.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                this.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                this.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                this.setTipoappquimia(cur.getInt(cur.getColumnIndex("tipoappquimia")));
                this.setIdsponsor(cur.getInt(cur.getColumnIndex("idsponsor")));
                this.setIdlocal(cur.getString(cur.getColumnIndex("idlocal")));
                this.setEstrellasatencion(cur.getInt(cur.getColumnIndex("estrellasatencion")));
                this.setEstrellaslocal(cur.getInt(cur.getColumnIndex("estrellaslocal")));
                this.setSync(cur.getInt(cur.getColumnIndex("sync")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public ArrayList<Calificacion> getNotSync(SQLiteDatabase db) throws Exception {
        ArrayList<Calificacion> arr = new ArrayList<>();
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE sync = 0", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                Calificacion c = new Calificacion();
                c.setId(cur.getString(cur.getColumnIndex("id")));
                c.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                c.setEstrellasconductor(cur.getInt(cur.getColumnIndex("estrellasconductor")));
                c.setFecha(cur.getString(cur.getColumnIndex("fecha")));
                c.setEstrellasmovil(cur.getInt(cur.getColumnIndex("estrellasmovil")));
                c.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                c.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                c.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                c.setTipoappquimia(cur.getInt(cur.getColumnIndex("tipoappquimia")));
                c.setIdsponsor(cur.getInt(cur.getColumnIndex("idsponsor")));
                c.setIdlocal(cur.getString(cur.getColumnIndex("idlocal")));
                c.setEstrellasatencion(cur.getInt(cur.getColumnIndex("estrellasatencion")));
                c.setEstrellaslocal(cur.getInt(cur.getColumnIndex("estrellaslocal")));
                c.setSync(cur.getInt(cur.getColumnIndex("sync")));
                arr.add(c);
                cur.moveToNext();
            }
        } catch (Exception e) {
            throw new Exception(e);
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id);
        map.put("idcuenta", idcuenta);
        map.put("estrellasconductor", estrellasconductor + "");
        map.put("fecha", fecha);
        map.put("estrellasmovil", estrellasmovil + "");
        map.put("idmovil", idmovil + "");
        map.put("idbitacora", idbitacora);
        map.put("idconductor", idconductor + "");
        map.put("tipoappquimia", tipoappquimia + "");
        map.put("idsponsor", idsponsor + "");
        map.put("idlocal", idlocal);
        map.put("estrellasatencion", estrellasatencion + "");
        map.put("estrellaslocal", estrellaslocal + "");
        return map;
    }
}
