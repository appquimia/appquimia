package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.enums.PremioEstados;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by usuario on 27/09/2017.
 */

public class PremioXCuenta extends EntityAbstract
{
    public static final String TABLE_NAME = "premiosxcuenta";
    public static final String CREATE_TABLE = "CREATE TABLE [premiosxcuenta](" +
            "[id] VARCHAR2 UNIQUE, [idpremio] BIGINT, [idcuenta] VARCHAR2, [fecpremio] VARCHAR2, " +
            "[fecven] VARCHAR2, [idestado] INT, [idmovil] VARCHAR2, [idbitacora] VARCHAR2, " +
            "[feccanjeo] VARCHAR2, [idconductor] VARCHAR2, [sync] INT);";

    private String id;
    private long idpremio;
    private String idcuenta;
    private String fecpremio;
    private String fecven;
    private int idestado;
    private String idmovil;
    private String idbitacora;
    private String feccanjeo;
    private String idconductor;
    private int sync;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public long getIdpremio()
    {
        return idpremio;
    }

    public void setIdpremio(long idpremio)
    {
        this.idpremio = idpremio;
    }

    public String getIdcuenta()
    {
        return idcuenta;
    }

    public void setIdcuenta(String idcuenta)
    {
        this.idcuenta = idcuenta;
    }

    public String getFecpremio()
    {
        return fecpremio;
    }

    public void setFecpremio(String fecpremio)
    {
        this.fecpremio = fecpremio;
    }

    public String getFecven()
    {
        return fecven;
    }

    public void setFecven(String fecven)
    {
        this.fecven = fecven;
    }

    public int getIdestado()
    {
        return idestado;
    }

    public void setIdestado(int idestado)
    {
        this.idestado = idestado;
    }

    public String getIdmovil()
    {
        return idmovil;
    }

    public void setIdmovil(String idmovil)
    {
        this.idmovil = idmovil;
    }

//    public String getIdbitacora()
//    {
//        return idbitacora;
//    }

    public void setIdbitacora(String idbitacora)
    {
        this.idbitacora = idbitacora;
    }

//    public String getFeccanjeo() {
//        return feccanjeo;
//    }

    public void setFeccanjeo(String feccanjeo) {
        this.feccanjeo = feccanjeo;
    }

//    public String getIdconductor() {
//        return idconductor;
//    }

    public void setIdconductor(String idconductor) {
        this.idconductor = idconductor;
    }

//    public int getSync() {
//        return sync;
//    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.id);
        contentValues.put("idpremio", this.idpremio);
        contentValues.put("idcuenta", this.idcuenta);
        contentValues.put("fecpremio", this.fecpremio);
        contentValues.put("fecven", this.fecven);
        contentValues.put("idestado", this.idestado);
        contentValues.put("idmovil", this.idmovil);
        contentValues.put("idbitacora", this.idbitacora);
        contentValues.put("feccanjeo", this.feccanjeo);
        contentValues.put("idconductor", this.idconductor);
        contentValues.put("sync", sync);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fillByID(String id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idpremio = cur.getLong(cur.getColumnIndex("idpremio"));
                this.idcuenta = cur.getString(cur.getColumnIndex("idcuenta"));
                this.fecpremio = cur.getString(cur.getColumnIndex("fecpremio"));
                this.fecven = cur.getString(cur.getColumnIndex("fecven"));
                this.idestado = cur.getInt(cur.getColumnIndex("idestado"));
                this.idmovil = cur.getString(cur.getColumnIndex("idmovil"));
                this.idbitacora = cur.getString(cur.getColumnIndex("idbitacora"));
                this.feccanjeo = cur.getString(cur.getColumnIndex("feccanjeo"));
                this.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                this.setSync(cur.getInt(cur.getColumnIndex("sync")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

//    public static ArrayList<PremioXCuenta> getListByIdEstado(long idestado, SQLiteDatabase db) {
//        String mess = "";
//        Cursor cur = null;
//        ArrayList<PremioXCuenta> arr = new ArrayList<PremioXCuenta>();
//        try {
//            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idestado = " + idestado, null);
//            cur.moveToFirst();
//            while (!cur.isAfterLast()) {
//                PremioXCuenta item = new PremioXCuenta();
//                item.setId(cur.getString(cur.getColumnIndex("id")));
//                item.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
//                item.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
//                item.setFecpremio(cur.getString(cur.getColumnIndex("fecpremio")));
//                item.setFecven(cur.getString(cur.getColumnIndex("fecven")));
//                item.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
//                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
//                item.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
//                item.setFeccanjeo(cur.getString(cur.getColumnIndex("feccanjeo")));
//                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
//                item.setSync(cur.getInt(cur.getColumnIndex("sync")));
//
//                arr.add(item);
//                cur.moveToNext();
//            }
//        } catch (Exception e) {
//            mess = e.getMessage();
//        } finally {
//            if (cur != null)
//                cur.close();
//        }
//
//        return arr;
//    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public static ArrayList<PremioXCuenta> getListByIdCuenta(String idcuenta, SQLiteDatabase db) {
        Cursor cur = null;
        ArrayList<PremioXCuenta> arr = new ArrayList<>();
        try {
            cur = db.rawQuery("SELECT x.* " +
                    "FROM premiosxcuenta x " +
                    "LEFT JOIN premios p ON p.id = x.idpremio " +
                    "WHERE x.idcuenta = '" + idcuenta + "' " +
                        "AND (x.fecven > date('now') " +
                        "AND p.fecven > date('now'))", null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                PremioXCuenta item = new PremioXCuenta();
                item.setId(cur.getString(cur.getColumnIndex("id")));
                item.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
                item.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                item.setFecpremio(cur.getString(cur.getColumnIndex("fecpremio")));
                item.setFecven(cur.getString(cur.getColumnIndex("fecven")));
                item.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                item.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                item.setFeccanjeo(cur.getString(cur.getColumnIndex("feccanjeo")));
                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                item.setSync(cur.getInt(cur.getColumnIndex("sync")));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            arr = new ArrayList<>();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public static ArrayList<PremioXCuenta> getListByIdBitacora(String idbitacora, SQLiteDatabase db) {
        Cursor cur = null;
        ArrayList<PremioXCuenta> arr = new ArrayList<>();
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idbitacora = '" + idbitacora + "'", null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                PremioXCuenta item = new PremioXCuenta();
                item.setId(cur.getString(cur.getColumnIndex("id")));
                item.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
                item.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                item.setFecpremio(cur.getString(cur.getColumnIndex("fecpremio")));
                item.setFecven(cur.getString(cur.getColumnIndex("fecven")));
                item.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                item.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                item.setFeccanjeo(cur.getString(cur.getColumnIndex("feccanjeo")));
                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                item.setSync(cur.getInt(cur.getColumnIndex("sync")));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            arr = new ArrayList<>();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public static void vencerPremios(SQLiteDatabase db) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat fActual = new SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault());
        long lActual = Long.parseLong(fActual.format(calendar.getTime()));

        ArrayList<PremioXCuenta> arr = getPendientes(db);
        for(PremioXCuenta p : arr) {
            String sFecha = Utils.dateToLong(p.getFecven(), "yyyy-MM-dd");
            long lVen = Long.parseLong(sFecha);
            if(lActual > lVen) {
                p.setIdestado(PremioEstados.Vencido.ordinal());
                p.save(db);
            }
        }
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    private static ArrayList<PremioXCuenta> getPendientes(SQLiteDatabase db) {
        Cursor cur = null;
        ArrayList<PremioXCuenta> arr = new ArrayList<>();
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idestado = " + PremioEstados.Pendiente.ordinal(), null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                PremioXCuenta item = new PremioXCuenta();
                item.setId(cur.getString(cur.getColumnIndex("id")));
                item.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
                item.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                item.setFecpremio(cur.getString(cur.getColumnIndex("fecpremio")));
                item.setFecven(cur.getString(cur.getColumnIndex("fecven")));
                item.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                item.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                item.setFeccanjeo(cur.getString(cur.getColumnIndex("feccanjeo")));
                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                item.setSync(cur.getInt(cur.getColumnIndex("sync")));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            arr = new ArrayList<>();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public ArrayList<PremioXCuenta> getNotSync(SQLiteDatabase db) {
        ArrayList<PremioXCuenta> arr = new ArrayList<>();
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE sync = 0 AND idestado=" + PremioEstados.Pendiente.ordinal(), null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                PremioXCuenta item = new PremioXCuenta();
                item.setId(cur.getString(cur.getColumnIndex("id")));
                item.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
                item.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                item.setFecpremio(cur.getString(cur.getColumnIndex("fecpremio")));
                item.setFecven(cur.getString(cur.getColumnIndex("fecven")));
                item.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                item.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                item.setFeccanjeo(cur.getString(cur.getColumnIndex("feccanjeo")));
                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                item.setSync(cur.getInt(cur.getColumnIndex("sync")));
                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            arr = new ArrayList<>();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public ArrayList<PremioXCuenta> getCanjeadosNotSync(SQLiteDatabase db) {
        ArrayList<PremioXCuenta> arr = new ArrayList<>();
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE sync = 0 AND idestado = " + PremioEstados.Canjeado.ordinal(), null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                PremioXCuenta item = new PremioXCuenta();
                item.setId(cur.getString(cur.getColumnIndex("id")));
                item.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
                item.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                item.setFecpremio(cur.getString(cur.getColumnIndex("fecpremio")));
                item.setFecven(cur.getString(cur.getColumnIndex("fecven")));
                item.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                item.setIdbitacora(cur.getString(cur.getColumnIndex("idbitacora")));
                item.setFeccanjeo(cur.getString(cur.getColumnIndex("feccanjeo")));
                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                item.setSync(cur.getInt(cur.getColumnIndex("sync")));
                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            arr = new ArrayList<>();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", id);
        map.put("idpremio", this.idpremio + "");
        map.put("idcuenta", this.idcuenta);
        map.put("fecpremio", this.fecpremio);
        map.put("fecven", this.fecven);
        map.put("idestado", this.idestado + "");
        map.put("idmovil", this.idmovil);
        map.put("idbitacora", this.idbitacora);
        map.put("feccanjeo", this.feccanjeo);
        map.put("idconductor", this.idconductor );
        map.put("feccanjeocond", "" );
        map.put("idestadoprexcond", "" );
        return map;
    }
}
