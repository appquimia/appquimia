package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashMap;

/**
 * Created by usuario on 20/12/2017.
 */

public class PremioEstado extends EntityAbstract
{
    public static final String TABLE_NAME = "premiosestados";
    public static final String CREATE_TABLE = "CREATE TABLE [premiosestados](" +
            "[id] INT PRIMARY KEY UNIQUE, " +
            "[estado] VARCHAR2);";

    private int id;
    private String estado;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("estado", estado);

        String mess = "";

        try
        {
            int id = (int) db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                db.update(TABLE_NAME, contentValues, "id=?", new String[] {this.id + ""});
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }

        return mess;
    }

    public String fill(SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " LIMIT 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                id = cur.getInt(cur.getColumnIndex("id"));
                estado = cur.getString(cur.getColumnIndex("estado"));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public String fillByID(long id, SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.id = cur.getInt(cur.getColumnIndex("id"));
                this.estado = cur.getString(cur.getColumnIndex("estado"));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }
}
