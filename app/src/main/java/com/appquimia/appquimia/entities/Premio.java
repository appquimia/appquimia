package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appquimia.appquimia.classes.DatabaseHelper;

import java.util.ArrayList;

/**
 * Created by usuario on 27/09/2017.
 */

public class Premio extends EntityAbstract
{
    public static final String TABLE_NAME = "premios";
    public static final String CREATE_TABLE = "CREATE TABLE premios ([id] BIGINT UNIQUE, " +
            "[idsponsor] BIGINT, [descripcion] varchar, [direccion] VARCHAR2, [fecinicio] VARCHAR2, " +
            "[fecfin] VARCHAR2, [idpaquete] BIGINT, [idclasificacion] BIGINT, [imagen] VARCHAR2, " +
            "[fecven] VARCHAR2, [codigo] VARCHAR2, [fecmodificacion] VARCHAR2, [imagenb] BLOB);";

    public static final String ALTER_TABLE_V2_1 = "ALTER TABLE premios ADD COLUMN [direccion] VARCHAR2 DEFAULT '';";

    private long id;
    private long idsponsor;
    private String descripcion;
    private String direccion;
    private String fecinicio;
    private String fecfin;
    private long idpaquete;
    private long idclasificacion;
    private String imagen;
    private String fecven;
    private String codigo;
    private String fecmodificacion;
    private byte[] imagenb;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public long getIdsponsor()
    {
        return idsponsor;
    }

    public void setIdsponsor(long idsponsor)
    {
        this.idsponsor = idsponsor;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getFecinicio()
    {
        return fecinicio;
    }

    public void setFecinicio(String fecinicio)
    {
        this.fecinicio = fecinicio;
    }

    public String getFecfin()
    {
        return fecfin;
    }

    public void setFecfin(String fecfin)
    {
        this.fecfin = fecfin;
    }

    public long getIdpaquete()
    {
        return idpaquete;
    }

    public void setIdpaquete(long idpaquete)
    {
        this.idpaquete = idpaquete;
    }

    public long getIdclasificacion()
    {
        return idclasificacion;
    }

    public void setIdclasificacion(long idclasificacion)
    {
        this.idclasificacion = idclasificacion;
    }

    public String getImagen()
    {
        return imagen;
    }

    public void setImagen(String imagen)
    {
        this.imagen = imagen;
    }

    public String getFecven()
    {
        return fecven;
    }

    public void setFecven(String fecven)
    {
        this.fecven = fecven;
    }

    public String getCodigo()
    {
        return codigo;
    }

    public void setCodigo(String codigo)
    {
        this.codigo = codigo;
    }

    public String getFecmodificacion() {
        return fecmodificacion;
    }

    public void setFecmodificacion(String fecmodificacion) {
        this.fecmodificacion = fecmodificacion;
    }

    public byte[] getImagenb()
    {
        return imagenb;
    }

    public void setImagenb(byte[] imagenb)
    {
        this.imagenb = imagenb;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("idsponsor", idsponsor);
        contentValues.put("descripcion", descripcion);
        contentValues.put("direccion", direccion);
        contentValues.put("fecinicio", fecinicio);
        contentValues.put("fecfin", fecfin);
        contentValues.put("idpaquete", idpaquete);
        contentValues.put("idclasificacion", idclasificacion);
        contentValues.put("imagen", imagen);
        contentValues.put("fecven", fecven);
        contentValues.put("codigo", codigo);
        contentValues.put("fecmodificacion", fecmodificacion);
        contentValues.put("imagenb", imagenb);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public static ArrayList<Premio> getPremiosByIDPaquete(long idpaquete, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        ArrayList<Premio> arr = new ArrayList<Premio>();
        try
        {
            String query = "SELECT id, idsponsor, descripcion, direccion, fecinicio, fecfin, idpaquete, idclasificacion, imagen, fecven, codigo, fecmodificacion FROM " + TABLE_NAME
                    + " WHERE DATE(fecinicio) < DATE('now') AND DATE(fecfin) > DATE('now') AND DATE('now') < DATE(fecven)";
//            String query = "SELECT id, idsponsor, descripcion, fecinicio, fecfin, idpaquete, idclasificacion, imagen, fecven, codigo, fecmodificacion FROM " + TABLE_NAME
//                    + " WHERE DATE(fecinicio) < DATE('now') AND DATE(fecfin) > DATE('now') AND DATE('now') < DATE(fecven)";

            if(idpaquete != 0)
                query += " AND idpaquete = " + idpaquete;

            query += " ORDER BY RANDOM();";
            cur = db.rawQuery(query, null);
            cur.moveToFirst();
            while (!cur.isAfterLast())
            {
                Premio item = new Premio();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                item.descripcion = cur.getString(cur.getColumnIndex("descripcion"));
                item.direccion = cur.getString(cur.getColumnIndex("direccion"));
                item.fecinicio = cur.getString(cur.getColumnIndex("fecinicio"));
                item.fecfin = cur.getString(cur.getColumnIndex("fecfin"));
                item.idpaquete = cur.getLong(cur.getColumnIndex("idpaquete"));
                item.idclasificacion = cur.getLong(cur.getColumnIndex("idclasificacion"));
                item.imagen = cur.getString(cur.getColumnIndex("imagen"));
                item.fecven = cur.getString(cur.getColumnIndex("fecven"));
                item.codigo = cur.getString(cur.getColumnIndex("codigo"));
                item.fecmodificacion = cur.getString(cur.getColumnIndex("fecmodificacion"));
               // item.imagenb = cur.getBlob(cur.getColumnIndex("imagenb"));

                arr.add(item);
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public String fillByID(long id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = " + id, null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.id = cur.getLong(cur.getColumnIndex("id"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.descripcion = cur.getString(cur.getColumnIndex("descripcion"));
                this.direccion = cur.getString(cur.getColumnIndex("direccion"));
                this.fecinicio = cur.getString(cur.getColumnIndex("fecinicio"));
                this.fecfin = cur.getString(cur.getColumnIndex("fecfin"));
                this.idpaquete = cur.getLong(cur.getColumnIndex("idpaquete"));
                this.idclasificacion = cur.getLong(cur.getColumnIndex("idclasificacion"));
                this.imagen = cur.getString(cur.getColumnIndex("imagen"));
                this.fecven = cur.getString(cur.getColumnIndex("fecven"));
                this.codigo = cur.getString(cur.getColumnIndex("codigo"));
                this.fecmodificacion = cur.getString(cur.getColumnIndex("fecmodificacion"));
                this.imagenb = cur.getBlob(cur.getColumnIndex("imagenb"));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public static ArrayList<Premio> getPremiosByJuegoID(int idGame) {
        ArrayList<Premio> arr = new ArrayList<>();
        return arr;
    }

    public static String getLastFechaModif(SQLiteDatabase db) {
        String fecmodif = "0000-00-00 00:00:00";
        Cursor cur = null;

        try
        {
            cur = db.rawQuery("SELECT fecmodificacion FROM " + TABLE_NAME + " ORDER BY fecmodificacion DESC LIMIT 1", null);
            cur.moveToFirst();
            if(!cur.isAfterLast())
            {
                fecmodif = cur.getString(cur.getColumnIndex("fecmodificacion"));
            }
        }
        catch (Exception e)
        {
            fecmodif = "0000-00-00";
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return fecmodif;
    }

    public static void clearTable(SQLiteDatabase db) {
        try
        {
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e) {}
    }

    public static boolean checkData(SQLiteDatabase db) {
        boolean check = false;
        Cursor cur = null;

        try {
            cur = db.rawQuery("SELECT COUNT(*) AS errores FROM premios WHERE LENGTH(imagenb) IS NULL", null);
            cur.moveToFirst();
            if(!cur.isAfterLast()) {
                check = (cur.getInt(cur.getColumnIndex("errores")) == 0);
            }
        } catch (Exception e) {
            check = false;
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return check;
    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Premio) obj).id;
    }
}
