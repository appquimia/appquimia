package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by usuario on 09/10/2017.
 */

public class Conductor extends EntityAbstract {
    public static final String TABLE_NAME = "conductores";
    public static final String CREATE_TABLE = "CREATE TABLE [conductores]([id] VARCHAR2 UNIQUE, [apellidos] VARCHAR2, [nombres] VARCHAR2, [imagen] VARCHAR2, [imagenb] BLOB, [activo] INTEGER);";

    private String id;
    private String apellidos;
    private String nombres;
    private String imagen;
    private byte[] imagenb;
    private int activo;

    public Conductor() {
        this.apellidos = "";
        this.nombres = "";
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getApellidos()
    {
        return apellidos;
    }

    private void setApellidos(String apellidos)
    {
        this.apellidos = apellidos;
    }

    public String getNombres()
    {
        return nombres;
    }

    private void setNombres(String nombres)
    {
        this.nombres = nombres;
    }

    public String getImagen()
    {
        return imagen;
    }

    public void setImagen(String imagen)
    {
        this.imagen = imagen;
    }

    public byte[] getImagenb()
    {
        return imagenb;
    }

    private void setImagenb(byte[] imagenb)
    {
        this.imagenb = imagenb;
    }

//    public int getActivo() {
//        return activo;
//    }

    private void setActivo(int activo) {
        this.activo = activo;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("apellidos", apellidos);
        contentValues.put("nombres", nombres);
        contentValues.put("imagen", imagen);
        contentValues.put("imagenb", imagenb);
        contentValues.put("activo", 1);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fillByID(String id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setApellidos(cur.getString(cur.getColumnIndex("apellidos")));
                this.setNombres(cur.getString(cur.getColumnIndex("nombres")));
                this.setImagen(cur.getString(cur.getColumnIndex("imagen")));
                this.setImagenb(cur.getBlob(cur.getColumnIndex("imagenb")));
                this.setActivo(cur.getInt(cur.getColumnIndex("activo")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public void desactivateAll(SQLiteDatabase db) throws Exception {
        try {
            db.execSQL("UPDATE " + TABLE_NAME + " SET activo = 0");
        } catch (Exception e) {
            throw new Exception(e);
        }
    }
}
