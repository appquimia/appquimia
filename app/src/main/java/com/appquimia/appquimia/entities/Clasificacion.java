package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by usuario on 09/10/2017.
 */

public class Clasificacion extends EntityAbstract {
    public static final String TABLE_NAME = "clasificaciones";
    public static final String CREATE_TABLE = "CREATE TABLE clasificaciones ([id] BIGINT UNIQUE,[clasificacion] VARCHAR2,[probabilidad] DECIMAL);";

    private long id;
    private String clasificacion;
    private double probabilidad;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

//    public String getClasificacion()
//    {
//        return clasificacion;
//    }

    private void setClasificacion(String clasificacion)
    {
        this.clasificacion = clasificacion;
    }

    public double getProbabilidad()
    {
        return probabilidad;
    }

    private void setProbabilidad(double probabilidad)
    {
        this.probabilidad = probabilidad;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("clasificacion", clasificacion);
        contentValues.put("probabilidad", probabilidad);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fillByID(long id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = " + id, null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.setId(cur.getLong(cur.getColumnIndex("id")));
                this.setClasificacion(cur.getString(cur.getColumnIndex("clasificacion")));
                this.setProbabilidad(cur.getDouble(cur.getColumnIndex("probabilidad")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }
}
