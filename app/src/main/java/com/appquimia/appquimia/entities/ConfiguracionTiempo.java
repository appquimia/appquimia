package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by usuario on 09/10/2017.
 */

public class ConfiguracionTiempo extends EntityAbstract {
    public static final String TABLE_NAME = "configuraciontiempo";
    public static final String CREATE_TABLE = "CREATE TABLE [configuraciontiempo]" +
            "([id] BIGINT UNIQUE, [moviles] INT, [commerce] INT, [street] INT);";

    public static final String ALTER_TABLE_V3_1 = "CREATE TABLE [configuraciontiempo] ([id] BIGINT UNIQUE, [moviles] INT, [commerce] INT, [street] INT);";

    private long id;
    private int moviles;
    private int commerce;
    private int street;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getMoviles() {
        return moviles;
    }

//    public void setMoviles(int moviles) {
//        this.moviles = moviles;
//    }
//
//    public int getCommerce() {
//        return commerce;
//    }
//
//    public void setCommerce(int commerce) {
//        this.commerce = commerce;
//    }
//
//    public int getStreet() {
//        return street;
//    }
//
//    public void setStreet(int street) {
//        this.street = street;
//    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("moviles", moviles);
        contentValues.put("commerce", commerce);
        contentValues.put("street", street);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fill(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getInt(cur.getColumnIndex("id"));
                this.moviles = cur.getInt(cur.getColumnIndex("moviles"));
                this.commerce = cur.getInt(cur.getColumnIndex("commerce"));
                this.street = cur.getInt(cur.getColumnIndex("street"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }
}
