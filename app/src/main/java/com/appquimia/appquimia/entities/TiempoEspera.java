package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 09/10/2017.
 */

public class TiempoEspera extends EntityAbstract
{
    public static final String TABLE_NAME = "tiemposesperas";
    public static final String CREATE_TABLE = "CREATE TABLE [tiemposesperas]([id] BIGINT UNIQUE, [costo] DECIMAL);";

    private long id;
    private double costo;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public double getCosto()
    {
        return costo;
    }

    public void setCosto(double costo)
    {
        this.costo = costo;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("costo", costo);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public static List<TiempoEspera> getList(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        ArrayList<TiempoEspera> mArr = new ArrayList<TiempoEspera>();
        try
        {
            TiempoEspera item;
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                item = new TiempoEspera();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.costo = cur.getDouble(cur.getColumnIndex("costo"));
                mArr.add(item);
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mArr;
    }

    public String fill(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.id = cur.getLong(cur.getColumnIndex("id"));
                this.costo = cur.getDouble(cur.getColumnIndex("costo"));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }
}
