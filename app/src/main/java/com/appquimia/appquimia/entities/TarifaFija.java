package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by usuario on 09/10/2017.
 */

public class TarifaFija extends EntityAbstract
{
    public static final String TABLE_NAME = "tarifasfijas";
    public static final String CREATE_TABLE = "CREATE TABLE [tarifasfijas]([id] BIGINT UNIQUE, [fecha] VARCHAR2, [destino] VARCHAR2, [precio] DECIMAL);";

    private long id;
    private String fecha;
    private String destino;
    private double precio;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getFecha()
    {
        return fecha;
    }

    public void setFecha(String fecha)
    {
        this.fecha = fecha;
    }

    public String getDestino()
    {
        return destino;
    }

    public void setDestino(String destino)
    {
        this.destino = destino;
    }

    public double getPrecio()
    {
        return precio;
    }

    public void setPrecio(double precio)
    {
        this.precio = precio;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("fecha", fecha);
        contentValues.put("destino", destino);
        contentValues.put("precio", precio);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public static List<TarifaFija> getList(SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        ArrayList<TarifaFija> mArr = new ArrayList<TarifaFija>();
        try
        {
            TarifaFija item;
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                item = new TarifaFija();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.fecha = cur.getString(cur.getColumnIndex("fecha"));
                item.destino = cur.getString(cur.getColumnIndex("destino"));
                item.precio = cur.getDouble(cur.getColumnIndex("precio"));
                mArr.add(item);
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mArr;
    }
}
