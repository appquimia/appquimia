package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.HashMap;

/**
 * Created by usuario on 09/10/2017.
 */

public class Cuenta extends EntityAbstract {
    public static final String TABLE_NAME = "cuentas";
    public static final String CREATE_TABLE = "CREATE TABLE [cuentas]([id] VARCHAR2 PRIMARY KEY UNIQUE, [nombre] VARCHAR2, [email] VARCHAR2, [imagen] VARCHAR2, [provider] VARCHAR2);";

    private String id;
    private String nombre;
    private String email;
    private String imagen;
    private String provider;

    public String getId() {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getNombre()
    {
        return nombre;
    }

    public void setNombre(String nombre)
    {
        this.nombre = nombre;
    }

    public String getEmail()
    {
        return email;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public String getImagen()
    {
        return imagen;
    }

    public void setImagen(String imagen)
    {
        this.imagen = imagen;
    }

    public String getProvider()
    {
        return provider;
    }

    public void setProvider(String provider)
    {
        this.provider = provider;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("nombre", nombre);
        contentValues.put("email", email);
        contentValues.put("imagen", imagen);
        contentValues.put("provider", provider);

        String mess = "";

        try {
            int id = (int) db.insertWithOnConflict(TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_IGNORE);
            if (id == -1) {
                db.update(TABLE_NAME, contentValues, "id=?", new String[] {this.id});
            }
        } catch (Exception e) {
            mess = e.getMessage();
        }

        return mess;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fill(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM cuentas LIMIT 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setNombre(cur.getString(cur.getColumnIndex("nombre")));
                this.setEmail(cur.getString(cur.getColumnIndex("email")));
                this.setImagen(cur.getString(cur.getColumnIndex("imagen")));
                this.setProvider(cur.getString(cur.getColumnIndex("provider")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public HashMap<String,String> getHashMap() {
        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id);
        map.put("nombre", this.nombre);
        map.put("email", this.email);
        map.put("imagen", this.imagen);
        map.put("provider", this.provider);
        map.put("celular", "");

        return map;
    }
}
