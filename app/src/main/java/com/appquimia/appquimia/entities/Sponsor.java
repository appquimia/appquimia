package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by usuario on 10/10/2017.
 */

public class Sponsor extends EntityAbstract {
    public static final String TABLE_NAME = "sponsors";
    public static final String CREATE_TABLE = "CREATE TABLE sponsors " +
            "([id] BIGINT UNIQUE, [razonsocial] VARCHAR2, [descripcion] VARCHAR2, " +
            "[direcciones] VARCHAR2, [codigocanjeo] VARCHAR2, [telefono] VARCHAR2, [cuit] BIGINT, " +
            "[imagen] VARCHAR2 DEFAULT '', [fecultmodif] VARCHAR2 DEFAULT '1900-01-01 00:00:00', " +
            "[imagenb] BLOB);";

    public static final String ALTER_TABLE_V3_1 = "ALTER TABLE sponsors ADD COLUMN [imagen] VARCHAR2 DEFAULT '';";
    public static final String ALTER_TABLE_V3_2 = "ALTER TABLE sponsors ADD COLUMN [fecultmodif] VARCHAR2 DEFAULT '1900-01-01 00:00:00';";
    public static final String ALTER_TABLE_V3_3 = "ALTER TABLE sponsors ADD COLUMN [imagenb] BLOB;";
    public static final String ALTER_TABLE_V3_4 = "ALTER TABLE sponsors ADD COLUMN [descripcion] VARCHAR2;";

    private long id;
    private String razonsocial;
    private String descripcion;
    private String direcciones;
    private String codigocanjeo;
    private String telefono;
    private long cuit;
    private String imagen;
    private String fecultmodif;
    private byte[] imagenb;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getRazonsocial()
    {
        return razonsocial;
    }

    public void setRazonsocial(String razonsocial)
    {
        this.razonsocial = razonsocial;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDirecciones()
    {
        return direcciones;
    }

    public void setDirecciones(String direcciones)
    {
        this.direcciones = direcciones;
    }

    public String getCodigocanjeo()
    {
        return codigocanjeo;
    }

    public void setCodigocanjeo(String codigocanjeo)
    {
        this.codigocanjeo = codigocanjeo;
    }

    public String getTelefono()
    {
        return telefono;
    }

    public void setTelefono(String telefono)
    {
        this.telefono = telefono;
    }

    public long getCuit()
    {
        return cuit;
    }

    public void setCuit(long cuit)
    {
        this.cuit = cuit;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getFecultmodif() {
        return fecultmodif;
    }

    public void setFecultmodif(String fecultmodif) {
        this.fecultmodif = fecultmodif;
    }

    public byte[] getImagenb() {
        return imagenb;
    }

    public void setImagenb(byte[] imagenb) {
        this.imagenb = imagenb;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.id);
        contentValues.put("razonsocial", this.razonsocial);
        contentValues.put("descripcion", this.descripcion);
        contentValues.put("direcciones", this.direcciones);
        contentValues.put("codigocanjeo", this.codigocanjeo);
        contentValues.put("telefono", this.telefono);
        contentValues.put("cuit", this.cuit);
        contentValues.put("imagen", this.imagen);
        contentValues.put("fecultmodif", this.fecultmodif);
        contentValues.put("imagenb", this.imagenb);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public String fillByID(long id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getLong(cur.getColumnIndex("id"));
                this.razonsocial = cur.getString(cur.getColumnIndex("razonsocial"));
                this.descripcion = cur.getString(cur.getColumnIndex("descripcion"));
                this.direcciones = cur.getString(cur.getColumnIndex("direcciones"));
                this.codigocanjeo = cur.getString(cur.getColumnIndex("codigocanjeo"));
                this.telefono = cur.getString(cur.getColumnIndex("telefono"));
                this.cuit = cur.getLong(cur.getColumnIndex("cuit"));
                this.imagen = cur.getString(cur.getColumnIndex("imagen"));
                this.fecultmodif = cur.getString(cur.getColumnIndex("fecultmodif"));
                this.imagenb = cur.getBlob(cur.getColumnIndex("imagenb"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public static ArrayList<Sponsor> getList(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        ArrayList<Sponsor> arr = new ArrayList<>();

        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME, null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                Sponsor item = new Sponsor();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.razonsocial = cur.getString(cur.getColumnIndex("razonsocial"));
                item.descripcion = cur.getString(cur.getColumnIndex("descripcion"));
                item.direcciones = cur.getString(cur.getColumnIndex("direcciones"));
                item.codigocanjeo = cur.getString(cur.getColumnIndex("codigocanjeo"));
                item.telefono = cur.getString(cur.getColumnIndex("telefono"));
                item.cuit = cur.getLong(cur.getColumnIndex("cuit"));
                item.imagen = cur.getString(cur.getColumnIndex("imagen"));
                item.fecultmodif = cur.getString(cur.getColumnIndex("fecultmodif"));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public static String getLastFechaModif(SQLiteDatabase db) {
        String fecmodif = "0000-00-00";
        Cursor cur = null;

        try {
            cur = db.rawQuery("SELECT fecultmodif FROM " + TABLE_NAME + " ORDER BY fecultmodif DESC LIMIT 1", null);
            cur.moveToFirst();
            if(!cur.isAfterLast()) {
                fecmodif = cur.getString(cur.getColumnIndex("fecultmodif"));
            }
        } catch (Exception e) {
            fecmodif = "0000-00-00";
        } finally {
            if (cur != null)
                cur.close();
        }

        return fecmodif;
    }

    public static void clearTable(SQLiteDatabase db) {
        try {
            db.execSQL("delete from "+ TABLE_NAME);
        } catch (Exception e) {}
    }
}
