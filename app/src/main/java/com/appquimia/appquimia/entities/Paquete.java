package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by usuario on 10/10/2017.
 */

public class Paquete extends EntityAbstract
{
    public static final String TABLE_NAME = "paquetes";
    public static final String CREATE_TABLE = "CREATE TABLE [paquetes]([id] BIGINT UNIQUE, [paquete] VARCHAR2);";

    private long id;
    private String paquete;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getPaquete()
    {
        return paquete;
    }

    public void setPaquete(String paquete)
    {
        this.paquete = paquete;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("paquete", paquete);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public static ArrayList<Paquete> getList(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        ArrayList<Paquete> mArr = new ArrayList<Paquete>();
        try {
            Paquete item = new Paquete();
            item.id = 0;
            item.paquete = "Todos los rubros";
            mArr.add(item);

            cur = db.rawQuery("SELECT pa.* " +
                    "FROM paquetes pa " +
                    "INNER JOIN premios pr ON pa.id = pr.idpaquete " +
                    "WHERE DATE(pr.fecinicio) < DATE('now') AND DATE(pr.fecfin) > DATE('now') AND DATE('now') < DATE(pr.fecven)" +
                    "GROUP BY pa.id", null);
            cur.moveToFirst();

            if(cur.getCount() == 1)
                return mArr;

            while(!cur.isAfterLast()) {
                item = new Paquete();
                item.id = cur.getLong(cur.getColumnIndex("id"));
                item.paquete = cur.getString(cur.getColumnIndex("paquete"));
                mArr.add(item);
                cur.moveToNext();
            }
        }
        catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mArr;
    }
}
