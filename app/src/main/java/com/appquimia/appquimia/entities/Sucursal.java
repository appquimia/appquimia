package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.enums.PremioEstados;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by usuario on 27/09/2017.
 */

public class Sucursal extends EntityAbstract {
    public static final String TABLE_NAME = "sucursales";
    public static final String CREATE_TABLE = "CREATE TABLE [sucursales](" +
            "[id] VARCHAR2 UNIQUE, [idsponsor] BIGINT, [sucursal] VARCHAR2, [direccion] VARCHAR2, " +
            "[descripcion] VARCHAR2);";

    public static final String ALTER_TABLE_V3_1 = "CREATE TABLE [sucursales]([id] VARCHAR2 UNIQUE, [idsponsor] BIGINT, [sucursal] VARCHAR2, [direccion] VARCHAR2, [descripcion] VARCHAR2);";

    private String id;
    private long idsponsor;
    private String sucursal;
    private String direccion;
    private String descripcion;

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public long getIdsponsor() {
        return idsponsor;
    }

    public void setIdsponsor(long idsponsor) {
        this.idsponsor = idsponsor;
    }

    public String getSucursal() {
        return sucursal;
    }

    public void setSucursal(String sucursal) {
        this.sucursal = sucursal;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", this.id);
        contentValues.put("idsponsor", this.idsponsor);
        contentValues.put("sucursal", this.sucursal);
        contentValues.put("direccion", this.direccion);
        contentValues.put("descripcion", this.descripcion);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public String fillByID(String id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.sucursal = cur.getString(cur.getColumnIndex("sucursal"));
                this.direccion = cur.getString(cur.getColumnIndex("direccion"));
                this.descripcion = cur.getString(cur.getColumnIndex("descripcion"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public static ArrayList<Sucursal> getListByIdSponsor(long idsponsor, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        ArrayList<Sucursal> arr = new ArrayList<Sucursal>();
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idsponsor = " + idsponsor, null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                Sucursal item = new Sucursal();
                item.setId(cur.getString(cur.getColumnIndex("id")));
                item.setIdsponsor(cur.getLong(cur.getColumnIndex("idsponsor")));
                item.setSucursal(cur.getString(cur.getColumnIndex("sucursal")));
                item.setDescripcion(cur.getString(cur.getColumnIndex("direccion")));
                item.setDireccion(cur.getString(cur.getColumnIndex("descripcion")));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public static void clearTable(SQLiteDatabase db) {
        try {
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e) {}
    }
}
