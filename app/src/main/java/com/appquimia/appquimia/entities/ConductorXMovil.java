package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by usuario on 24/10/2017.
 */

public class ConductorXMovil extends EntityAbstract {
    public static final String TABLE_NAME = "conductoresxmoviles";
    public static final String CREATE_TABLE = "CREATE TABLE [conductoresxmoviles]([id] BIGINT UNIQUE,  [idconductor] VARCHAR2,  [idmovil] VARCHAR2, [activo] INT );";

    private long id;
    private String idconductor;
    private String idmovil;

    private int activo;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getIdconductor()
    {
        return idconductor;
    }

    private void setIdconductor(String idconductor)
    {
        this.idconductor = idconductor;
    }

    public String getIdmovil()
    {
        return idmovil;
    }

    private void setIdmovil(String idmovil)
    {
        this.idmovil = idmovil;
    }

//    public int getActivo() {
//        return activo;
//    }

    private void setActivo(int activo) {
        this.activo = activo;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("idconductor", idconductor);
        contentValues.put("idmovil", idmovil);
        contentValues.put("activo", 1);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public static ArrayList getConductoresByIDMovil(String id, SQLiteDatabase db) throws Exception {
        Cursor cur = null;
        ArrayList<ConductorXMovil> mArr = new ArrayList<>();
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idmovil = '" + id +"' AND activo = 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                ConductorXMovil item = new ConductorXMovil();
                item.setId(cur.getLong(cur.getColumnIndex("id")));
                item.setIdconductor(cur.getString(cur.getColumnIndex("idconductor")));
                item.setIdmovil(cur.getString(cur.getColumnIndex("idmovil")));
                item.setActivo(cur.getInt(cur.getColumnIndex("activo")));
                mArr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            throw  new Exception(e);
        } finally {
            if (cur != null)
                cur.close();
        }

        return mArr;
    }

    public String desactivateAll(SQLiteDatabase db) {
        String mess = "";
        try {
            db.execSQL("UPDATE " + TABLE_NAME + " SET activo = 0");
        } catch (Exception e) {
            mess = e.getMessage();
        }
        return mess;
    }
}
