package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

/**
 * Created by usuario on 27/09/2017.
 */

public class Producto extends EntityAbstract
{
    public static final String TABLE_NAME = "productos";
    public static final String CREATE_TABLE = "CREATE TABLE productos ([id] VARCHAR2 UNIQUE, " +
            "[idsponsor] BIGINT, [producto] VARCHAR2, [descripcion] VARCHAR2, [imagen] VARCHAR2);";

    public static final String ALTER_TABLE_V3_1 = "CREATE TABLE productos ([id] VARCHAR2 UNIQUE, [idsponsor] BIGINT, [producto] VARCHAR2, [descripcion] VARCHAR2, [imagen] VARCHAR2)";

    private String id;
    private long idsponsor;
    private String producto;
    private String descripcion;
    private String imagen;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getIdsponsor() {
        return idsponsor;
    }

    public void setIdsponsor(long idsponsor) {
        this.idsponsor = idsponsor;
    }

    public String getProducto() {
        return producto;
    }

    public void setProducto(String producto) {
        this.producto = producto;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("idsponsor", idsponsor);
        contentValues.put("producto", producto);
        contentValues.put("descripcion", descripcion);
        contentValues.put("imagen", imagen);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public static ArrayList<Producto> getProductosByIdSponsor(long idsponsor, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        ArrayList<Producto> arr = new ArrayList<Producto>();
        try {
            String query = "SELECT id, idsponsor, producto, descripcion, imagen FROM " + TABLE_NAME
                    + " WHERE idsponsor = " + idsponsor;

            cur = db.rawQuery(query, null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                Producto item = new Producto();
                item.id = cur.getString(cur.getColumnIndex("id"));
                item.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                item.producto = cur.getString(cur.getColumnIndex("producto"));
                item.descripcion = cur.getString(cur.getColumnIndex("descripcion"));
                item.imagen = cur.getString(cur.getColumnIndex("imagen"));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public String fillByID(String id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.producto = cur.getString(cur.getColumnIndex("producto"));
                this.descripcion = cur.getString(cur.getColumnIndex("fecinicio"));
                this.imagen = cur.getString(cur.getColumnIndex("imagen"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public static ArrayList<Producto> getByIDSponsor(String idsponsor, SQLiteDatabase db) {
        ArrayList<Producto> marr = new ArrayList<>();
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idsponsor = " + idsponsor, null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                Producto prd = new Producto();
                prd.id = cur.getString(cur.getColumnIndex("id"));
                prd.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                prd.producto = cur.getString(cur.getColumnIndex("producto"));
                prd.descripcion = cur.getString(cur.getColumnIndex("fecinicio"));
                prd.imagen = cur.getString(cur.getColumnIndex("imagen"));
                marr.add(prd);
                cur.moveToNext();
            }
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return marr;
    }

    public static void clearTable(SQLiteDatabase db) {
        try {
            db.execSQL("delete from "+ TABLE_NAME);
        } catch (Exception e) {}
    }
}
