package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by usuario on 09/10/2017.
 */

public class Movil extends EntityAbstract
{
    public static final String TABLE_NAME = "moviles";
    public static final String CREATE_TABLE = "CREATE TABLE [moviles]" +
            "([id] VARCHAR2 UNIQUE, [descripcion] VARCHAR2, [dominio] VARCHAR2, [idresponsable] VARCHAR2, " +
            "[imagen] VARCHAR2, [nromovil] INT, [imagenb] BLOB, [activo] INT, [bloqueaingreso] INT DEFAULT 0," +
            "[bloqueajuego] INT DEFAULT 0);";

    public static final String ALTER_TABLE_V3_1 = "ALTER TABLE moviles ADD COLUMN [bloqueaingreso] INT DEFAULT 0;";
    public static final String ALTER_TABLE_V3_2 = "ALTER TABLE moviles ADD COLUMN [bloqueajuego] INT DEFAULT 0;";

    private String id;
    private String descripcion;
    private String dominio;
    private String idresponsable;
    private String imagen;
    private String nromovil;
    private byte[] imagenb;
    private int activo;
    private int bloqueaingreso;
    private int bloqueajuego;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    public String getIdresponsable() {
        return idresponsable;
    }

    public void setIdresponsable(String idresponsable) {
        this.idresponsable = idresponsable;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getNromovil() {
        return nromovil;
    }

    public void setNromovil(String nromovil) {
        this.nromovil = nromovil;
    }

    public byte[] getImagenb() {
        return imagenb;
    }

    public void setImagenb(byte[] imagenb) {
        this.imagenb = imagenb;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public int getBloqueaingreso() {
        return bloqueaingreso;
    }

    public void setBloqueaingreso(int bloqueaingreso) {
        this.bloqueaingreso = bloqueaingreso;
    }

    public int getBloqueajuego() {
        return bloqueajuego;
    }

    public void setBloqueajuego(int bloqueajuego) {
        this.bloqueajuego = bloqueajuego;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("descripcion", descripcion);
        contentValues.put("dominio", dominio);
        contentValues.put("idresponsable", idresponsable);
        contentValues.put("imagen", imagen);
        contentValues.put("nromovil", nromovil);
        contentValues.put("imagenb", imagenb);
        contentValues.put("activo", 1);
        contentValues.put("bloqueaingreso", this.bloqueaingreso);
        contentValues.put("bloqueajuego", this.bloqueajuego);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public String fillByNro(String nro, SQLiteDatabase db) {
        String mess = "No existe el móvil.";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE nromovil = '" + nro + "' AND activo = 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setDescripcion(cur.getString(cur.getColumnIndex("descripcion")));
                this.setDominio(cur.getString(cur.getColumnIndex("dominio")));
                this.setIdresponsable(cur.getString(cur.getColumnIndex("idresponsable")));
                this.setImagen(cur.getString(cur.getColumnIndex("imagen")));
                this.setNromovil(cur.getString(cur.getColumnIndex("nromovil")));
                this.setImagenb(cur.getBlob(cur.getColumnIndex("imagenb")));
                this.setActivo(cur.getInt(cur.getColumnIndex("activo")));
                this.setBloqueaingreso(cur.getInt(cur.getColumnIndex("bloqueaingreso")));
                this.setBloqueajuego(cur.getInt(cur.getColumnIndex("bloqueajuego")));
                cur.moveToNext();
                mess = "";
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public String fillByID(String id, SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setDescripcion(cur.getString(cur.getColumnIndex("descripcion")));
                this.setDominio(cur.getString(cur.getColumnIndex("dominio")));
                this.setIdresponsable(cur.getString(cur.getColumnIndex("idresponsable")));
                this.setImagen(cur.getString(cur.getColumnIndex("imagen")));
                this.setNromovil(cur.getString(cur.getColumnIndex("nromovil")));
                this.setImagenb(cur.getBlob(cur.getColumnIndex("imagenb")));
                this.setActivo(cur.getInt(cur.getColumnIndex("activo")));
                this.setBloqueaingreso(cur.getInt(cur.getColumnIndex("bloqueaingreso")));
                this.setBloqueajuego(cur.getInt(cur.getColumnIndex("bloqueajuego")));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public String desactivateAll(SQLiteDatabase db) {
        String mess = "";
        try {
            db.execSQL("UPDATE " + TABLE_NAME + " SET activo = 0");
        } catch (Exception e) {
            mess = e.getMessage();
        }
        return mess;
    }
}
