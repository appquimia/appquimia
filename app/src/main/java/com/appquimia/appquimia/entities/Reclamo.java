package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by ajade on 19/12/2017.
 */

public class Reclamo extends EntityAbstract
{
    public static final String TABLE_NAME = "reclamos";
    public static final String CREATE_TABLE = "CREATE TABLE [reclamos](" +
            "[id] VARCHAR2 UNIQUE, " +
            "[idcuenta] VARCHAR2, " +
            "[idpremio] BIGINT, " +
            "[idestado] INT, " +
            "[fecha] VARCHAR2, " +
            "[hora] VARCHAR2, " +
            "[sync] INT);";

    private String id;
    private String idcuenta;
    private long idpremio;
    private int idestado;
    private String fecha;
    private String hora;
    private int sync;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(String idcuenta) {
        this.idcuenta = idcuenta;
    }

    public long getIdpremio() {
        return idpremio;
    }

    public void setIdpremio(long idpremio) {
        this.idpremio = idpremio;
    }

    public int getIdestado() {
        return idestado;
    }

    public void setIdestado(int idestado) {
        this.idestado = idestado;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public int getSync() {
        return sync;
    }

    public void setSync(int sync) {
        this.sync = sync;
    }

    public String fillByID(String id, SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE id = '" + id + "'", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.setId(cur.getString(cur.getColumnIndex("id")));
                this.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                this.setIdpremio(cur.getInt(cur.getColumnIndex("idpremio")));
                this.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                this.setFecha(cur.getString(cur.getColumnIndex("fecha")));
                this.setHora(cur.getString(cur.getColumnIndex("hora")));
                this.setSync(cur.getInt(cur.getColumnIndex("sync")));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("idcuenta", idcuenta);
        contentValues.put("idpremio", idpremio);
        contentValues.put("idestado", idestado);
        contentValues.put("fecha", fecha);
        contentValues.put("hora", hora);
        contentValues.put("sync", sync);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public ArrayList<Reclamo> getNotSync(SQLiteDatabase db)
    {
        ArrayList<Reclamo> arr = new ArrayList<>();
        String mess = "";
        Cursor cur = null;
        try
        {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE sync = 0", null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                Reclamo r = new Reclamo();
                r.setId(cur.getString(cur.getColumnIndex("id")));
                r.setIdcuenta(cur.getString(cur.getColumnIndex("idcuenta")));
                r.setIdpremio(cur.getLong(cur.getColumnIndex("idpremio")));
                r.setIdestado(cur.getInt(cur.getColumnIndex("idestado")));
                r.setFecha(cur.getString(cur.getColumnIndex("fecha")));
                r.setHora(cur.getString(cur.getColumnIndex("hora")));

                arr.add(r);
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return arr;
    }

    public HashMap<String,String> getHashMap() {

        HashMap<String, String> map = new HashMap<>();
        map.put("id", this.id);
        map.put("idcuenta", this.idcuenta);
        map.put("idpremio", this.idpremio+ "");
        map.put("idestado", this.idestado + "");
        map.put("fecha", this.fecha);
        map.put("hora", this.hora);

        return map;
    }
}
