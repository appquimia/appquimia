package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.appquimia.appquimia.classes.Constantes;

import java.util.ArrayList;

/**
 * Created by usuario on 09/10/2017.
 */

public class Bitacora extends EntityAbstract {
    public static final String TABLE_NAME = "bitacoras";
    public static final String CREATE_TABLE = "CREATE TABLE [bitacoras]" +
            "([id] VARCHAR2 UNIQUE, [idcuenta] VARCHAR2, [idmovil] VARCHAR2, [fecha] VARCHAR2, " +
            "[costo] DOUBLE, [fecjuego] VARCHAR2, [idsponsor] BIGINT DEFAULT '', " +
            "[idsucursal] VARCHAR2 DEFAULT '', [modulo] INT DEFAULT 0);";

    public static final String ALTER_TABLE_V3_1 = "ALTER TABLE bitacoras ADD COLUMN [idsponsor] BIGINT DEFAULT '';";
    public static final String ALTER_TABLE_V3_2 = "ALTER TABLE bitacoras ADD COLUMN [idsucursal] VARCHAR2 DEFAULT '';";
    public static final String ALTER_TABLE_V3_3 = "ALTER TABLE bitacoras ADD COLUMN [modulo] INT DEFAULT 0;";

    private String id;
    private String idcuenta;
    private String idmovil;
    private long idsponsor;
    private String idsucursal;
    private String fecha;
    private double costo;
    private String fecjuego;
    private int modulo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdcuenta() {
        return idcuenta;
    }

    public void setIdcuenta(String idcuenta) {
        this.idcuenta = idcuenta;
    }

    public String getIdmovil() {
        return idmovil;
    }

    public void setIdmovil(String idmovil) {
        this.idmovil = idmovil;
    }

    public long getIdsponsor() {
        return idsponsor;
    }

    public void setIdsponsor(long idsponsor) {
        this.idsponsor = idsponsor;
    }

    public String getIdsucursal() {
        return idsucursal;
    }

    public void setIdsucursal(String idsucursal) {
        this.idsucursal = idsucursal;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public double getCosto() {
        return costo;
    }

    public void setCosto(double costo) {
        this.costo = costo;
    }

    public String getFecjuego() {
        return fecjuego;
    }

    public void setFecjuego(String fecjuego) {
        this.fecjuego = fecjuego;
    }

    public int getModulo() {
        return modulo;
    }

    public void setModulo(int modulo) {
        this.modulo = modulo;
    }

    public String save(SQLiteDatabase db) {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("idcuenta", idcuenta);
        contentValues.put("idmovil", idmovil);
        contentValues.put("idsponsor", idsponsor);
        contentValues.put("idsucursal", idsucursal);
        contentValues.put("fecha", fecha);
        contentValues.put("costo", costo);
        contentValues.put("fecjuego", fecjuego);
        contentValues.put("modulo", modulo);

        return super.save(db, TABLE_NAME, contentValues, this.id);
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public String fillLast(SQLiteDatabase db) {
        String mess = "";
        Cursor cur = null;
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " ORDER BY fecha DESC LIMIT 1", null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idcuenta = cur.getString(cur.getColumnIndex("idcuenta"));
                this.idmovil = cur.getString(cur.getColumnIndex("idmovil"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.idsucursal = cur.getString(cur.getColumnIndex("idsucursal"));
                this.fecha = cur.getString(cur.getColumnIndex("fecha"));
                this.costo = cur.getDouble(cur.getColumnIndex("costo"));
                this.fecjuego = cur.getString(cur.getColumnIndex("fecjuego"));
                this.modulo = cur.getInt(cur.getColumnIndex("modulo"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            mess = e.getMessage();
        } finally {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public void fillLastGameMovil(SQLiteDatabase db) throws Exception {
        Cursor cur = null;
        try {
            String query = "SELECT * FROM " + TABLE_NAME
                    + " WHERE fecjuego <> 0 AND modulo = " + Constantes.MODULE_MOVILES
                    + " ORDER BY fecjuego DESC LIMIT 1";
            cur = db.rawQuery(query, null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idcuenta = cur.getString(cur.getColumnIndex("idcuenta"));
                this.idmovil = cur.getString(cur.getColumnIndex("idmovil"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.idsucursal = cur.getString(cur.getColumnIndex("idsucursal"));
                this.fecha = cur.getString(cur.getColumnIndex("fecha"));
                this.costo = cur.getDouble(cur.getColumnIndex("costo"));
                this.fecjuego = cur.getString(cur.getColumnIndex("fecjuego"));
                this.modulo = cur.getInt(cur.getColumnIndex("modulo"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            throw  new Exception(e);
        } finally {
            if (cur != null)
                cur.close();
        }
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public void fillLastGameCommerce(SQLiteDatabase db) throws Exception {
        Cursor cur = null;
        try {
            String query = "SELECT * FROM " + TABLE_NAME
                    + " WHERE fecjuego <> 0 AND modulo = " + Constantes.MODULE_COMMERCE
                    + " ORDER BY fecjuego DESC LIMIT 1";
            cur = db.rawQuery(query, null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idcuenta = cur.getString(cur.getColumnIndex("idcuenta"));
                this.idmovil = cur.getString(cur.getColumnIndex("idmovil"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.idsucursal = cur.getString(cur.getColumnIndex("idsucursal"));
                this.fecha = cur.getString(cur.getColumnIndex("fecha"));
                this.costo = cur.getDouble(cur.getColumnIndex("costo"));
                this.fecjuego = cur.getString(cur.getColumnIndex("fecjuego"));
                this.modulo = cur.getInt(cur.getColumnIndex("modulo"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            throw  new Exception(e);
        } finally {
            if (cur != null)
                cur.close();
        }
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public void fillLastGameStreet(SQLiteDatabase db) throws Exception {
        Cursor cur = null;
        try {
            String query = "SELECT * FROM " + TABLE_NAME
                    + " WHERE fecjuego <> 0 AND modulo = " + Constantes.MODULE_STREET
                    + " ORDER BY fecjuego DESC LIMIT 1";
            cur = db.rawQuery(query, null);
            cur.moveToFirst();
            while(!cur.isAfterLast()) {
                this.id = cur.getString(cur.getColumnIndex("id"));
                this.idcuenta = cur.getString(cur.getColumnIndex("idcuenta"));
                this.idmovil = cur.getString(cur.getColumnIndex("idmovil"));
                this.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                this.idsucursal = cur.getString(cur.getColumnIndex("idsucursal"));
                this.fecha = cur.getString(cur.getColumnIndex("fecha"));
                this.costo = cur.getDouble(cur.getColumnIndex("costo"));
                this.fecjuego = cur.getString(cur.getColumnIndex("fecjuego"));
                this.modulo = cur.getInt(cur.getColumnIndex("modulo"));
                cur.moveToNext();
            }
        } catch (Exception e) {
            throw  new Exception(e);
        } finally {
            if (cur != null)
                cur.close();
        }
    }

    @SuppressWarnings("TryFinallyCanBeTryWithResources")
    public static ArrayList<Bitacora> getListByIdCuenta(String idcuenta, SQLiteDatabase db) {
        Cursor cur = null;
        ArrayList<Bitacora> arr = new ArrayList<>();
        try {
            cur = db.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE idcuenta = '" + idcuenta + "'", null);
            cur.moveToFirst();
            while (!cur.isAfterLast()) {
                Bitacora item = new Bitacora();
                item.id = cur.getString(cur.getColumnIndex("id"));
                item.idcuenta = cur.getString(cur.getColumnIndex("idcuenta"));
                item.idmovil = cur.getString(cur.getColumnIndex("idmovil"));
                item.idsponsor = cur.getLong(cur.getColumnIndex("idsponsor"));
                item.idsucursal = cur.getString(cur.getColumnIndex("idsucursal"));
                item.fecha = cur.getString(cur.getColumnIndex("fecha"));
                item.costo = cur.getDouble(cur.getColumnIndex("costo"));
                item.fecjuego = cur.getString(cur.getColumnIndex("fecjuego"));
                item.modulo = cur.getInt(cur.getColumnIndex("modulo"));

                arr.add(item);
                cur.moveToNext();
            }
        } catch (Exception e) {
            arr = new ArrayList<>();
        } finally {
            if (cur != null)
                cur.close();
        }

        return arr;
    }
}
