package com.appquimia.appquimia.entities;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * Created by usuario on 09/10/2017.
 */

public class Publicidad extends EntityAbstract
{
    public static final String TABLE_NAME = "publicidades";
    public static final String CREATE_TABLE = "CREATE TABLE [publicidades](" +
            "[id] BIGINT UNIQUE, [idclasificacion] INT, [publicidad] VARCHAR2, [url] VARCHAR2, [fecinicio] VARCHAR2, " +
            "[fecfin] VARCHAR2, [fecmodificacion] VARCHAR2, [imagen] VARCHAR2, [imagenb] BLOB);";

    private long id;
    private int idclasificacion;
    private String publicidad;
    private String url;
    private String fecinicio;
    private String fecfin;
    private String fecmodificacion;
    private String imagen;
    private byte[] imagenb;

    public Publicidad() {
        this.id = id-1;
    }

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getPublicidad()
    {
        return publicidad;
    }

    public void setPublicidad(String publicidad)
    {
        this.publicidad = publicidad;
    }

    public String getUrl()
    {
        return url;
    }

    public void setUrl(String url)
    {
        this.url = url;
    }

    public String getFecinicio()
    {
        return fecinicio;
    }

    public void setFecinicio(String fecinicio)
    {
        this.fecinicio = fecinicio;
    }

    public String getFecfin()
    {
        return fecfin;
    }

    public void setFecfin(String fecfin){
        this.fecfin = fecfin;
    }

    public String getFecmodificacion() {
        return fecmodificacion;
    }

    public void setFecmodificacion(String fecmodificacion) {
        this.fecmodificacion = fecmodificacion;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public byte[] getImagenb() {
        return imagenb;
    }

    public void setImagenb(byte[] imagenb) {
        this.imagenb = imagenb;
    }

    public int getIdclasificacion() {
        return idclasificacion;
    }

    public void setIdclasificacion(int idclasificacion) {
        this.idclasificacion = idclasificacion;
    }

    public String save(SQLiteDatabase db)
    {
        final ContentValues contentValues = new ContentValues();
        contentValues.put("id", id);
        contentValues.put("publicidad", publicidad);
        contentValues.put("url", url);
        contentValues.put("fecinicio", fecinicio);
        contentValues.put("fecfin", fecfin);
        contentValues.put("fecmodificacion", fecmodificacion);
        contentValues.put("imagen", imagen);
        contentValues.put("imagenb", imagenb);
        contentValues.put("idclasificacion", idclasificacion);

        return super.save(db, TABLE_NAME, contentValues, String.valueOf(this.id));
    }

    public String fill(long pid, float peso, SQLiteDatabase db)
    {
        String mess = "";
        Cursor cur = null;
        try
        {
            String query = "SELECT p.* " +
                    "FROM publicidades p " +
                    "LEFT JOIN clasificaciones c ON c.id = p.idclasificacion " +
                    "WHERE c.probabilidad >= " + peso + " AND p.id <> " + pid + " " +
                    "ORDER BY RANDOM() " +
                    "LIMIT 1";
            cur = db.rawQuery(query, null);
            cur.moveToFirst();
            while(!cur.isAfterLast())
            {
                this.setId(cur.getLong(cur.getColumnIndex("id")));
                this.setPublicidad(cur.getString(cur.getColumnIndex("publicidad")));
                this.setUrl(cur.getString(cur.getColumnIndex("url")));
                this.setFecinicio(cur.getString(cur.getColumnIndex("fecinicio")));
                this.setFecmodificacion(cur.getString(cur.getColumnIndex("fecmodificacion")));
                this.setFecfin(cur.getString(cur.getColumnIndex("fecfin")));
                this.setImagen(cur.getString(cur.getColumnIndex("imagen")));
                this.setImagenb(cur.getBlob(cur.getColumnIndex("imagenb")));
                this.setIdclasificacion(cur.getInt(cur.getColumnIndex("idclasificacion")));
                cur.moveToNext();
            }
        }
        catch (Exception e)
        {
            mess = e.getMessage();
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return mess;
    }

    public static String getLastFechaModif(SQLiteDatabase db) {
        String fecmodif = "0000-00-00";
        Cursor cur = null;

        try
        {
            cur = db.rawQuery("SELECT fecmodificacion FROM " + TABLE_NAME + " ORDER BY fecmodificacion DESC LIMIT 1", null);
            cur.moveToFirst();
            if(!cur.isAfterLast())
            {
                fecmodif = cur.getString(cur.getColumnIndex("fecmodificacion"));
            }
        }
        catch (Exception e)
        {
            fecmodif = "0000-00-00";
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return fecmodif;
    }

    public static void clearTable(SQLiteDatabase db) {
        try
        {
            db.execSQL("delete from "+ TABLE_NAME);
        }
        catch (Exception e) {}
    }

    public static boolean checkData(SQLiteDatabase db) {
        boolean check = false;
        Cursor cur = null;

        try {
            cur = db.rawQuery("SELECT COUNT(*) AS errores FROM publicidades WHERE LENGTH(imagenb) IS NULL", null);
            cur.moveToFirst();
            if(!cur.isAfterLast()) {
                check = (cur.getInt(cur.getColumnIndex("errores")) == 0);
            }
        } catch (Exception e) {
            check = false;
        }
        finally
        {
            if (cur != null)
                cur.close();
        }

        return check;
    }

    @Override
    public boolean equals(Object obj) {
        return this.id == ((Publicidad) obj).id;
    }
}
