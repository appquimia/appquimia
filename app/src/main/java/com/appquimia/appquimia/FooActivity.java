package com.appquimia.appquimia;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Patterns;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.adeel.library.easyFTP;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.MainApp;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.entities.Version;
import com.appquimia.appquimia.services.ClasificacionService;
import com.appquimia.appquimia.services.ConductorService;
import com.appquimia.appquimia.services.ConductorXMovilService;
import com.appquimia.appquimia.services.ConfiguracionTiempoService;
import com.appquimia.appquimia.services.MovilService;
import com.appquimia.appquimia.services.PaqueteService;
import com.appquimia.appquimia.services.PremioService;
import com.appquimia.appquimia.services.ProductoService;
import com.appquimia.appquimia.services.PublicidadService;
import com.appquimia.appquimia.services.SponsorService;
import com.appquimia.appquimia.services.SucursalService;
import com.appquimia.appquimia.services.TarifaFijaService;
import com.appquimia.appquimia.services.TarifaService;
import com.appquimia.appquimia.services.TiempoEsperaService;
import com.appquimia.appquimia.services.VersionService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.lang.reflect.Type;
import java.util.List;

public class FooActivity extends AppCompatActivity {
    private boolean mHsError;
    private static int mCount;
    static List<Premio> mPremios;
    static List<Publicidad> mPublicidades;
    static List<Sponsor> mSponsors;
    static long mInicio;
    static boolean mCracked;
    static String mVersionValue;

    protected static void setCount(int count) {
        mCount = count;
    }

    protected static synchronized void decCount() {
        mCount--;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foo);

        mVersionValue = "0";
        try {
            PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
            mVersionValue = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        if (Build.VERSION.SDK_INT >= 23) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            } else {
                procesSyncro();
            }
        } else {
            procesSyncro();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();


        final PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        assert pm != null;
        PowerManager.WakeLock mWakeLock = pm.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK, "appquimia:com.appquimia");
        mWakeLock.acquire(2*60*1000L /*10 minutes*/);

//        if(test) {
//            uploadTask async = new uploadTask();
//            String address = "ftp.appquimia.com";
//            String user = "admin@appquimia.com";
//            String pass = "admin2019";
//            String directory = "/";
//            async.execute(address, user, pass, directory);
//        } else
//            procesSyncro();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
        } else {
            procesSyncro();
        }
    }

    @Override
    public void onStop() {
        System.gc();
        super.onStop();
    }

    void procesSyncro() {
        mHsError = false;
        mCracked = false;
        OkHttpHandler okHttpHandler= new OkHttpHandler(this);
        okHttpHandler.execute();
    }

    public static class OkHttpHandler extends AsyncTask<Void, Void, Boolean> {
        AlertDialog.Builder mAlertDialog;
        OkHttpClient client = new OkHttpClient();
        boolean mCerrar;
        String mConductorJson;
        String mMovilJson;
        String mPremioJson;
        String mSponsorJson;
        String mPaqueteJson;
        String mClasificacionJson;
        String mPublicidadJson;
        String mTarifaJson;
        String mTarifaFijaJson;
        String mConductorXMovilJson;
        String mTiempoEsperaJson;
        String mConfiguracionTiempoJson;
        String mSucursalJson;
        String mProductoJson;

        private final WeakReference<AppCompatActivity> weakActivity;

        OkHttpHandler(AppCompatActivity myActivity) {
            this.weakActivity = new WeakReference<>(myActivity);
        }

        protected void onPreExecute() {
            super.onPreExecute();
            AppCompatActivity activity = weakActivity.get();
            mAlertDialog = new AlertDialog.Builder(activity);
        }

        @Override
        protected Boolean doInBackground(Void[] params) {
            AppCompatActivity activity = weakActivity.get();
            if (activity == null || activity.isFinishing()) {
                // activity is no longer valid, don't do anything!
                return false;
            }
            mCerrar = true;
            Request.Builder builder = new Request.Builder();

            builder.url(VersionService.API_GET_VERSION);
            Request reqVersion = builder.build();
            Response respVersion = null;
            try {
                respVersion = client.newCall(reqVersion).execute();
                String version = respVersion.body().string();
                Gson gson = new Gson();
                Type listType = new TypeToken<List<Version>>(){}.getType();
                List<Version> items = gson.fromJson(version, listType);

                if (items.get(0).getMantenimiento() == 1) {
                    showVersionStatus(activity, false, true);
                    mCerrar = false;
                    return true;
                }

                if (FooActivity.mVersionValue.equals(items.get(0).getVersion())) {
                    showVersionStatus(activity, true, false);
                    mCerrar = false;
                    return true;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }

            builder.url(ConductorService.API_CALL_LISTADO);
            Request reqConductor = builder.build();
            builder.url(MovilService.API_CALL_LISTADO);
            Request reqMovil = builder.build();
            String fechaUltModif = Utils.dateTimerEncode(Premio.getLastFechaModif(DatabaseHelper.getDBReadableInstance(activity)), "yyyy-MM-dd HH:mm:ss");
            String url = PremioService.API_CALL_NEWS.replace("?", fechaUltModif);
            builder.url(url);
            Request reqPremio = builder.build();
            builder.url(SponsorService.API_CALL_LISTADO);
            Request reqSponsor = builder.build();
            builder.url(PaqueteService.API_CALL_LISTADO);
            Request reqPaquete = builder.build();
            builder.url(ClasificacionService.API_CALL_LISTADO);
            Request reqClasificacion = builder.build();
            fechaUltModif = Publicidad.getLastFechaModif(DatabaseHelper.getDBReadableInstance(activity));
            url = PublicidadService.API_CALL_NEWS.replace("?", fechaUltModif);
            builder.url(url);
            Request reqPublicidad = builder.build();
            builder.url(TarifaService.API_CALL_LISTADO);
            Request reqTarifa = builder.build();
            builder.url(TarifaFijaService.API_CALL_LISTADO);
            Request reqTarifaFija = builder.build();
            builder.url(ConductorXMovilService.API_CALL_LISTADO);
            Request reqConductorXMovil = builder.build();
            builder.url(TiempoEsperaService.API_CALL_LISTADO);
            Request reqTiempoEspera = builder.build();
            builder.url(ConfiguracionTiempoService.API_CALL_LISTADO);
            Request reqConfiguracionTiempo = builder.build();
            builder.url(SucursalService.API_CALL_LISTADO);
            Request reqSucursal = builder.build();
            builder.url(ProductoService.API_CALL_LISTADO);
            Request reqProducto = builder.build();

            try {
                Response respConductor = client.newCall(reqConductor).execute();
                mConductorJson = respConductor.body().string();
                Response respMovil = client.newCall(reqMovil).execute();
                mMovilJson = respMovil.body().string();
                Response respPremio = client.newCall(reqPremio).execute();
                mPremioJson = respPremio.body().string();
                Response respSponsor = client.newCall(reqSponsor).execute();
                mSponsorJson = respSponsor.body().string();
                Response respPaquete = client.newCall(reqPaquete).execute();
                mPaqueteJson = respPaquete.body().string();
                Response respClasificacion = client.newCall(reqClasificacion).execute();
                mClasificacionJson = respClasificacion.body().string();
                Response respPublicidad = client.newCall(reqPublicidad).execute();
                mPublicidadJson = respPublicidad.body().string();
                Response respTarifa = client.newCall(reqTarifa).execute();
                mTarifaJson = respTarifa.body().string();
                Response respTarifaFija = client.newCall(reqTarifaFija).execute();
                mTarifaFijaJson = respTarifaFija.body().string();
                Response respConductorXMovil = client.newCall(reqConductorXMovil).execute();
                mConductorXMovilJson = respConductorXMovil.body().string();
                Response respTiempoEspera = client.newCall(reqTiempoEspera).execute();
                mTiempoEsperaJson = respTiempoEspera.body().string();
                Response respConfiguracionTiempo = client.newCall(reqConfiguracionTiempo).execute();
                mConfiguracionTiempoJson = respConfiguracionTiempo.body().string();
                Response respSucursal = client.newCall(reqSucursal).execute();
                mSucursalJson = respSucursal.body().string();
                Response respProducto = client.newCall(reqProducto).execute();
                mProductoJson = respProducto.body().string();

                FooActivity.mPremios = PremioService.extractImages(mPremioJson);
                FooActivity.mPublicidades = PublicidadService.extractImages(mPublicidadJson);
                FooActivity.mSponsors = SponsorService.extractImages(mSponsorJson);
                int cantSponsor = 0;
                for (Sponsor item : FooActivity.mSponsors){
                    if(!Patterns.WEB_URL.matcher(item.getImagen()).matches())
                        continue;
                    builder.url(item.getImagen());
                    Request reqImagen = builder.build();
                    client.newCall(reqImagen).enqueue(new SponsorImageCallback(activity, item));
                    cantSponsor++;
                }
                int cantPublicidades = 0;
                for (Publicidad item : FooActivity.mPublicidades){
                    builder.url(item.getUrl());
                    Request reqImagen = builder.build();
                    client.newCall(reqImagen).enqueue(new PublicidadImageCallback(activity, item));
                    cantPublicidades++;
                }
                int cantPremios = 0;
                for (Premio item : FooActivity.mPremios){
                    builder.url(item.getImagen());
                    Request reqImagen = builder.build();
                    client.newCall(reqImagen).enqueue(new PremioImageCallback(activity, item));
                    cantPremios++;
                }

                mInicio = System.currentTimeMillis();
                FooActivity.setCount(cantPublicidades + cantPremios + cantSponsor);
                ConnectivityManager cm =
                        (ConnectivityManager)activity.getSystemService(Context.CONNECTIVITY_SERVICE);

                assert cm != null;
                NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
                boolean isConnected;
                do{
                    isConnected = activeNetwork != null &&
                            activeNetwork.isConnectedOrConnecting();
                }while((FooActivity.mCount > 0)  && isConnected && !FooActivity.mCracked);// && Utils.isOnline());

                SQLiteDatabase db =  DatabaseHelper.getDBInstance(activity);
                if (FooActivity.mCount > 0) {
                    retoreBackup(activity, db);
                    return false;
                }

                DatabaseHelper.backup(activity);
                if(db.isOpen()) {
                    db.enableWriteAheadLogging();
                    String pragma = "PRAGMA temp_store = 2;PRAGMA cache_size = 100000;PRAGMA journal_mode = memory;PRAGMA synchronous = 0;";
                    db.execSQL(pragma);
                    db.beginTransaction();
                    try {
                        ConductorService.process(mConductorJson, db);
                        MovilService.process(mMovilJson, db);
                        PremioService.process(mPremioJson, db);
                        SponsorService.process(mSponsorJson, db);
                        PaqueteService.process(mPaqueteJson, db);
                        ClasificacionService.process(mClasificacionJson, db);
                        PublicidadService.process(mPublicidadJson, db);
                        TarifaService.process(mTarifaJson, db);
                        TarifaFijaService.process(mTarifaFijaJson, db);
                        ConductorXMovilService.process(mConductorXMovilJson, db);
                        TiempoEsperaService.process(mTiempoEsperaJson, db);
                        ConfiguracionTiempoService.process(mConfiguracionTiempoJson, db);
                        SucursalService.process(mSucursalJson, db);
                        ProductoService.process(mProductoJson, db);

                        for (Publicidad item : FooActivity.mPublicidades){
                            if(item.getImagenb() != null) {
                                item.save(db);
                            } else {
                                throw new Exception("Premio: imagen nula");
                            }
                        }

                        for (Premio item : FooActivity.mPremios){
                            if(item.getImagenb() != null) {
                                item.save(db);
                            } else {
                                throw new Exception("Publicidad: imagen nula");
                            }
                        }

                        for (Sponsor item : FooActivity.mSponsors){
                            if(item.getImagenb() != null) {
                                item.save(db);
                            } /*else {
                                throw new Exception("Sponsor: imagen nula");
                            }*/
                        }

                        db.setTransactionSuccessful();
                        db.endTransaction();

                        boolean verifyPremios = Premio.checkData(DatabaseHelper.getDBReadableInstance(activity));
                        boolean verifyPublicidades = Publicidad.checkData(DatabaseHelper.getDBReadableInstance(activity));

                        if(!verifyPremios || !verifyPublicidades) {
                            retoreBackup(activity, db);
                        } else if(db.isOpen()) {
                            db.close();
                        }

                    } catch (Exception e) {
                        retoreBackup(activity, db);
                        return false;
                    }
                }
            } catch (Exception e){
                return false;
            }
            return true;
        }

        protected void onPostExecute(Boolean exito) {
            super.onPostExecute(exito);
            AppCompatActivity activity = weakActivity.get();
            if (activity == null || activity.isFinishing()) {
                // activity is no longer valid, don't do anything!
                return;
            }
            if(mCerrar) {
                Intent target = new Intent(activity, AutenticacionActivity.class);
                activity.startActivity(target);
                activity.finish();
            }
        }

        private void retoreBackup(Context cnt, SQLiteDatabase db) {
            if(!((FooActivity) cnt).mHsError) {
                try {
                    ((FooActivity) cnt).mHsError = true;
                    if (db.inTransaction())
                        db.endTransaction();
                    if (DatabaseHelper.getDBInstance(cnt).isOpen())
                        DatabaseHelper.getDBInstance(cnt).close();
                    DatabaseHelper.restore(cnt);
                } catch (Exception e) {
                    Toast.makeText(cnt, "backup", Toast.LENGTH_LONG).show();
                }
            }
        }



        private void showVersionStatus(AppCompatActivity activity, boolean newVersion, boolean mantenimiento) {
            if (mantenimiento) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        mAlertDialog.setTitle("Appquimia")
                                .setMessage("Nuestros servidores se encuentran en mantenimiento. Por favor vuelva a intentar más tarde.")
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        AppCompatActivity activity = weakActivity.get();
                                        activity.finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher)
                                .show();
                    }
                });
                return;
            }

            if (newVersion) {
                activity.runOnUiThread(new Runnable() {
                    public void run() {
                        mAlertDialog.setTitle("Appquimia")
                                .setMessage("Se ha publicado una nueva versión. Por favor actualice.")
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        AppCompatActivity activity = weakActivity.get();
                                        try {
                                            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse("market://details?id=" + activity.getPackageName())));
                                        } catch (android.content.ActivityNotFoundException e) {
                                            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                                                    Uri.parse("http://play.google.com/store/apps/details?id=" + activity.getPackageName())));
                                        }
                                        activity.finish();
                                    }
                                })
                                .setIcon(R.mipmap.ic_launcher)
                                .show();
                    }
                });
            }

        }
    }

    public static class PremioImageCallback implements Callback {
        private final WeakReference<AppCompatActivity> weakActivity;
        Premio mPremio;

        PremioImageCallback(AppCompatActivity myActivity, Premio premio) {
            this.weakActivity = new WeakReference<>(myActivity);
            mPremio = premio;
        }

        @Override
        public void onFailure(Request request, IOException e) {
            AppCompatActivity activity = weakActivity.get();
            FooActivity.mCracked = true;
            if (activity == null || activity.isFinishing()) {
            }
        }

        @Override
        public void onResponse(Response response) throws IOException {
            AppCompatActivity activity = weakActivity.get();
            int index = FooActivity.mPremios.indexOf(mPremio);
            if (activity == null || activity.isFinishing()) {
                // activity is no longer valid, don't do anything!
                return;
            }
            BufferedInputStream bis = new BufferedInputStream(response.body().byteStream());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream(102400);
            byte[] data = new byte[10240];
            int current;
            while ((current = bis.read(data, 0, data.length)) != -1){
                buffer.write(data, 0, current);
            }
            mPremio.setImagenb(buffer.toByteArray());
            FooActivity.mPremios.set(index, mPremio);
            response.body().close();
            FooActivity.decCount();
            mInicio = System.currentTimeMillis();
        }
    }

    public static class PublicidadImageCallback implements Callback {
        private final WeakReference<AppCompatActivity> weakActivity;
        Publicidad mPublicidad;

        PublicidadImageCallback(AppCompatActivity myActivity, Publicidad publicidad) {
            this.weakActivity = new WeakReference<>(myActivity);
            mPublicidad = publicidad;
        }

        @Override
        public void onFailure(Request request, IOException e) {
            AppCompatActivity activity = weakActivity.get();
            FooActivity.mCracked = true;
            if (activity == null || activity.isFinishing()) {
                return;
            }
        }

        @Override
        public void onResponse(Response response) throws IOException {
            AppCompatActivity activity = weakActivity.get();
            int index = FooActivity.mPublicidades.indexOf(mPublicidad);
            if (activity == null || activity.isFinishing()) {
                // activity is no longer valid, don't do anything!
                return;
            }
            BufferedInputStream bis = new BufferedInputStream(response.body().byteStream());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream(102400);
            byte[] data = new byte[10240];
            int current;
            while ((current = bis.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, current);
            }
            mPublicidad.setImagenb(buffer.toByteArray());
//            mPublicidad.save(DatabaseHelper.getDBInstance(activity));
            FooActivity.mPublicidades.set(index, mPublicidad);
            response.body().close();
            FooActivity.decCount();
            mInicio = System.currentTimeMillis();
        }
    }

    public static class SponsorImageCallback implements Callback {
        private final WeakReference<AppCompatActivity> weakActivity;
        Sponsor mSponsor;

        SponsorImageCallback(AppCompatActivity myActivity, Sponsor sponsor) {
            this.weakActivity = new WeakReference<>(myActivity);
            mSponsor = sponsor;
        }

        @Override
        public void onFailure(Request request, IOException e) {
            AppCompatActivity activity = weakActivity.get();
            FooActivity.mCracked = true;
            if (activity == null || activity.isFinishing()) {
                return;
            }
        }

        @Override
        public void onResponse(Response response) throws IOException {
            AppCompatActivity activity = weakActivity.get();
            int index = FooActivity.mSponsors.indexOf(mSponsor);
            if (activity == null || activity.isFinishing()) {
                // activity is no longer valid, don't do anything!
                return;
            }
            BufferedInputStream bis = new BufferedInputStream(response.body().byteStream());
            ByteArrayOutputStream buffer = new ByteArrayOutputStream(102400);
            byte[] data = new byte[10240];
            int current;
            while ((current = bis.read(data, 0, data.length)) != -1) {
                buffer.write(data, 0, current);
            }
            mSponsor.setImagenb(buffer.toByteArray());
//            mSponsor.save(DatabaseHelper.getDBInstance(activity));
            FooActivity.mSponsors.set(index, mSponsor);
            response.body().close();
            FooActivity.decCount();
            mInicio = System.currentTimeMillis();
        }
    }

    class uploadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                easyFTP ftp = new easyFTP();
                File dbFile = FooActivity.this.getDatabasePath(DatabaseHelper.DATABASE_NAME);
                if(!dbFile.exists()) {
                    return null;
                }
                FileInputStream fis = new FileInputStream(dbFile);
                ftp.connect(params[0],params[1],params[2]);
                boolean status=false;
                if (!params[3].isEmpty()){
                    status=ftp.setWorkingDirectory(params[3]); // if User say provided any Destination then Set it , otherwise
                }                                              // Upload will be stored on Default /root level on server
                ftp.uploadFile(fis,DatabaseHelper.DATABASE_NAME);
                return new String("Upload Successful");
            }catch (Exception e){
                String t="Failure : " + e.getLocalizedMessage();
                return t;
            }
        }

        @Override
        protected void onPostExecute(String str) {
            Intent target = new Intent(FooActivity.this, AutenticacionActivity.class);
            startActivity(target);
            finish();
        }
    }
}
