package com.appquimia.appquimia;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageButton;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.adeel.library.easyFTP;
import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.CredentialsManager;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.ConductorXMovil;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Movil;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.entities.Sucursal;
import com.auth0.android.Auth0;
import com.auth0.android.authentication.AuthenticationAPIClient;
import com.auth0.android.authentication.AuthenticationException;
import com.auth0.android.callback.BaseCallback;
import com.auth0.android.management.ManagementException;
import com.auth0.android.management.UsersAPIClient;
import com.auth0.android.provider.AuthCallback;
import com.auth0.android.provider.WebAuthProvider;
import com.auth0.android.result.Credentials;
import com.auth0.android.result.UserProfile;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class AutenticacionActivity extends AppCompatActivity implements OnClickListener {
    DatabaseHelper mDBHelper;
    SQLiteDatabase mDatabase;

    private ImageView mLogo;
    private ImageView mLogoCentred;
    private View mProgressView;
    private View mLoginFormView;
    private FrameLayout mlFacebook;
    private FrameLayout mlTwitter;
    private FrameLayout mlGmail;

    private TextView mBienvenido;
    private TextView mBienvenidoCentred;

    private LinearLayout mlQR;
    private LinearLayout mlPremios;

    private Auth0 mAuth0;
    private UserProfile userProfile;
    private UsersAPIClient mUsersClient;
    private UserProfile mProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_autenticacion);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        if( getIntent().getBooleanExtra("close", false)){
            finish();
        }

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    protected void onResume() {
        super.onResume();

//        if (Utils.DEBUG) {
//            uploadTask async = new uploadTask();
//            String address = "ftp.appquimia.com";
//            String user = "admin@appquimia.com";
//            String pass = "admin2019";
//            String directory = "/";
//            async.execute(address, user, pass, directory);
//        }

        mLogo = findViewById(R.id.ivLogo);
        mLogoCentred = findViewById(R.id.ivLogoCentred);

        mBienvenido = findViewById(R.id.tvBienvenido);
        mBienvenidoCentred = findViewById(R.id.tvBienvenidoCentred);

        Button facebook = findViewById(R.id.login_facebook);
        facebook.setOnClickListener(this);
        Button twitter = findViewById(R.id.login_twitter);
        twitter.setOnClickListener(this);
        Button gmail = findViewById(R.id.login_gmail);
        gmail.setOnClickListener(this);

        mlFacebook = findViewById(R.id.lyLoginFacebook);
        mlTwitter = findViewById(R.id.lyLoginTwitter);
        mlGmail = findViewById(R.id.lyLoginGmail);

        AppCompatImageButton llamar = findViewById(R.id.llamar);
        llamar.setOnClickListener(this);
        AppCompatImageButton qr = findViewById(R.id.qr_code);
        qr.setOnClickListener(this);
        AppCompatImageButton premios = findViewById(R.id.premios);
        premios.setOnClickListener(this);

        mlQR = findViewById(R.id.lyQr);
        mlPremios = findViewById(R.id.lyPremios);

        mDBHelper = DatabaseHelper.getInstance(this);

        verifyToken();

        if(true) {
            Button syncFTP = findViewById(R.id.btnSyncFTP);
            syncFTP.setVisibility(View.VISIBLE);
            syncFTP.setOnClickListener(this);
        }
    }

    public void syncro() {
        uploadTask async = new uploadTask();
        String address = "ftp.appquimia.com";
        String user = "admin@appquimia.com";
        String pass = "admin2019";
        String directory = "/";
        async.execute(address, user, pass, directory);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // In some cases modifying newConfig leads to unexpected behavior,
        // so it's better to edit new instance.
        Configuration configuration = new Configuration(newConfig);
        Utils.adjustFontScale(getApplicationContext(), configuration);
    }

    private void drawForm(boolean logued) {
        if(logued) {
            mLogo.setVisibility(View.INVISIBLE);
            mLogoCentred.setVisibility(View.VISIBLE);

            mBienvenido.setVisibility(View.INVISIBLE);
            mBienvenidoCentred.setVisibility(View.VISIBLE);

            mlFacebook.setVisibility(View.INVISIBLE);
            mlTwitter.setVisibility(View.INVISIBLE);
            mlGmail.setVisibility(View.INVISIBLE);

            mlQR.setVisibility(View.VISIBLE);
            mlPremios.setVisibility(View.VISIBLE);
        } else {
            mLogo.setVisibility(View.VISIBLE);
            mLogoCentred.setVisibility(View.INVISIBLE);

            mBienvenido.setVisibility(View.VISIBLE);
            mBienvenidoCentred.setVisibility(View.INVISIBLE);

            mlFacebook.setVisibility(View.VISIBLE);
            mlTwitter.setVisibility(View.VISIBLE);
            mlGmail.setVisibility(View.VISIBLE);

            mlQR.setVisibility(View.GONE);
            mlPremios.setVisibility(View.GONE);
        }
    }

    private class LoginCallBack implements AuthCallback {
        @Override
        public void onSuccess(@NonNull final Credentials credentials) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    CredentialsManager.saveCredentials(AutenticacionActivity.this, credentials);
                    getUserInfo();
                    verifyToken();
                }
            });
        }

        @Override
        public void onFailure(@NonNull final Dialog dialog) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //dialog.show();
                    Toast.makeText(AutenticacionActivity.this, "Error: ", Toast.LENGTH_SHORT).show();
                    showProgress(false);
                }
            });
        }

        @Override
        public void onFailure(final AuthenticationException exception) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(AutenticacionActivity.this, "Error: " + exception.getMessage(), Toast.LENGTH_SHORT).show();
                    showProgress(false);
                }
            });
        }
    }

    private void verifyToken() {
        String accessToken = CredentialsManager.getCredentials(this).getAccessToken();
        if (accessToken != null) {
            drawForm(true);
        } else {
            drawForm(false);
            showProgress(false);
        }
    }

    private void getUserInfo() {
        String accessToken = CredentialsManager.getCredentials(this).getAccessToken();
        mAuth0 = new Auth0(this);
        mAuth0.setOIDCConformant(true);

        mUsersClient = new UsersAPIClient(mAuth0, CredentialsManager.getCredentials(this).getIdToken());
        AuthenticationAPIClient aClient = new AuthenticationAPIClient(mAuth0);
        assert accessToken != null;
        aClient.userInfo(accessToken)
                .start(new BaseCallback<UserProfile, AuthenticationException>() {
                    @Override
                    public void onSuccess(final UserProfile payload) {
                        userProfile = payload;
                        runOnUiThread(new Runnable() {
                            public void run() {
                                Toast.makeText(AutenticacionActivity.this, "Automatic Login Success", Toast.LENGTH_SHORT).show();

                                refreshUserInfo();
                                drawForm(true);
                                showProgress(false);
                            }
                        });
                    }

                    @Override
                    public void onFailure(AuthenticationException error) {
                        runOnUiThread(new Runnable() {
                            public void run() {
                                drawForm(false);
                                Toast.makeText(AutenticacionActivity.this, "Sesión Expirada, Por favor registrese.", Toast.LENGTH_SHORT).show();
                            }
                        });
                        CredentialsManager.deleteCredentials(AutenticacionActivity.this);
                    }
                });
    }

    public void refreshUserInfo() {
        mUsersClient.getProfile(userProfile.getId())
                .start(new BaseCallback<UserProfile, ManagementException>() {
                    @Override
                    public void onSuccess(UserProfile profile) {
                        mProfile = profile;
                        runOnUiThread(new Runnable() {
                            public void run() {
                                String sText = "Bienvenido " + mProfile.getName();
                                mBienvenido.setText(sText);
                                mBienvenidoCentred.setText(sText);
                                saveUserInfo();
                                drawForm(true);
                            }
                        });
                    }

                    @Override
                    public void onFailure(ManagementException error)
                    {
                        //show error
                    }
                });

    }

    private void saveUserInfo() {
        Cuenta cuenta = new Cuenta();

        if (!Utils.TEST) {
            String id;
            if (!TextUtils.isEmpty(mProfile.getEmail())) {
                id = mProfile.getEmail();
                cuenta.setEmail(mProfile.getEmail());
            } else if (mProfile.getExtraInfo().get("sub") != null) {
                id = mProfile.getExtraInfo().get("sub").toString();
                cuenta.setEmail("");
            } else {
                id = mProfile.getId();
                cuenta.setEmail("");
            }
            cuenta.setId(id);
            cuenta.setNombre(mProfile.getName());
            cuenta.setImagen(mProfile.getPictureURL());
            cuenta.setProvider(mProfile.getIdentities().get(0).getConnection());
        } else {
            cuenta.setId("personasde prueba@gmail.com");
            cuenta.setNombre("Tester");
            cuenta.setEmail("personasde prueba@gmail.com");
            cuenta.setImagen("https://files.merca20.com/uploads/2016/01/emma-watson-cambio-foto-de-perfil.jpg");
            cuenta.setProvider("testing");
        }

        try {
            mDatabase = mDBHelper.getWritableDatabase();
            mDatabase.enableWriteAheadLogging();
            cuenta.save(mDatabase);
        } catch (Exception e) {
            Log.v(this.getPackageName(), e.getMessage());
        } finally {
            if(mDatabase.isOpen()) {
                mDatabase.close();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.login_facebook:
                login("facebook");
                break;
            case R.id.login_twitter:
                login("twitter");
                break;
            case R.id.login_gmail:
                login("google-oauth2");
                break;
            case R.id.llamar:
                llamar();
                break;
            case R.id.qr_code:
                scanQR();
                break;
            case R.id.premios:
                showPremios();
                break;
            case R.id.btnSyncFTP:
                syncro();
                break;
        }
    }

    private void showPremios() {
        Intent target = new Intent(AutenticacionActivity.this, PremiosActivity.class);
        target.putExtra(PremiosActivity.FROM_LOGIN, true);
        startActivity(target);
    }

    private void llamar() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:3794433400"));
        startActivity(intent);
    }

    private void scanQR() {
        if (Utils.TEST) {
            processResults("S364dqKj;64dqKjc-199");
            return;
        }
        try {
            mDBHelper = DatabaseHelper.getInstance(this);
            mDatabase = mDBHelper.getWritableDatabase();
            mDatabase.enableWriteAheadLogging();
            if (mDatabase.isOpen())
                mDatabase.close();
            showProgress(true);
            IntentIntegrator integrator = new IntentIntegrator(this);
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
            integrator.setPrompt("Explorando...");
            integrator.setCameraId(0);
            integrator.setBeepEnabled(false);
            integrator.setBarcodeImageEnabled(false);
            integrator.initiateScan();
        } catch (Exception e) {
            Log.v(this.getPackageName(), e.getMessage());
        }
    }

    private void login(String connection) {
        if(Utils.TEST) {
            String sText = "Bienvenido TESTER";
            mBienvenido.setText(sText);
            mBienvenidoCentred.setText(sText);
            saveUserInfo();
            drawForm(true);
            return;
        }

        showProgress(true);
        mAuth0 = new Auth0(this);
        mAuth0.setOIDCConformant(false);
        WebAuthProvider.init(mAuth0)
                .withScheme("xxx")
                .withScope("openid email profile")
                .withConnection(connection)
                .withAudience("https://appquimia.auth0.com/userinfo")
                .start(AutenticacionActivity.this, new LoginCallBack());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {
            if (result.getContents() == null) {
                Log.d("AutenticacionActivity", "Cancelled scan");
                Toast.makeText(AutenticacionActivity.this, "Cancelado", Toast.LENGTH_LONG).show();
                showProgress(false);
            } else {
                Log.d("AutenticacionActivity", "Scanned");
                processResults(result.getContents());
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void processResults(String contents) {
        try {
            String[] fields = contents.split(";");

            if (fields[0].startsWith("CMC")) {
                enterCommerce(fields[2], fields[3]);
            } else if(fields[0].startsWith("STR")) {
                enterStreet();
            } else {
                String[] movilFlds = fields[1].split("-");
                enterMoviles(movilFlds[1]);
            }
        } catch (Exception ex) {
            Toast.makeText(this, "QR mal formado. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
            showProgress(false);
        }
    }

    private void enterMoviles(String movilLeido) {
        Movil movil = new Movil();

        if(movil.fillByNro(movilLeido, DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this)).length() != 0) {
            Toast.makeText(this, "El móvil no está registrado en APIPE. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
            showProgress(false);
            return;
        }

        if(movil.getBloqueaingreso() == 1) {
            Toast.makeText(this, "El móvil no está habilitado para usar la aplicación. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
            showProgress(false);
            return;
        }

        ArrayList conductores = new ArrayList();
        try {
            conductores = ConductorXMovil.getConductoresByIDMovil(movil.getId(), DatabaseHelper.getDBReadableInstance(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (conductores.size() == 0) {
            Toast.makeText(AutenticacionActivity.this, "El móvil no posee un conductor autorizado. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
            showProgress(false);
            return;
        }

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        String strDate = mdformat.format(calendar.getTime());

        Bitacora bitacora = new Bitacora();
        try {
            bitacora.fillLastGameMovil(DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String ultimoJuego = "19771212121212";
        String ultimoMovil = "-1";

        if (bitacora.getFecjuego() != null)
            ultimoJuego = bitacora.getFecjuego();

        if (bitacora.getIdmovil() != null)
            ultimoMovil = bitacora.getIdmovil();

        bitacora = new Bitacora();
        bitacora.setId(cuenta.getId() + strDate);
        bitacora.setIdcuenta(cuenta.getId());
        bitacora.setIdmovil(movil.getId());
        bitacora.setFecha(strDate);
        bitacora.setFecjuego("0");
        bitacora.setModulo(Constantes.MODULE_MOVILES);
        bitacora.save(DatabaseHelper.getDBInstance(AutenticacionActivity.this));

        SessionProcessor.setLastGameID(getApplicationContext(), -1);
        Intent target = new Intent(AutenticacionActivity.this, DashboardActivity.class);
        target.putExtra("LastGame", ultimoJuego);
        target.putExtra("LastMovil", ultimoMovil);
        startActivity(target);
        finish();
    }

    private void enterCommerce(String commerceLeido, String sucursalLeida) {
        Toast.makeText(this, "QR Correspondiente a Appquimia Commerce.", Toast.LENGTH_LONG).show();
        Sponsor sponsor = new Sponsor();

        if(sponsor.fillByID(Long.valueOf(commerceLeido), DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this)).length() != 0) {
            Toast.makeText(this, "El sponsor no está registrado en APPQUIMIA. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
            showProgress(false);
            return;
        }

        Sucursal sucursal = new Sucursal();
        if(sucursal.fillByID(sucursalLeida, DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this)).length() != 0) {
            Toast.makeText(this, "La sucursal no está registrada en APPQUIMIA. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
            showProgress(false);
            return;
        }

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        String strDate = mdformat.format(calendar.getTime());

        Bitacora bitacora = new Bitacora();
        try {
            bitacora.fillLastGameCommerce(DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String ultimoJuego = "19771212121212";

        if (bitacora.getFecjuego() != null)
            ultimoJuego = bitacora.getFecjuego();

        bitacora = new Bitacora();
        bitacora.setId(cuenta.getId() + strDate);
        bitacora.setIdcuenta(cuenta.getId());
        bitacora.setIdsponsor(sponsor.getId());
        bitacora.setIdsucursal(sucursal.getId());
        bitacora.setFecha(strDate);
        bitacora.setFecjuego("0");
        bitacora.setModulo(Constantes.MODULE_COMMERCE);
        bitacora.save(DatabaseHelper.getDBInstance(AutenticacionActivity.this));

        SessionProcessor.setLastGameID(getApplicationContext(), -1);
        Intent target = new Intent(AutenticacionActivity.this, DashboardCommerceActivity.class);
        target.putExtra("LastGame", ultimoJuego);
        startActivity(target);
        finish();
    }

    private void enterStreet() {
        Toast.makeText(this, "QR Correspondiente a Appquimia Street.", Toast.LENGTH_LONG).show();

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        String strDate = mdformat.format(calendar.getTime());

        Bitacora bitacora = new Bitacora();
        try {
            bitacora.fillLastGameStreet(DatabaseHelper.getDBReadableInstance(AutenticacionActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String ultimoJuego = "19771212121212";
        String ultimoMovil = "-1";

        if (bitacora.getFecjuego() != null)
            ultimoJuego = bitacora.getFecjuego();

        bitacora = new Bitacora();
        bitacora.setId(cuenta.getId() + strDate);
        bitacora.setIdcuenta(cuenta.getId());
        bitacora.setIdmovil(ultimoMovil);
        bitacora.setFecha(strDate);
        bitacora.setFecjuego("0");
        bitacora.setModulo(Constantes.MODULE_STREET);
        bitacora.save(DatabaseHelper.getDBInstance(AutenticacionActivity.this));

        SessionProcessor.setLastGameID(getApplicationContext(), -1);
        Intent target = new Intent(AutenticacionActivity.this, DashboardStreetActivity.class);
        target.putExtra("LastGame", ultimoJuego);
        startActivity(target);
        finish();
    }

//    public void processResults(String contents) {
//        String movilLeido;
//        try {
//            mDBHelper = DatabaseHelper.getInstance(this);
//            mDatabase = mDBHelper.getWritableDatabase();
//            mDatabase.enableWriteAheadLogging();
//
//            String[] fields = contents.split(";");
//            String[] movilFlds = fields[1].split("-");
//
//            movilLeido = movilFlds[1];
//
//            Movil movil = new Movil();
//
//            if(movil.fillByNro(movilLeido, DatabaseHelper.getDBReadableInstance(this)).length() != 0) {
//                Toast.makeText(AutenticacionActivity.this, "El móvil no está registrado en APIPE. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
//                showProgress(false);
//                return;
//            }
//
//            ArrayList conductores = ConductorXMovil.getConductoresByIDMovil(movil.getId(), DatabaseHelper.getDBReadableInstance(this));
//            if (conductores.size() == 0) {
//                Toast.makeText(AutenticacionActivity.this, "El móvil no posee un conductor autorizado. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
//                showProgress(false);
//                return;
//            }
//
//            Cuenta cuenta = new Cuenta();
//            cuenta.fill(DatabaseHelper.getDBReadableInstance(this));
//
//            Calendar calendar = Calendar.getInstance();
//            SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
//            String strDate = mdformat.format(calendar.getTime());
//
//            Bitacora bitacora = new Bitacora();
//            bitacora.fillLastGame(DatabaseHelper.getDBReadableInstance(this));
//            String ultimoJuego = "19771212121212";
//            String ultimoMovil = "-1";
//
//           if (bitacora.getFecjuego() != null)
//                ultimoJuego = bitacora.getFecjuego();
//
//            if (bitacora.getIdmovil() != null)
//                ultimoMovil = bitacora.getIdmovil();
//
//            bitacora = new Bitacora();
//            bitacora.setId(cuenta.getId() + strDate);
//            bitacora.setIdcuenta(cuenta.getId());
//            bitacora.setIdmovil(movil.getId());
//            bitacora.setFecha(strDate);
//            bitacora.setFecjuego("0");
//            bitacora.save(DatabaseHelper.getDBInstance(this));
//            if(DatabaseHelper.getDBInstance(this).isOpen())
//                DatabaseHelper.getDBInstance(this).close();
//
//            SessionProcessor.setLastGameID(getApplicationContext(), -1);
//            Intent target = new Intent(AutenticacionActivity.this, DashboardActivity.class);
//            target.putExtra("LastGame", ultimoJuego);
//            target.putExtra("LastMovil", ultimoMovil);
//            startActivity(target);
//            finish();
//        } catch (Exception ex) {
//            Toast.makeText(AutenticacionActivity.this, "QR mal formado. Por favor informe a Administración.", Toast.LENGTH_LONG).show();
//            showProgress(false);
//        } finally {
//            if(mDatabase.isOpen())
//                mDatabase.close();
//        }
//    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
        });
    }

    class uploadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            showProgress(true);
//            prg = new ProgressDialog(demo.this);
//            prg.setMessage("Uploading...");
//            prg.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                easyFTP ftp = new easyFTP();
                File dbFile = AutenticacionActivity.this.getDatabasePath(DatabaseHelper.DATABASE_NAME);
                if(!dbFile.exists()) {
                    return null;
                }
                FileInputStream fis = new FileInputStream(dbFile);
                ftp.connect(params[0],params[1],params[2]);
                boolean status=false;
                if (!params[3].isEmpty()){
                    status=ftp.setWorkingDirectory(params[3]); // if User say provided any Destination then Set it , otherwise
                }                                              // Upload will be stored on Default /root level on server
                ftp.uploadFile(fis,DatabaseHelper.DATABASE_NAME);
                return new String("Upload Successful");
            }catch (Exception e){
                String t="Failure : " + e.getLocalizedMessage();
                return t;
            }
        }

        @Override
        protected void onPostExecute(String str) {
            Toast.makeText(AutenticacionActivity.this,str,Toast.LENGTH_LONG).show();
            new AlertDialog.Builder(AutenticacionActivity.this)
                    .setTitle("Sincronización")
                    .setMessage("Sincronizada la base de datos")
                    .setPositiveButton(android.R.string.yes, null)
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
            showProgress(false);
        }
    }
}

