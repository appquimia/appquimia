package com.appquimia.appquimia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Tarifa;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;

public class TarifadorTabFragment extends Fragment implements View.OnClickListener {
    private static final String ARG_PARAM1 = "titulo";
    private static final String ARG_PARAM2 = "posicion";

    private String mTitulo;

    private EditText metInput;
    private EditText metCosto;

    DatabaseHelper mDBHelper;
    SQLiteDatabase mDatabase;
    private ArrayList<Tarifa> mArr;

    public TarifadorTabFragment() {
    }

    public static TarifadorTabFragment newInstance(String param1, String param2) {
        TarifadorTabFragment fragment = new TarifadorTabFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mTitulo = getArguments().getString(ARG_PARAM1);
//            String mPosicion = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tarifas_tab, container, false);

        TextView tvLeyenda = view.findViewById(R.id.tvLeyendaTag);
        Spanned spn = Html.fromHtml("Ingrese el valor del odómetro o distancia recorrida");
        tvLeyenda.setText(spn);

        metInput = view.findViewById(R.id.etInput);
        metInput.setFocusable(false);
        metInput.setClickable(false);

        metCosto = view.findViewById(R.id.etCosto);
        metCosto.setFocusable(false);
        metCosto.setClickable(false);

        Button nbtnTarifador1 = view.findViewById(R.id.btnTarifador1);
        nbtnTarifador1.setOnClickListener(this);
        Button nbtnTarifador2 = view.findViewById(R.id.btnTarifador2);
        nbtnTarifador2.setOnClickListener(this);
        Button nbtnTarifador3 = view.findViewById(R.id.btnTarifador3);
        nbtnTarifador3.setOnClickListener(this);

        Button nbtnTarifador4 = view.findViewById(R.id.btnTarifador4);
        nbtnTarifador4.setOnClickListener(this);
        Button nbtnTarifador5 = view.findViewById(R.id.btnTarifador5);
        nbtnTarifador5.setOnClickListener(this);
        Button nbtnTarifador6 = view.findViewById(R.id.btnTarifador6);
        nbtnTarifador6.setOnClickListener(this);

        Button nbtnTarifador7 = view.findViewById(R.id.btnTarifador7);
        nbtnTarifador7.setOnClickListener(this);
        Button nbtnTarifador8 = view.findViewById(R.id.btnTarifador8);
        nbtnTarifador8.setOnClickListener(this);
        Button nbtnTarifador9 = view.findViewById(R.id.btnTarifador9);
        nbtnTarifador9.setOnClickListener(this);

        Button nbtnTarifadorDot = view.findViewById(R.id.btnTarifadorDot);
        nbtnTarifadorDot.setOnClickListener(this);
        Button nbtnTarifador0 = view.findViewById(R.id.btnTarifador0);
        nbtnTarifador0.setOnClickListener(this);
        ImageButton nbtnTarifadorBack = view.findViewById(R.id.btnTarifadorBack);
        nbtnTarifadorBack.setOnClickListener(this);

        ImageButton mbtnGuardar = view.findViewById(R.id.btnGuardar);
        mbtnGuardar.setOnClickListener(this);

//        mDBHelper = DatabaseHelper.getInstance(getActivity());
//        mDatabase = mDBHelper.getWritableDatabase();
//        mDatabase.enableWriteAheadLogging();

        mArr = Tarifa.getList(DatabaseHelper.getDBReadableInstance(this.getActivity()));
//        mDatabase.close();

        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    public String getTitulo() {
        return mTitulo;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnTarifador1:
                inputNewNumber(1);
                break;
            case R.id.btnTarifador2:
                inputNewNumber(2);
                break;
            case R.id.btnTarifador3:
                inputNewNumber(3);
                break;
            case R.id.btnTarifador4:
                inputNewNumber(4);
                break;
            case R.id.btnTarifador5:
                inputNewNumber(5);
                break;
            case R.id.btnTarifador6:
                inputNewNumber(6);
                break;
            case R.id.btnTarifador7:
                inputNewNumber(7);
                break;
            case R.id.btnTarifador8:
                inputNewNumber(8);
                break;
            case R.id.btnTarifador9:
                inputNewNumber(9);
                break;
            case R.id.btnTarifador0:
                inputNewNumber(0);
                break;
            case R.id.btnTarifadorDot:
                inputDot();
                break;
            case R.id.btnTarifadorBack:
                inputBack();
                break;
            case R.id.btnGuardar:
                guardar();
                break;
        }
        calculate();
    }

    private void guardar() {
        mDBHelper = DatabaseHelper.getInstance(getActivity());
        mDatabase = mDBHelper.getWritableDatabase();
        mDatabase.enableWriteAheadLogging();

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(mDatabase);

        String sCosto = metCosto.getText().toString();
        sCosto = sCosto.replace('$', ' ').replace(',', '.').trim();
        double dCosto = Double.parseDouble(sCosto);
        bitacora.setCosto(dCosto);
        bitacora.save(mDatabase);
        mDatabase.close();

        Utils.showMessage(getActivity(), "Se ha registrado el costo de su viaje.", true);
    }

    private void calculate() {
        double value = Double.valueOf(metInput.getText().toString());
        double costo;
        int pos = 0;

        for (Tarifa tarifa : mArr) {
            if(value > tarifa.getKms())
                pos++;
            else
                break;
        }

        String text;
        if(pos >= mArr.size()) {
//            mDBHelper = DatabaseHelper.getInstance(getActivity());
//            mDatabase = mDBHelper.getReadableDatabase();
//            mDatabase.enableWriteAheadLogging();
            Tarifa item = Tarifa.getBase(DatabaseHelper.getDBReadableInstance(this.getActivity()));
//            mDatabase.close();

            costo = item.getValor() * value;
            DecimalFormat df = new DecimalFormat("##.00");
            df.setRoundingMode(RoundingMode.CEILING);
            text = "$ " + df.format(costo);
            metCosto.setText(text);
        } else {
            costo = mArr.get(pos).getValor();
            DecimalFormat df = new DecimalFormat("##.00");
            df.setRoundingMode(RoundingMode.CEILING);
            text = "$ " + df.format(costo);
            metCosto.setText(text);
        }
    }

    private void inputBack() {
        if(metInput.getText().toString().length() == 1)
            metInput.setText("0");
        else {
            String sNumber = metInput.getText().toString();
            sNumber = sNumber.substring(0, sNumber.length()-1);
            metInput.setText(sNumber);
        }
    }

    private void inputDot() {
        if(metInput.getText().toString().indexOf('.') < 0 ) {
            String sNumber = metInput.getText().toString();
            sNumber += '.';
            metInput.setText(sNumber);
        }
    }

    private void inputNewNumber(int input) {
        if(Float.valueOf(metInput.getText().toString()) == 0.00)
            metInput.setText(String.valueOf(input));
        else {
            String sNumber = metInput.getText().toString();
            sNumber += input;
            metInput.setText(sNumber);
        }
    }

//    private void showMessage(String mensaje)
//    {
//        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(getActivity());
//        dlgAlert.setMessage(mensaje);
//        dlgAlert.setTitle("Appquimia");
//        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//                getActivity().finish();
//            }
//        });
//        dlgAlert.setCancelable(true);
//        dlgAlert.create().show();
//    }
}
