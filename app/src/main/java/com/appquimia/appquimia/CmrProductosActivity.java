package com.appquimia.appquimia;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.appquimia.appquimia.adapters.CarouselProductosAdapter;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Producto;
import com.appquimia.appquimia.entities.Sponsor;
import com.appquimia.appquimia.services.ProductoService;
import com.appquimia.appquimia.services.VolleySingleton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;

public class CmrProductosActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawer;
    private RecyclerView.LayoutManager mlmProductos;
    private RecyclerView.Adapter madpProductos;
    private ArrayList<Producto> marrProductos;
    private ImageView mImagen;
    private TextView mSponsor;
    private NavigationView mNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_cmr_productos);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(R.id.dlCmrProductos);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        mNavigationView = findViewById(R.id.nvCmrProductos);
        mNavigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = mNavigationView.getMenu();
        mNavigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                CmrProductosActivity.this.mNavigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    CmrProductosActivity.this.mNavigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_cmr_productos);
        }

        mImagen = findViewById(R.id.imgLogo);
        mSponsor = findViewById(R.id.tvSponsor);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(CmrProductosActivity.this));
        Sponsor sponsor = new Sponsor();
        sponsor.fillByID(bitacora.getIdsponsor(), DatabaseHelper.getDBReadableInstance(CmrProductosActivity.this));

        Bitmap theImage;
        if(sponsor.getImagenb() != null) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(sponsor.getImagenb());
            theImage = BitmapFactory.decodeStream(imageStream);
        } else {
            theImage = BitmapFactory.decodeResource(getResources(), R.drawable.sponsor_default);
        }
        int displayWidth = Utils.getScreenSize(this).x;
        int displayHeight = Utils.getScreenSize(this).y;

        mImagen.setImageBitmap(theImage);
        mSponsor.setText(sponsor.getRazonsocial());

        createLayoutManager();
        RecyclerView rvProductos = findViewById(R.id.rvProductos);
        rvProductos.setHasFixedSize(true);
        rvProductos.setLayoutManager(mlmProductos);

        marrProductos = Producto.getProductosByIdSponsor(sponsor.getId(), DatabaseHelper.getDBReadableInstance(CmrProductosActivity.this));
        madpProductos = new CarouselProductosAdapter(marrProductos);

        ((CarouselProductosAdapter) madpProductos).setWidth(displayWidth);
        ((CarouselProductosAdapter) madpProductos).setHeight(displayHeight);
        rvProductos.setAdapter(madpProductos);

//        setProductosData(sponsor.getId());
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_marca) {
            NavigationOptions.opMarca(this, true);
        }  else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugarCommerce(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremiosCommerce(this, true);
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificarCommerce(this, false);
        } else if (id == R.id.nav_recomendar) {
            NavigationOptions.opRecomentdarCommerce(this, false);
        } else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    private void createLayoutManager() {
        mlmProductos = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false){
            @Override
            public RecyclerView.LayoutParams generateDefaultLayoutParams() {
                return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            }
        };
    }

    private void setProductosData(long id) {
        String customUrl = ProductoService.API_CALL_BYIDSPONSOR.replace("?", id + "");
        JsonArrayRequest jsonreq = new JsonArrayRequest(customUrl,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {

                        for (int i = 0; i < response.length(); i++) {
                            try {
                                JSONObject obj = response.getJSONObject(i);
                                Producto prod = new Producto();
                                prod.setId(obj.getString("id"));
                                prod.setIdsponsor(obj.getInt("idsponsor"));
                                prod.setProducto(obj.getString("producto"));
                                prod.setDescripcion(obj.getString("descripcionextra"));
                                prod.setImagen(obj.getString("imagen"));
                                marrProductos.add(prod);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        madpProductos.notifyDataSetChanged();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Utils.showMessage(CmrProductosActivity.this,"No dispone de conexión. Por favor verifique", false);
                    }
        });
        VolleySingleton volley = VolleySingleton.getInstance(getApplicationContext());
        volley.addToRequestQueue(jsonreq);
    }

}
