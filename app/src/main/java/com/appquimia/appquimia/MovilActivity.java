package com.appquimia.appquimia;

import android.content.Context;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Conductor;
import com.appquimia.appquimia.entities.ConductorXMovil;
import com.appquimia.appquimia.entities.Movil;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;

public class MovilActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
//    DatabaseHelper mDBHelper;
//    SQLiteDatabase mDatabase;
    private ImageView mImagenMovil;

    ConductoresAdapter mAdapter;
    String mIDMovil;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_movil);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.dlMovil);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nvMovil);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                MovilActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    MovilActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_movil);
        }

        mImagenMovil = findViewById(R.id.ivMovil);

//        mDBHelper = DatabaseHelper.getInstance(this);
//        mDatabase = mDBHelper.getWritableDatabase();
//        mDatabase.enableWriteAheadLogging();

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(this));
        Movil movil = new Movil();
        mIDMovil = bitacora.getIdmovil();
        movil.fillByID(mIDMovil, DatabaseHelper.getDBReadableInstance(this));

        setMovilImage(movil);

        TextView mNumero = findViewById(R.id.tvMovil);
        TextView mDescripcion = findViewById(R.id.tvDescripcionMovil);
        TextView mPatente = findViewById(R.id.tvPatente);

        Spanned text = Html.fromHtml("<b>\u2022 Móvil Nº: </b>" + movil.getNromovil());
        mNumero.setText(text);
        text = Html.fromHtml("<b>\u2022 Descripción del móvil: </b>" + movil.getDescripcion());
        mDescripcion.setText(text);
        text = Html.fromHtml("<b>\u2022 Patente: </b>" + movil.getDominio());
        mPatente.setText(text);

        ListView mConductores = findViewById(R.id.lstConductores);
        mConductores.setDivider(null);
        ArrayList mArr = new ArrayList();
        try {
            mArr = ConductorXMovil.getConductoresByIDMovil(mIDMovil, DatabaseHelper.getDBReadableInstance(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        mAdapter = new ConductoresAdapter(this, R.id.lstConductores, mArr);
        mConductores.setAdapter(mAdapter);
//        mDatabase.close();
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_movil) {

        } else if (id == R.id.nav_tarifador) {
            NavigationOptions.opTarifador(this, true);
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificar(this, true);
        } else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugar(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremios(this, true);
        } else if (id == R.id.nav_bitacora) {
            NavigationOptions.opBitacora(this, true);
        }else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        DrawerLayout drawer = findViewById(R.id.dlMovil);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void setMovilImage(Movil movil) {
        if(movil.getImagenb() != null) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(movil.getImagenb());

            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            mImagenMovil.setImageBitmap(theImage);
        }
    }

    @Override
    public void onClick(View view) {
        finish();
    }

    private class ConductoresAdapter extends ArrayAdapter<ConductorXMovil> {
        protected List<ConductorXMovil> items;

        ConductoresAdapter(Context context, int textViewResourceId, List<ConductorXMovil> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public ConductorXMovil getItem(int pos) {
            return items.get(pos);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert vi != null;
                v = vi.inflate(R.layout.item_list_conductores, null);
            }

            ConductorXMovil cxm = items.get(position);
            Conductor c = new Conductor();
//            DatabaseHelper dbHelper = DatabaseHelper.getInstance(MovilActivity.this);
//            SQLiteDatabase database = dbHelper.getWritableDatabase();

            c.fillByID(cxm.getIdconductor(), DatabaseHelper.getDBReadableInstance(MovilActivity.this) );
            ImageView f3 = v.findViewById(R.id.imgConductor);
            //byte[] outImage = c.getImagenb();
            if(c.getImagenb() != null) {
                ByteArrayInputStream imageStream = new ByteArrayInputStream(c.getImagenb());

                Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                f3.setImageBitmap(theImage);
            }
            TextView f1 = v.findViewById(R.id.tvApellidos);
            f1.setText(c.getApellidos());
            TextView f2 = v.findViewById(R.id.tvNombres);
            f2.setText(c.getNombres());

//            database.close();
            return v;
        }
    }

}
