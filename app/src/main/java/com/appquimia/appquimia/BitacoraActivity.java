package com.appquimia.appquimia;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Conductor;
import com.appquimia.appquimia.entities.ConductorXMovil;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Movil;

import java.util.ArrayList;
import java.util.Locale;

public class BitacoraActivity extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_bitacora);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        // Sección acttion bar, toolbar, navbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.dlBitacora);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nvBitacora);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                BitacoraActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    BitacoraActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_bitacoras);
        }

        // Sección comportamiento particular
        ListView mLista = findViewById(R.id.lstViajes);

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(this));

        ArrayList<Bitacora> marrViajes = Bitacora.getListByIdCuenta(cuenta.getId(), DatabaseHelper.getDBReadableInstance(this));

        BitacoraAdapter mAdapter = new BitacoraAdapter(this, R.id.lstViajes, marrViajes);
        mLista.setAdapter(mAdapter);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_movil) {
            NavigationOptions.opMovil(this, true);
        } else if (id == R.id.nav_tarifador) {
            NavigationOptions.opTarifador(this, true);
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificar(this, true);
        } else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugar(this, true);
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremios(this, true);
        } else if (id == R.id.nav_bitacora) {
            //NavigationOptions.opBitacora(this, true);
        }else if (id == R.id.nav_fin) {
            NavigationOptions.finish(this);
        }

        DrawerLayout drawer = findViewById(R.id.dlBitacora);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View view) {
        finish();
    }

    private class BitacoraAdapter extends ArrayAdapter<Bitacora> {
        protected ArrayList<Bitacora> items;

        BitacoraAdapter(Context context, int textViewResourceId, ArrayList<Bitacora> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public Bitacora getItem(int pos) {
            return items.get(pos);
        }

        @SuppressLint("InflateParams")
        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert vi != null;
                v = vi.inflate(R.layout.item_list_bitacoras, null);
            }

            Bitacora s = items.get(position);

            if(Utils.mod(position, 2) == 0)
                v.setBackgroundColor(Color.parseColor("#76653C"));
            else
                v.setBackgroundColor(Color.parseColor("#A3965B"));
            if (s != null) {

                Movil movil = new Movil();
                movil.fillByID(s.getIdmovil(), DatabaseHelper.getDBReadableInstance(BitacoraActivity.this));

                Conductor conductor = null;
                ArrayList marr = new ArrayList();
                try {
                    marr = ConductorXMovil.getConductoresByIDMovil(s.getIdmovil(), DatabaseHelper.getDBReadableInstance(BitacoraActivity.this));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if(marr.size() > 0) {
                    ConductorXMovil condxmov = (ConductorXMovil) marr.get(0);
                    conductor = new Conductor();

                    if (condxmov != null) {
                        conductor.fillByID(condxmov.getIdconductor(), DatabaseHelper.getDBReadableInstance(BitacoraActivity.this));
                    }
                }

                TextView f1 = v.findViewById(R.id.tvFechaYHora);
                f1.setText(Utils.longToDate(s.getFecha()));

                TextView f2 = v.findViewById(R.id.tvNumeroMovil);
                f2.setText(movil.getNromovil());

                TextView f3 = v.findViewById(R.id.tvConductor);
                String value = "Sin conductor";
                if(conductor != null) {
                    value = conductor.getApellidos() + ", " + conductor.getNombres();
                    f3.setText(value);
                }
                f3.setText(value);

                TextView f4 = v.findViewById(R.id.tvCosto);
                value = "No valorizado";
                if(s.getCosto() > 0) {
                    value = String.format(new Locale("es", "AR"), "$ %.2f", s.getCosto());
                    f4.setText(value);
                }
                f4.setText(value);
            }
            return v;
        }
    }
}
