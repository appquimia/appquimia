package com.appquimia.appquimia;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adeel.library.easyFTP;
import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.entities.Sponsor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Random;

public class ResumenActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener, NavigationView.OnNavigationItemSelectedListener {
    public static final String TIPO_MODULO = "tipo_modulo";

    private DrawerLayout mDrawer;
    private PremiosAdapter mAdapter;

    static boolean mActive = false;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());

        int dl = R.id.dlResumen;
        int nv = R.id.nvResumen;
        int tab = R.layout.title_action_bar_resumen;

        switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
            case Constantes.MODULE_MOVILES:
                setContentView(R.layout.activity_resumen);
                dl = R.id.dlResumen;
                nv = R.id.nvResumen;
                tab = R.layout.title_action_bar_premios;
                break;
            case Constantes.MODULE_COMMERCE:
                setContentView(R.layout.activity_cmr_resumen);
                dl = R.id.dlCmrResumen;
                nv = R.id.nvCmrResumen;
                tab = R.layout.title_action_bar_premios;
                break;
            case Constantes.MODULE_STREET:
                setContentView(R.layout.activity_str_resumen);
                dl = R.id.dlStrResumen;
                nv = R.id.nvStrResumen;
                tab = R.layout.title_action_bar_resumen;
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        // Sección acttion bar, toolbar, navbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(dl);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(nv);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                ResumenActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    ResumenActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(tab);
        }

        // Sección comportamiento particular
        ListView lista = findViewById(R.id.lstPremios);
        TextView usuario = findViewById(R.id.tvUsuario);

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(ResumenActivity.this));
        usuario.setText(cuenta.getNombre());

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(ResumenActivity.this));

        ArrayList<PremioXCuenta> arrPremios = PremioXCuenta.getListByIdBitacora(bitacora.getId(), DatabaseHelper.getDBReadableInstance(ResumenActivity.this));

        mAdapter = new PremiosAdapter(this, R.id.lstPremios, arrPremios);
        lista.setAdapter(mAdapter);
        lista.setOnItemClickListener(this);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_MOVILES) {
            if (id == R.id.nav_movil) {
                NavigationOptions.opMovil(this, true);
            } else if (id == R.id.nav_tarifador) {
                NavigationOptions.opTarifador(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificar(this, true);
            } else if (id == R.id.nav_jugar) {
                NavigationOptions.opJugar(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremios(this, true);
            } else if (id == R.id.nav_bitacora) {
                NavigationOptions.opBitacora(this, true);
            }else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else  if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_COMMERCE) {
            if (id == R.id.nav_marca) {
                NavigationOptions.opMarca(this, true);
            }  else if (id == R.id.nav_jugar) {
//                NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosCommerce(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else {
            if (id == R.id.nav_auspiciantes) {
                NavigationOptions.opAuspiciantes(this, true);
            }  else if (id == R.id.nav_jugar) {
                //NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosStreet(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActive = true;
        final Handler hdPublicidad = new Handler();
        hdPublicidad.postDelayed(new Runnable() {
            public void run() {
                showPublicidad(DatabaseHelper.getDBReadableInstance(ResumenActivity.this));
                if (mActive)
                    hdPublicidad.postDelayed(this, 10000);
            }
        }, 500);
    }

    @Override
    protected void  onPause() {
        mActive = false;
        super.onPause();
    }

    @Override
    protected void onStop() {
        mActive = false;
        super.onStop();
    }

    private void showPublicidad(SQLiteDatabase db) {
        Publicidad pub1 = new Publicidad();
        Publicidad pub2 = new Publicidad();
        Random randomGenerator = new Random();
        float randomInt;

        while (pub1.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub1.fill(-1, randomInt, db);
        }

        while (pub2.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub2.fill(pub1.getId(), randomInt, db);
        }

        ByteArrayInputStream imageStream;

        if(pub1.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub1.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice1);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }

        if(pub2.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub2.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice2);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }
    }

    @Override
    public void onClick(View view) {
        finish();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        PremioXCuenta pxc = mAdapter.getItem(position);
        assert pxc != null;
        SessionProcessor.setPremioID(getApplicationContext(), pxc.getId());

        Intent intent = new Intent();
        int tipo = getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES);
        intent.putExtra(JuegoActivity.TIPO_MODULO, tipo);
        intent.setClass(this, PremioActivity.class);
        startActivity(intent);
    }

    private class PremiosAdapter extends ArrayAdapter<PremioXCuenta> {
        protected ArrayList<PremioXCuenta> items;

        PremiosAdapter(Context context, int textViewResourceId, ArrayList<PremioXCuenta> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public PremioXCuenta getItem(int pos) {
            return items.get(pos);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            ResumenActivity.PremiosAdapter.ItemView holder;
            Spanned text;

            LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.item_list_premios, null);
            holder = new ResumenActivity.PremiosAdapter.ItemView(convertView);

            PremioXCuenta s = items.get(position);
            if (s != null) {
                Premio premio = new Premio();
                premio.fillByID(s.getIdpremio(), DatabaseHelper.getDBReadableInstance(ResumenActivity.this));//mDatabase);

                Sponsor sponsor = new Sponsor();
                sponsor.fillByID(premio.getIdsponsor(), DatabaseHelper.getDBReadableInstance(ResumenActivity.this));//mDatabase);

                holder.mSponsor.setText(sponsor.getRazonsocial());
                holder.mDescripcion.setText(premio.getDescripcion());
                text = Html.fromHtml("<b>Código: </b>" + premio.getCodigo());
                holder.mCodigo.setText(text);
                text = Html.fromHtml("<b>Vencimiento: </b>" + premio.getFecven());
                holder.mVencimiento.setText(text);

                holder.mUrl = premio.getImagenb();

                if (premio.getImagenb() != null) {
                    ByteArrayInputStream imageStream = new ByteArrayInputStream(premio.getImagenb());
                    Bitmap theImage = BitmapFactory.decodeStream(imageStream);
                    holder.mPremio.setImageBitmap(theImage);
                } else {
                    holder.mPremio.setBackgroundResource(R.drawable.premio);
                }
                convertView.setTag(holder);
            }

            return convertView;
        }

        class ItemView extends RecyclerView.ViewHolder {
            private TextView mSponsor;
            private TextView mDescripcion;
            private TextView mCodigo;
            private TextView mVencimiento;
            private ImageView mPremio;
            private byte[] mUrl;

            ItemView(View itemView) {
                super(itemView);

                mSponsor = itemView.findViewById(R.id.tvSponsor);
                mDescripcion = itemView.findViewById(R.id.tvDescripcionPremio);
                mCodigo = itemView.findViewById(R.id.tvCodigoPremio);
                mVencimiento = itemView.findViewById(R.id.tvVencimiento);
                mPremio = itemView.findViewById(R.id.imgPremio);

                mUrl=null;
            }
        }

//        public class ImageLoader extends AsyncTask<ResumenActivity.PremiosAdapter.ItemView, String, Bitmap> {
//            private ResumenActivity.PremiosAdapter.ItemView v;
//            private String mImage;
//
//            @Override
//            protected Bitmap doInBackground(ResumenActivity.PremiosAdapter.ItemView... params) {
//                v = params[0];
//                Bitmap theImage;
//
//                ByteArrayInputStream imageStream = new ByteArrayInputStream(v.mUrl);
//                if (v.mUrl != null) {
//                    theImage = BitmapFactory.decodeStream(imageStream);
//                } else {
//                    theImage = BitmapFactory.decodeResource(getResources(), R.drawable.premio);
//                }
//
//                return theImage;
//            }
//
//            @Override
//            protected void onPostExecute(Bitmap result) {
//                super.onPostExecute(result);
//                if (v != null) {
//                    v.mPremio.setImageBitmap(result);
//                    v.mPremio.setVisibility(View.VISIBLE);
//                }
//            }
//        }
    }


    class uploadTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
//            prg = new ProgressDialog(demo.this);
//            prg.setMessage("Uploading...");
//            prg.show();
        }
        @Override
        protected String doInBackground(String... params) {
            try {
                easyFTP ftp = new easyFTP();
                File dbFile = ResumenActivity.this.getDatabasePath(DatabaseHelper.DATABASE_NAME);
                if(!dbFile.exists()) {
                    return null;
                }
                FileInputStream fis = new FileInputStream(dbFile);
                ftp.connect(params[0],params[1],params[2]);
                boolean status=false;
                if (!params[3].isEmpty()){
                    status=ftp.setWorkingDirectory(params[3]); // if User say provided any Destination then Set it , otherwise
                }                                              // Upload will be stored on Default /root level on server
                ftp.uploadFile(fis,DatabaseHelper.DATABASE_NAME);
                return new String("Upload Successful");
            }catch (Exception e){
                String t="Failure : " + e.getLocalizedMessage();
                return t;
            }
        }

        @Override
        protected void onPostExecute(String str) {
            Toast.makeText(ResumenActivity.this,str,Toast.LENGTH_LONG).show();
        }
    }
}
