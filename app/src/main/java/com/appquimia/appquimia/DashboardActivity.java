package com.appquimia.appquimia;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.appquimia.appquimia.classes.CustomTFSpan;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Calificacion;
import com.appquimia.appquimia.entities.Cuenta;
import com.appquimia.appquimia.entities.Movil;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.entities.Reclamo;
import com.appquimia.appquimia.services.CalificacionService;
import com.appquimia.appquimia.services.CuentaService;
import com.appquimia.appquimia.services.PremioXCuentaService;
import com.appquimia.appquimia.services.ReclamoService;
import com.appquimia.appquimia.services.VolleySingleton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class DashboardActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener
{
    static boolean active = false;
    SQLiteDatabase mDatabase;
    long mLastGame;
    String mLastMovil;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());
        setContentView(R.layout.activity_dashboard);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                DashboardActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    DashboardActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font

                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });
        assert getSupportActionBar() != null;
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        assert wm != null;
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    private void showPublicidad(SQLiteDatabase db) {
        Publicidad pub1 = new Publicidad();
        Publicidad pub2 = new Publicidad();
        Random randomGenerator = new Random();
        float randomInt;

        while (pub1.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub1.fill(-1, randomInt, db);
        }

        while (pub2.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub2.fill(pub1.getId(), randomInt, db);
        }

        ByteArrayInputStream imageStream;

        if(pub1.getImagenb() != null)
        {
            imageStream = new ByteArrayInputStream(pub1.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice1);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }

        if(pub2.getImagenb() != null)
        {
            imageStream = new ByteArrayInputStream(pub2.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice2);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Intent target = getIntent();
        mLastGame = Long.parseLong(target.getStringExtra("LastGame"));
        mLastMovil = target.getStringExtra("LastMovil");

        SessionProcessor.setLastGame(this, mLastGame + "");
        SessionProcessor.setLastMovil(this, mLastMovil);

        ImageView mAvatarUsuario = findViewById(R.id.imgAvatar) ;

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));

        Picasso.get()
                .load(cuenta.getImagen())
                .into(mAvatarUsuario);

        TextView mUsuario = findViewById(R.id.tvUsuario);
        mUsuario.setText(cuenta.getNombre());

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));//mDatabase);
        Movil movil = new Movil();
        movil.fillByID(bitacora.getIdmovil(), DatabaseHelper.getDBReadableInstance(DashboardActivity.this));//mDatabase);
        TextView mMovil = findViewById(R.id.tvMovil) ;
        String val = "Movil: " + movil.getNromovil();
        mMovil.setText(val);

        boolean mHayPremios = Premio.getPremiosByIDPaquete(0, DatabaseHelper.getDBReadableInstance(DashboardActivity.this)).size() > 0;
        SessionProcessor.setExistPremios(this, mHayPremios);

        ListView mLista = findViewById(R.id.recOpciones);
        List<String> mArrLogs = Arrays.asList("Móvil", "Tarifador", "Calificar", "Jugar", "Premios", "Bitácora", "Finalizar Viaje");
        OpcionesAdapter mAdapter = new OpcionesAdapter(this, R.id.recOpciones, mArrLogs);
        mLista.setAdapter(mAdapter);
        mLista.setOnItemClickListener(new ItemClickListener());

        active = true;

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                syncCuenta();
                syncReclamos();
                syncCalificaciones();
                syncPremios();
                syncCanjeos();
                if(active)
                    handler.postDelayed(this, 36000);
            }
        }, 500);

        final Handler hdPublicidad = new Handler();
        hdPublicidad.postDelayed(new Runnable() {
            public void run() {
                DashboardActivity.this.
                showPublicidad(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
                if(active)
                    hdPublicidad.postDelayed(this, 10000);
            }
        }, 500);
    }

    @Override
    protected void onNewIntent (Intent intent) {
        setIntent(intent);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_movil) {
            NavigationOptions.opMovil(this, false);//opMovil();
        } else if (id == R.id.nav_tarifador) {
            NavigationOptions.opTarifador(this, false);//opTarifador();
        } else if (id == R.id.nav_calificar) {
            NavigationOptions.opCalificar(this, false);//opCalificar();
        } else if (id == R.id.nav_jugar) {
            NavigationOptions.opJugar(this, false);//opJugar();
        } else if (id == R.id.nav_premios) {
            NavigationOptions.opPremios(this, false);//opPremios();
        } else if (id == R.id.nav_bitacora) {
            NavigationOptions.opBitacora(this, false);//opBitacora();
        }else if (id == R.id.nav_fin) {
            showByeMessage();
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//    private void opMovil() {
//        Intent intent = new Intent();
//        intent.setClass(DashboardActivity.this, MovilActivity.class);
//        startActivity(intent);
//    }
//
//    private void opTarifador() {
//        Intent intent = new Intent();
//        intent.setClass(DashboardActivity.this, TarifasActivity.class);
//        startActivity(intent);
//    }
//
//    private void opCalificar() {
//        Intent intent = new Intent();
//        intent.setClass(DashboardActivity.this, CalificarActivity.class);
//        startActivity(intent);
//    }
//
//    private void opJugar() {
//        if(mHayPremios) {
//            Intent intent = new Intent();
//            if (SessionProcessor.getLastGameId(getApplicationContext()) == -1) {
//                Calendar calendar = Calendar.getInstance();
//                SimpleDateFormat fActual = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
//                long lActual = Long.parseLong(fActual.format(calendar.getTime()));
//
//                Bitacora bitacora = new Bitacora();
//                bitacora.fillLast(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
//
//                Intent target = getIntent();
//                mLastGame = Long.parseLong(target.getStringExtra("LastGame"));
//                mLastMovil = target.getStringExtra("LastMovil");
//
//                if(!Utils.DEBUG) {
//                    if (bitacora.getIdmovil().equals(mLastMovil) && (lActual - mLastGame) < 10000) {
//                        showMessage();
//                        return;
//                    }
//                }
//                intent.setClass(DashboardActivity.this, JuegoActivity.class);
//                startActivity(intent);
//            } else
//                Toast.makeText(getApplicationContext(), "Ya ha jugado!!", Toast.LENGTH_LONG).show();
//        } else
//            Toast.makeText(getApplicationContext(), "No hay premios disponibles", Toast.LENGTH_LONG).show();
//    }
//
//    private void opPremios() {
//        Intent intent = new Intent();
//        intent.setClass(DashboardActivity.this, PremiosActivity.class);
//        startActivity(intent);
//    }
//
//    private void opBitacora() {
//        Intent intent = new Intent();
//        intent.setClass(DashboardActivity.this, BitacoraActivity.class);
//        startActivity(intent);
//    }

    private void deleteRecursive(File fileOrDirectory) {
        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

        try {
            if (fileOrDirectory.isDirectory() && fileOrDirectory.getCanonicalPath().equals(this.getCacheDir().getCanonicalPath()))
                return;
        } catch (IOException e) {
            e.printStackTrace();
        }

        //noinspection ResultOfMethodCallIgnored
        fileOrDirectory.delete();
    }

    @Override
    protected void  onPause() {
        active = false;
        System.gc();
        deleteRecursive(this.getCacheDir());
        super.onPause();
    }

    @Override
    protected void onStop(){
        active = false;
        System.gc();
        deleteRecursive(this.getCacheDir());
        super.onStop();
    }

    private class ItemClickListener implements AdapterView.OnItemClickListener {
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            switch (position) {
                case 0:
                    NavigationOptions.opMovil(DashboardActivity.this, false);
                    break;
                case 1:
                    NavigationOptions.opTarifador(DashboardActivity.this, false);
                    break;
                case 2:
                    NavigationOptions.opCalificar(DashboardActivity.this, false);
                    break;
                case 3:
                    NavigationOptions.opJugar(DashboardActivity.this, false);
                    break;
                case 4:
                    NavigationOptions.opPremios(DashboardActivity.this, false);
                    break;
                case 5:
                    NavigationOptions.opBitacora(DashboardActivity.this, false);
                    break;
                case 6:
                    showByeMessage();
                    break;
            }
        }
    }

//    private void showMessage() {
//        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
//        dlgAlert.setMessage("Todavía no puede volver a jugar en este móvil.");
////        dlgAlert.setTitle("Appquimia");
//        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int which) {
//                dialog.cancel();
//            }
//        });
//        dlgAlert.setCancelable(true);
//
//        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
//        CustomTFSpan tfSpan = new CustomTFSpan(tf);
//        SpannableString spannableString = new SpannableString("Appquimia");
//        spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//        dlgAlert.setTitle(spannableString);
//
//        Dialog dlg = dlgAlert.create();
//        dlg.show();
//
//        try {
//            tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_regular.otf");
//            @SuppressWarnings("ConstantConditions")
//            TextView alertMessage = dlg.getWindow().findViewById(android.R.id.message);
//            alertMessage.setTypeface(tf);
//        } catch (NullPointerException ignored) {}
//    }

    private void showByeMessage() {
        AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(this);
        dlgAlert.setMessage("Gracias por elegirnos!!!");
        dlgAlert.setTitle("Appquimia");
        dlgAlert.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                if(DatabaseHelper.getDBInstance(DashboardActivity.this).isOpen())
                    DatabaseHelper.getDBInstance(DashboardActivity.this).close();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    finishAndRemoveTask();
                } else {
                    finish();
                    System.exit(0);
                }
            }
        });
        dlgAlert.setCancelable(true);

        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
        CustomTFSpan tfSpan = new CustomTFSpan(tf);
        SpannableString spannableString = new SpannableString("Appquimia");
        spannableString.setSpan(tfSpan, 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        dlgAlert.setTitle(spannableString);

        Dialog dlg = dlgAlert.create();
        dlg.show();

        try {
             tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_regular.otf");
            @SuppressWarnings("ConstantConditions")
            TextView alertMessage = dlg.getWindow().findViewById(android.R.id.message);
            alertMessage.setTypeface(tf);



//            TextView alertTitle;
//            tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
//            int textViewId = dlg.getContext().getResources().getIdentifier("android:id/alertTitle", null, null);
//            if (textViewId != 0) {
//                alertTitle = dlg.getWindow().findViewById(textViewId);
//                alertTitle.setTypeface(tf);
//            } else {
//                int titleId = getResources().getIdentifier( "alertTitle", "id", "android" );
//                if(titleId > 0) {
//                    alertTitle = dlg.getWindow().findViewById(titleId);
//                    alertTitle.setTypeface(tf);
//                }
//            }
        } catch (NullPointerException ignored) {}
    }

    private class OpcionesAdapter extends ArrayAdapter<String> {
        protected List<String> items;

        OpcionesAdapter(Context context, int textViewResourceId, List<String> items) {
            super(context, textViewResourceId, items);
            this.items = items;
        }

        @Override
        public String getItem(int pos)
        {
            return items.get(pos);
        }

        @SuppressLint("InflateParams")
        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            View v = convertView;
            if (v == null)
            {
                LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                assert vi != null;
                v = vi.inflate(R.layout.item_list_opciones, null);
            }

            String s = items.get(position);
            if (s != null)
            {
                View line = v.findViewById(R.id.line1);
                line.setVisibility((position != 0) ? (View.INVISIBLE) : (View.VISIBLE));

                TextView f1 = v.findViewById(R.id.tvOpcionTag);
                f1.setText(s);
            }
            return v;
        }
    }

    //*********************************
    //***   Sinconización de datos  ***
    //*********************************
    public void syncCuenta() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());

        Cuenta cuenta = new Cuenta();
        cuenta.fill(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));//mDatabase);

        HashMap<String, String> map = cuenta.getHashMap();

        // Crear nuevo objeto Json basado en el mapa
        JSONObject jobject = new JSONObject(map);
        // Depurando objeto Json...
        Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
        mVolley.addToRequestQueue(
            new JsonObjectRequest(
                Request.Method.POST,
                CuentaService.API_CALL,
                jobject,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Procesar la respuesta del servidor
                        //Toast.makeText(DashboardActivity.this, "Cuenta sincronizada",Toast.LENGTH_SHORT).show();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(Utils.DEBUG)
                            Toast.makeText(DashboardActivity.this, "Sin conexión",Toast.LENGTH_SHORT).show();
                    }
                }
            )
            {
                @Override
                public Map<String, String> getHeaders() {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/json; charset=utf-8");
                    headers.put("Accept", "application/json");
                    return headers;
                }

                @Override
                public String getBodyContentType() {
                    return "application/json; charset=utf-8" + getParamsEncoding();
                }
            }
        );
    }

    public void syncReclamos() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());

        Reclamo reclamo = new Reclamo();
        ArrayList<Reclamo> arrReclamos = reclamo.getNotSync(DatabaseHelper.getDBReadableInstance(this));

        for (Reclamo r : arrReclamos) {

            HashMap<String, String> map = r.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                new JsonObjectRequest(
                    Request.Method.POST,
                    ReclamoService.API_CALL,
                    jobject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            // Procesar la respuesta del servidor
                            Toast.makeText(DashboardActivity.this, "Cuenta sincronizada", Toast.LENGTH_SHORT).show();

                            try {
                                Reclamo reclamo = new Reclamo();
                                reclamo.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
                                reclamo.setSync(1);
                                reclamo.save(DatabaseHelper.getDBInstance(DashboardActivity.this));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(Utils.DEBUG)
                                Toast.makeText(DashboardActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                        }
                    }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
            );
        }
    }

    public void syncCalificaciones() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());
        Calificacion calificacion = new Calificacion();
        ArrayList<Calificacion> arrCalif = null;
        try {
            arrCalif = calificacion.getNotSync(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }

        assert arrCalif != null;
        for (Calificacion c : arrCalif) {
            HashMap<String, String> map = c.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                new JsonObjectRequest(
                    Request.Method.POST,
                    CalificacionService.API_CALL,
                    jobject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Calificacion calif = new Calificacion();
                                calif.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
                                calif.setSync(1);
                                calif.save(DatabaseHelper.getDBInstance(DashboardActivity.this));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(Utils.DEBUG)
                                Toast.makeText(DashboardActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                        }
                    }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
            );
        }
    }

    public void syncPremios() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());
        PremioXCuenta premio = new PremioXCuenta();
        ArrayList<PremioXCuenta> arrReclamos = premio.getNotSync(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));

        for (PremioXCuenta r : arrReclamos) {

            HashMap<String, String> map = r.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                new JsonObjectRequest(
                    Request.Method.POST, PremioXCuentaService.API_CALL,
                    jobject,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                PremioXCuenta pxc = new PremioXCuenta();
                                pxc.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
                                pxc.setSync(1);
                                pxc.save(DatabaseHelper.getDBInstance(DashboardActivity.this));//mDatabase);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            if(Utils.DEBUG)
                                Toast.makeText(DashboardActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                        }
                    }
                ) {
                    @Override
                    public Map<String, String> getHeaders() {
                        Map<String, String> headers = new HashMap<>();
                        headers.put("Content-Type", "application/json; charset=utf-8");
                        headers.put("Accept", "application/json");
                        return headers;
                    }

                    @Override
                    public String getBodyContentType() {
                        return "application/json; charset=utf-8" + getParamsEncoding();
                    }
                }
            );
        }
    }

    public void syncCanjeos() {
        VolleySingleton mVolley = VolleySingleton.getInstance(getApplicationContext());
        PremioXCuenta premio = new PremioXCuenta();
        ArrayList<PremioXCuenta> arrPremios = premio.getCanjeadosNotSync(DatabaseHelper.getDBReadableInstance(DashboardActivity.this));

        for (PremioXCuenta r : arrPremios) {

            HashMap<String, String> map = r.getHashMap();

            // Crear nuevo objeto Json basado en el mapa
            JSONObject jobject = new JSONObject(map);
            // Depurando objeto Json...
            Log.d(this.getLocalClassName(), jobject.toString());
            String customUrl =  PremioXCuentaService.API_CALL + "/" + r.getId();

            // Actualizar datos en el servidor
            mVolley.addToRequestQueue(
                    new JsonObjectRequest(
                            Request.Method.PUT,
                            customUrl,
                            jobject,
                            new Response.Listener<JSONObject>() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        PremioXCuenta pxc = new PremioXCuenta();
                                        pxc.fillByID(response.getString("message"), DatabaseHelper.getDBReadableInstance(DashboardActivity.this));
                                        pxc.setSync(1);
                                        pxc.save(DatabaseHelper.getDBInstance(DashboardActivity.this));//mDatabase);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } finally {
                                        if(DatabaseHelper.getDBInstance(DashboardActivity.this).isOpen())
                                            DatabaseHelper.getDBInstance(DashboardActivity.this).close();
                                    }
                                }
                            },
                            new Response.ErrorListener() {
                                @Override
                                public void onErrorResponse(VolleyError error) {
                                    if(Utils.DEBUG)
                                        Toast.makeText(DashboardActivity.this, "Sin conexión", Toast.LENGTH_SHORT).show();
                                }
                            }
                    ) {
                        @Override
                        public Map<String, String> getHeaders() {
                            Map<String, String> headers = new HashMap<>();
                            headers.put("Content-Type", "application/json; charset=utf-8");
                            headers.put("Accept", "application/json");
                            return headers;
                        }

                        @Override
                        public String getBodyContentType() {
                            return "application/json; charset=utf-8" + getParamsEncoding();
                        }
                    }
            );
        }
    }
}
