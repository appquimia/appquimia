package com.appquimia.appquimia;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.Spanned;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.appquimia.appquimia.classes.Constantes;
import com.appquimia.appquimia.classes.DatabaseHelper;
import com.appquimia.appquimia.classes.DistributedRandomNumberGenerator;
import com.appquimia.appquimia.classes.NavigationOptions;
import com.appquimia.appquimia.classes.SessionProcessor;
import com.appquimia.appquimia.classes.Utils;
import com.appquimia.appquimia.entities.Bitacora;
import com.appquimia.appquimia.entities.Clasificacion;
import com.appquimia.appquimia.entities.ConductorXMovil;
import com.appquimia.appquimia.entities.Paquete;
import com.appquimia.appquimia.entities.Premio;
import com.appquimia.appquimia.entities.PremioXCuenta;
import com.appquimia.appquimia.entities.Publicidad;
import com.appquimia.appquimia.enums.PremioEstados;

import java.io.ByteArrayInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

public class JuegoActivity extends AppCompatActivity implements OnCheckedChangeListener,View.OnClickListener, AdapterView.OnItemSelectedListener, NavigationView.OnNavigationItemSelectedListener  {
    public static final String TIPO_MODULO = "tipo_modulo";

    private static final int SOUND_WIN = 0;
    private static final int SOUND_LOST = 1;

    private DrawerLayout mDrawer;
    private View mvwGame, mvwWin;
    private SpinAdapter madpPaquetes;
    private ImageView mImagenPremio;

    protected ArrayList<Premio> mPremios;
    protected ArrayList<PremioXCuenta> mPremiosGanados = null;

    Typeface myFont;

    private SoundPool mSoundPool;
    int mSoundIds[];

    private float mNI; //intento actual
    private float mI; //cantidad máxima de intentos
    private float mG; //premios ganados

    private boolean mbFinished;
    static boolean mActive = false;
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        adjustFontScale(getResources().getConfiguration());

        int dl = R.id.dlJuego;
        int nv = R.id.nvJuego;

        switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
            case Constantes.MODULE_MOVILES:
                setContentView(R.layout.activity_juego);
                dl = R.id.dlJuego;
                nv = R.id.nvJuego;
                break;
            case Constantes.MODULE_COMMERCE:
                setContentView(R.layout.activity_cmr_juego);
                dl = R.id.dlCmrJuego;
                nv = R.id.nvCmrJuego;
                break;
            case Constantes.MODULE_STREET:
                setContentView(R.layout.activity_str_juego);
                dl = R.id.dlStrJuego;
                nv = R.id.nvStrJuego;
                break;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.BLACK);
        }

        // Sección acttion bar, toolbar, navbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mDrawer = findViewById(dl);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, mDrawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        mDrawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(nv);
        navigationView.setNavigationItemSelectedListener(this);
        final Menu navMenu = navigationView.getMenu();
        navigationView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ArrayList<View> menuItems = new ArrayList<>(); // save Views in this array
                JuegoActivity.this.navigationView.getViewTreeObserver().removeOnGlobalLayoutListener(this); // remove the global layout listener
                for (int i = 0; i < navMenu.size(); i++) {// loops over menu items  to get the text view from each menu item
                    final MenuItem item = navMenu.getItem(i);
                    JuegoActivity.this.navigationView.findViewsWithText(menuItems, item.getTitle(), View.FIND_VIEWS_WITH_TEXT);
                }
                Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_medium.otf");
                for (final View menuItem : menuItems) {// loops over the saved views and sets the font
                    ((TextView) menuItem).setTypeface(tf);
                }
            }
        });

        ActionBar bar = getSupportActionBar();
        if(bar!=null) {
            bar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            bar.setCustomView(R.layout.title_action_bar_juego);
        }

        // Sección comportamiento particular
        mPremios = Premio.getPremiosByIDPaquete(0, DatabaseHelper.getDBReadableInstance(JuegoActivity.this));

        myFont = Typeface.createFromAsset(getAssets(), "fonts/acuminpro_regular.otf");
        List<Paquete> marrPaquetes = Paquete.getList(DatabaseHelper.getDBReadableInstance(JuegoActivity.this));
        madpPaquetes = new SpinAdapter(this, R.layout.spinner_item, marrPaquetes);
        madpPaquetes.setDropDownViewResource(R.layout.spinner_dropdown_item);
        Spinner spnPaquetes = findViewById(R.id.spnPaquetes);
        spnPaquetes.setAdapter(madpPaquetes);
        spnPaquetes.setOnItemSelectedListener(this);
        spnPaquetes.setSelection(0);

        TextView mtvLeyenda = findViewById(R.id.tvLeyenda2Tag);
        Spanned text = Html.fromHtml("Descubre los <b>premios ocultos</b><br> en el tablero!!");
        mtvLeyenda.setText(text);

        mImagenPremio = findViewById(R.id.imgPremio);

        ImageButton btnClosePremio = findViewById(R.id.btnClose);
        btnClosePremio.setOnClickListener(this);

        mvwGame = findViewById(R.id.laygame);
        mvwGame.bringToFront();

        mvwWin = findViewById(R.id.laywin);
        mvwWin.setVisibility(View.GONE);

        initSound();

        mNI = 0;
        mG = 0;
        mI = 5;

        initToggles();
        mbFinished = false;
    }

    public void adjustFontScale(Configuration configuration) {
        configuration.fontScale = (float) 1.0;
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
        wm.getDefaultDisplay().getMetrics(metrics);
        metrics.scaledDensity = configuration.fontScale * metrics.density;
        getBaseContext().getResources().updateConfiguration(configuration, metrics);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_back) {
            if(mNI > 0) {
                Utils.showMessage(this, "Debe jugar todos sus turnos o los perderá.", false);
            } else {
                finish();
            }
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        if (mNI > 0) {
            Utils.showMessage(this, "Debes jugar todos tus turnos o los perderás.", false);
            return true;
        }
        int id = item.getItemId();

        if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_MOVILES) {
            if (id == R.id.nav_movil) {
                NavigationOptions.opMovil(this, true);
            } else if (id == R.id.nav_tarifador) {
                NavigationOptions.opTarifador(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificar(this, true);
            } else if (id == R.id.nav_jugar) {
                //NavigationOptions.opJugar(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremios(this, true);
            } else if (id == R.id.nav_bitacora) {
                NavigationOptions.opBitacora(this, true);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else  if (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES) == Constantes.MODULE_COMMERCE) {
            if (id == R.id.nav_marca) {
                NavigationOptions.opMarca(this, true);
            }  else if (id == R.id.nav_jugar) {
                //NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosCommerce(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        } else {
            if (id == R.id.nav_auspiciantes) {
                NavigationOptions.opAuspiciantes(this, true);
            }  else if (id == R.id.nav_jugar) {
                //NavigationOptions.opJugarStreet(this, true);
            } else if (id == R.id.nav_premios) {
                NavigationOptions.opPremiosStreet(this, true);
            } else if (id == R.id.nav_calificar) {
                NavigationOptions.opCalificarStreet(this, false);
            } else if (id == R.id.nav_recomendar) {
                NavigationOptions.opRecomentdarStreet(this, false);
            } else if (id == R.id.nav_fin) {
                NavigationOptions.finish(this);
            }
        }

        mDrawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mActive = true;
        final Handler hdPublicidad = new Handler();
        hdPublicidad.postDelayed(new Runnable() {
            public void run() {
                DatabaseHelper.getDBInstance(JuegoActivity.this).enableWriteAheadLogging();
                showPublicidad(DatabaseHelper.getDBReadableInstance(JuegoActivity.this));
                if (mActive)
                    hdPublicidad.postDelayed(this, 10000);
            }
        }, 500);
    }

    @Override
    protected void  onPause() {
        mActive = false;
        super.onPause();
    }

    @Override
    protected void onStop(){
        mActive = false;
        super.onStop();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && (mNI > 0)) {
            Utils.showMessage(this, "Debes jugar todos tus turnos o los perderás.", false);
        }
        return super.onKeyDown(keyCode, event);
    }

    private void showPublicidad(SQLiteDatabase db) {
        Publicidad pub1 = new Publicidad();
        Publicidad pub2 = new Publicidad();
        Random randomGenerator = new Random();
        float randomInt;

        while (pub1.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub1.fill(-1, randomInt, db);
        }

        while (pub2.getImagenb() == null) {
            randomInt = ((float) randomGenerator.nextInt(100) / 100);
            pub2.fill(pub1.getId(), randomInt, db);
        }

        ByteArrayInputStream imageStream;

        if(pub1.getImagenb() != null)
        {
            imageStream = new ByteArrayInputStream(pub1.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice1);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }

        if(pub2.getImagenb() != null) {
            imageStream = new ByteArrayInputStream(pub2.getImagenb());

            ImageView imgPublicidad = findViewById(R.id.advertice2);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imgPublicidad.setImageBitmap(theImage);
        }
    }

    private void initSound() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioAttributes attrs = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build();
            mSoundPool = new SoundPool.Builder()
                    .setMaxStreams(10)
                    .setAudioAttributes(attrs)
                    .build();

            setVolumeControlStream(AudioManager.STREAM_MUSIC);
            mSoundIds = new int[10];
            mSoundIds[SOUND_WIN] = mSoundPool.load(this, R.raw.win, 1);
            mSoundIds[SOUND_LOST] = mSoundPool.load(this, R.raw.lost, 1);
        }
    }

    private void initToggles() {
        ToggleButton mt11 = findViewById(R.id.tog11);
        mt11.setOnCheckedChangeListener(this);
        ToggleButton mt12 = findViewById(R.id.tog12);
        mt12.setOnCheckedChangeListener(this);
        ToggleButton mt13 = findViewById(R.id.tog13);
        mt13.setOnCheckedChangeListener(this);
        ToggleButton mt14 = findViewById(R.id.tog14);
        mt14.setOnCheckedChangeListener(this);

        ToggleButton mt21 = findViewById(R.id.tog21);
        mt21.setOnCheckedChangeListener(this);
        ToggleButton mt22 = findViewById(R.id.tog22);
        mt22.setOnCheckedChangeListener(this);
        ToggleButton mt23 = findViewById(R.id.tog23);
        mt23.setOnCheckedChangeListener(this);
        ToggleButton mt24 = findViewById(R.id.tog24);
        mt24.setOnCheckedChangeListener(this);

        ToggleButton mt31 = findViewById(R.id.tog31);
        mt31.setOnCheckedChangeListener(this);
        ToggleButton mt32 = findViewById(R.id.tog32);
        mt32.setOnCheckedChangeListener(this);
        ToggleButton mt33 = findViewById(R.id.tog33);
        mt33.setOnCheckedChangeListener(this);
        ToggleButton mt34 = findViewById(R.id.tog34);
        mt34.setOnCheckedChangeListener(this);

        ToggleButton mt41 = findViewById(R.id.tog41);
        mt41.setOnCheckedChangeListener(this);
        ToggleButton mt42 = findViewById(R.id.tog42);
        mt42.setOnCheckedChangeListener(this);
        ToggleButton mt43 = findViewById(R.id.tog43);
        mt43.setOnCheckedChangeListener(this);
        ToggleButton mt44 = findViewById(R.id.tog44);
        mt44.setOnCheckedChangeListener(this);

        ToggleButton mt51 = findViewById(R.id.tog51);
        mt51.setOnCheckedChangeListener(this);
        ToggleButton mt52 = findViewById(R.id.tog52);
        mt52.setOnCheckedChangeListener(this);
        ToggleButton mt53 = findViewById(R.id.tog53);
        mt53.setOnCheckedChangeListener(this);
        ToggleButton mt54 = findViewById(R.id.tog54);
        mt54.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if(isChecked)
        {
            mNI++;
            buttonView.setEnabled(false);

            makeATurn();
        }
    }

    private void makeATurn() {
        boolean bWin = false;

        float p = 1 / (mI - (mNI - (1 + mG)));

        Random randomGenerator = new Random();
        float randomInt = ((float) randomGenerator.nextInt(100) / 100);

        if(randomInt <= p)
        {
            mG++;
            bWin = true;
            obtenerPremio();
            showWin();
        }
        else
            lost();

        if (mNI == mI)
            mbFinished = true;

        if(!bWin && mbFinished)
            finishGame();
    }

    private void obtenerPremio() {
        if(mPremios.size() < 1)
            Toast.makeText(this, "ERROR", Toast.LENGTH_LONG).show();

        //Se obtienen de cual clasifiación se va a aobtener el premio
        DistributedRandomNumberGenerator rnd = new DistributedRandomNumberGenerator();
        Clasificacion c;
        for(Premio p : mPremios) {
            c = new Clasificacion();
            c.fillByID(p.getIdclasificacion(), DatabaseHelper.getDBReadableInstance(JuegoActivity.this));

            rnd.addNumber(c.getId(), c.getProbabilidad());
        }

        //se obtiene el premio en función de la clasificación obtenida
        c = new Clasificacion();
        c.fillByID(rnd.getDistributedRandomNumber(), DatabaseHelper.getDBReadableInstance(JuegoActivity.this));

        ArrayList<Premio> premiosCl = new ArrayList<>();
        for(Premio p : mPremios) {
            if(p.getIdclasificacion() == c.getId()) {
                premiosCl.add(p);
            }
        }

        Random randomGenerator = new Random();
        int pos = randomGenerator.nextInt(Math.abs(premiosCl.size()));
        Premio premio = premiosCl.get(pos);

        premio.fillByID(premio.getId(), DatabaseHelper.getDBReadableInstance(this));

        if(premio.getImagenb() != null) {
            ByteArrayInputStream imageStream = new ByteArrayInputStream(premio.getImagenb());

            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            mImagenPremio.setImageBitmap(theImage);
        }

        Bitacora bitacora = new Bitacora();
        bitacora.fillLast(DatabaseHelper.getDBReadableInstance(JuegoActivity.this));//mDatabase);
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat fActual = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        long lActual = Long.parseLong(fActual.format(calendar.getTime()));
        bitacora.setFecjuego(lActual + "");
        bitacora.save(DatabaseHelper.getDBInstance(JuegoActivity.this));//mDatabase);

        ArrayList arr = new ArrayList();
        try {
            arr = ConductorXMovil.getConductoresByIDMovil(bitacora.getIdmovil(), DatabaseHelper.getDBReadableInstance(JuegoActivity.this));
        } catch (Exception e) {
            e.printStackTrace();
        }
        String idconductor = "";
        if(arr.size() > 0) {
            ConductorXMovil cxm = (ConductorXMovil) arr.get(0);
            idconductor = cxm.getIdconductor();
        }

        SimpleDateFormat mdformat = new SimpleDateFormat("yyyyMMddHHmmss", new Locale("es", "AR"));
        String strDate = mdformat.format(calendar.getTime());

        if(mPremiosGanados == null)
            mPremiosGanados = new ArrayList<>();

        PremioXCuenta pxcuenta = new PremioXCuenta();
        pxcuenta.setId(UUID.randomUUID().toString());
        pxcuenta.setIdcuenta(bitacora.getIdcuenta());
        pxcuenta.setIdpremio(premio.getId());
        pxcuenta.setFecpremio(strDate);
        pxcuenta.setFecven(premio.getFecven());
        pxcuenta.setIdestado(PremioEstados.Pendiente.ordinal());//Pendiente
        pxcuenta.setIdmovil(bitacora.getIdmovil());
        pxcuenta.setIdbitacora(bitacora.getId());
        pxcuenta.setFeccanjeo("");
        pxcuenta.setIdconductor(idconductor);

        mPremiosGanados.add(pxcuenta);
    }

    private void showWin() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mSoundPool.play(mSoundIds[SOUND_WIN], 1, 1, 1, 0, 1);

        mvwGame.setVisibility(View.GONE);

        mvwWin.setVisibility(View.VISIBLE);
        mvwWin.bringToFront();
    }

    @Override
    public void onDestroy() {
        if (mSoundPool != null) {
            mSoundPool.release();
        }
        super.onDestroy();
    }

    private void lost() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mSoundPool.play(mSoundIds[SOUND_LOST], 1, 1, 1, 0, 1);
    }

    private void finishGame() {
        SessionProcessor.setLastGameID(getApplicationContext(), 1);

        SQLiteDatabase mDatabase =  DatabaseHelper.getDBInstance(JuegoActivity.this);
        mDatabase.enableWriteAheadLogging();

        for(PremioXCuenta pxcuenta : mPremiosGanados) {
            pxcuenta.save(mDatabase);
        }
        mDatabase.close();

        Intent intent = new Intent();
        switch (getIntent().getIntExtra(TIPO_MODULO, Constantes.MODULE_MOVILES)) {
            case Constantes.MODULE_MOVILES:
                intent.putExtra(ResumenActivity.TIPO_MODULO, Constantes.MODULE_MOVILES);
                break;
            case Constantes.MODULE_COMMERCE:
                intent.putExtra(ResumenActivity.TIPO_MODULO, Constantes.MODULE_COMMERCE);
                break;
            case Constantes.MODULE_STREET:
                intent.putExtra(ResumenActivity.TIPO_MODULO, Constantes.MODULE_STREET);
                break;
        }
        intent.setClass(this, ResumenActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnClose)
            closePremio();
    }

    private void closePremio() {
        mvwGame.bringToFront();
        mvwGame.setVisibility(View.VISIBLE);
        mvwWin.setVisibility(View.GONE);
        if(mbFinished)
            finishGame();
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int position, long id) {
        Paquete paq = madpPaquetes.getItem(position);
        if (paq != null) {
            mPremios = Premio.getPremiosByIDPaquete(paq.getId(), DatabaseHelper.getDBReadableInstance(JuegoActivity.this));
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public class SpinAdapter extends ArrayAdapter<Paquete> {
        private Context context;
        // Your custom items for the spinner (User)
        private List<Paquete> items;

        SpinAdapter(Context context, int textViewResourceId, List<Paquete> items) {
            super(context, textViewResourceId, items);
            this.context = context;
            this.items = items;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        @Override
        public Paquete getItem(int position) {
            return items.get(position);
        }

        @Override
        public long getItemId(int position) {
            return items.get(position).getId();
        }

        @NonNull
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            TextView label = new TextView(context);
            label.setText(items.get(position).getPaquete());
            label.setTextColor(getResources().getColor(R.color.colorFonts));
            label.setTextSize(16);
            label.setTypeface(myFont);

            label.setPadding(5,5,5,5);

            return label;
        }

        @Override
        public View getDropDownView(int position, View convertView, @NonNull ViewGroup parent) {
            TextView label = new TextView(context);
            label.setText(items.get(position).getPaquete());
            label.setTextColor(getResources().getColor(R.color.colorFonts));
            label.setTextSize(16);

            label.setTypeface(myFont);

            label.setPadding(5,5,5,5);

            return label;
        }

    }
}
